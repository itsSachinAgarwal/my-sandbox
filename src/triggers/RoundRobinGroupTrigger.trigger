trigger RoundRobinGroupTrigger on Round_Robin_Group__c (before insert, before update, after insert, after update) {
	if(Trigger.isBefore){
		if(Trigger.isInsert){
			RoundRobinGroupTriggerHandler.validateQueueIdsBeforeInsert(Trigger.New);
		}
		else if(Trigger.isUpdate){
			RoundRobinGroupTriggerHandler.validateQueueIdsBeforeUpdate(Trigger.New, Trigger.oldMap);
		}
	}
	else if(Trigger.isAfter){
		if(Trigger.isInsert){
			RoundRobinGroupTriggerHandler.populateQueueMembers(Trigger.New);
		}
		else if(Trigger.isUpdate){
			RoundRobinGroupTriggerHandler.removeAndPopulateNewQueueMembersOnQueueChange(Trigger.oldMap, Trigger.newMap);
		}
	}
}