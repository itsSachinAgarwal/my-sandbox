trigger OpportunityStatusChange on Opportunity (after update) 
{

    list<Opportunity> oppList=[select id, (Select Id, pricebookentryid From OpportunityLineItems) from opportunity where id in :Trigger.new and custom_status__c='Reset'];
    
    for(Opportunity singleOpp:oppList)
    {
        if(Trigger.oldMap.get(singleOpp.id).custom_status__c!='Reset')
        {        
            if(singleOpp.OpportunityLineItems.size()>0)
            delete singleOpp.OpportunityLineItems;
        }
    }
}