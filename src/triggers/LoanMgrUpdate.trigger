trigger LoanMgrUpdate on Loan__c (before insert) {
	//list<loan__c> listOfLoans=[select id, manager__r.id, city__c from loan__c where id in :Trigger.New];
    list<city_manager__c> listOfCityMgrs=[select id,city__c, manager__C from city_manager__c];
    map<String, city_manager__c> mymap=new map<String, city_manager__c>();
    
    for(city_manager__c singleCityMgr:listOfCityMgrs)
    {
        mymap.put(singleCityMgr.city__c, singleCityMgr);
    }
    for(loan__c singleLoan:Trigger.New)
    {
        singleLoan.manager__c=mymap.get(singleLoan.city__c).manager__C;
    }
}