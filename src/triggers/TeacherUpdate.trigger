trigger TeacherUpdate on Contact (before insert, before update) 
{
    for(Contact cnt:Trigger.New)
    {
        if(cnt.Subjects__c!=null)
        {
        list<string> listOfSubjects=cnt.Subjects__c.split(';');
        set<string> myset=new set<string>();
        for(string subject:listOfSubjects)
        {
            myset.add(subject);
        }
        if(myset.contains('Hindi'))
        {
            cnt.addError('Teacher insert/update is not allowed in case he/she is teaching Hindi');
        }
        }
    }
}