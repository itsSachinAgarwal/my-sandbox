trigger MaxLimitStuInsert on Student__c (before insert) {
	for(Student__c singleStudent:Trigger.New)
    {
        class__c cls=[select Number_Of_Students__c,maxsize__c from class__c where id=:singleStudent.class__c];
        if(cls.maxsize__c==cls.Number_Of_Students__c)
        {
            singleStudent.addError('Cannot insert more students into the class as the class has already reached its max limit');
        }
    }
}