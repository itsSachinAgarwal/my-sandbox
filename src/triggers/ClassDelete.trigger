trigger ClassDelete on Class__c (before delete) 
{

  List<Class__C> lstClass = [select ID, (Select Id FROM StudentClassRel__r Where sex__c='Female')
                                     from Class__c where Id IN :Trigger.old];
    for(class__c singleClass: lstClass )  
    {
        
        if(singleClass != null && singleClass.StudentClassRel__r.size() > 1)
        {
            singleClass.addError('Class delete is not allowed in case there is more than one female student in the class');
        }
    }
}