trigger CampaignTrigger on Campaign (before insert) {
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            CampaignTriggerHandler.beforeInsert(Trigger.New);
        }
    }
}