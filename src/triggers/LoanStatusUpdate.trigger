trigger LoanStatusUpdate on Loan__c (after update) {
	for(ID loanId:Trigger.newMap.keyset())
    {
        if(Trigger.newMap.get(loanId).Loan_Status__c != Trigger.oldMap.get(loanId).Loan_Status__c && Trigger.newMap.get(loanId).Loan_Status__c !=null)
        {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
            mail.setSubject('Alert:Loan Status Changed to '+Trigger.newMap.get(loanId).Loan_Status__c);
            mail.setToAddresses(new String[] { Trigger.newMap.get(loanId).Applicant_Email__c });
            string body='Hi '+Trigger.newMap.get(loanId).Applicant_Name__c+',<br/>Status of loan corresponding to id '+Trigger.newMap.get(loanId).id+' has been changed. Kindly do the needful<br/>Thanks.';
            mail.setHTMLBody(body);
            
            mail.setSaveAsActivity(false);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail}); 
        }
     }
    
}