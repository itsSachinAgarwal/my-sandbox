trigger MyCountUpdate on Student__c (after insert, after update) 
{
	for(Student__c singleStudent:Trigger.New)
    {
        class__c cls=[select MyCount__c from class__c where id=:singleStudent.class__c];
        if(cls.MyCount__c==null)
        {
            cls.MyCount__c=0;
        }
        cls.MyCount__c=cls.MyCount__c+1;
        update cls;
    }
}