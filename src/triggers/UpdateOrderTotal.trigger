trigger UpdateOrderTotal on Order_Line_Item__c (after insert, after update, after delete) {
    if(Trigger.isInsert || Trigger.isUpdate)
    {
    for(Order_Line_Item__c singleOrderLineItem:Trigger.New)
    {
        list<Purchase_Order__c> listOfPurchaseOrder;
        listOfPurchaseOrder=[select id, Order_Total__c from Purchase_Order__c where id=:singleOrderLineItem.Purchase_Order__c];
        
        if(Trigger.isInsert)
        {
            
            for(Purchase_Order__c singlePurchaseOrder:listOfPurchaseOrder)
            {
                if(singlePurchaseOrder.Order_Total__c==null)
                {
                    singlePurchaseOrder.Order_Total__c=0;
                }
                singlePurchaseOrder.Order_Total__c+=singleOrderLineItem.Total__c;
            }
        }
        if(Trigger.isUpdate)
        {
            
            Order_Line_Item__c OldOrderLineItem = Trigger.oldMap.get(singleOrderLineItem.Id);
            
            for(Purchase_Order__c singlePurchaseOrder:listOfPurchaseOrder)
            {
                if(singlePurchaseOrder.Order_Total__c==null)
                {
                    singlePurchaseOrder.Order_Total__c=0;
                }
                singlePurchaseOrder.Order_Total__c+=singleOrderLineItem.Total__c;
                singlePurchaseOrder.Order_Total__c-=OldOrderLineItem.Total__c;
            }
        }
        update listOfPurchaseOrder;
    }
    }
    if(Trigger.isDelete)
    {
        for(Order_Line_Item__c singleOrderLineItem:Trigger.Old)
        {
            list<Purchase_Order__c> listOfPurchaseOrder;
        	listOfPurchaseOrder=[select id, Order_Total__c from Purchase_Order__c where id=:singleOrderLineItem.Purchase_Order__c];
            for(Purchase_Order__c singlePurchaseOrder:listOfPurchaseOrder)
            {
             	if(singlePurchaseOrder.Order_Total__c==null)
                {
                    singlePurchaseOrder.Order_Total__c=0;
                }   
                singlePurchaseOrder.Order_Total__c-=singleOrderLineItem.Total__c;
            }
            update listOfPurchaseOrder;
        }
    
    }
	
}