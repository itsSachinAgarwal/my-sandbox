trigger ChildAccCountUpdate on Account (after insert, after update, after delete) 
{
    if(checkRecursive.runOnce())
    {
       list<account> listOfAccounts=[select id, Child_Count__c, accountnumber, (select id, Child_Count__c from ChildAccounts) from Account];
      list<account> newAccounts=new list<account>();
       for(Account singleAcc:listOfAccounts)   
       {
           singleAcc.Child_Count__c=singleAcc.ChildAccounts.size();
       }
      update listOfAccounts;
    }
}