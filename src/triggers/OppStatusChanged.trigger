trigger OppStatusChanged on Opportunity (after update) {
    EmailTemplate template = [Select id, Body from EmailTemplate where name = 'Notify Opp Owner'];
    list <opportunity> oppList=[select id, owner.id, owner.Email from opportunity where id in :Trigger.New];
    Lead tempLead;
	for(Opportunity singleOpp:oppList)
    {
        if(Trigger.newMap.get(singleOpp.Id).custom_Status__c != Trigger.oldMap.get(singleOpp.Id).custom_Status__c)
        {
            tempLead = new Lead(Email=singleOpp.owner.Email, LastName='test', Company='test'); insert tempLead;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
            mail.setTemplateId(template.id); 
            //mail.setToAddresses(new String[] { singleOpp.owner.Email });
            mail.setTargetObjectId(tempLead.id);
            system.debug('id of the owner:'+tempLead.id);
			mail.setWhatId(singleOpp.id);
            mail.setSaveAsActivity(false);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail}); 
            delete tempLead;
        }
     }
}