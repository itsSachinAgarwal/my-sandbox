/* 
# Created by............: Nathan Shilling
# Created Date..........: 01/10/2013
# Last Modified by......: Jarrod Kingston
# Last Modified Date....: 04/28/2014

Common trigger handler for Case object.
    
*/

trigger CaseTrigger on Case (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
    //CaseTriggerHandler handler = new CaseTriggerHandler(Trigger.isExecuting, Trigger.size);
    
    if (Trigger.isInsert && Trigger.isBefore){
    	CaseTwoTriggerHandler.updateCaseOwner(Trigger.New);
        // Enable if needed
        //handler.OnBeforeInsert(Trigger.new);
    }
    else if (Trigger.isInsert && Trigger.isAfter){ 
        // Enable if needed
        //handler.OnAfterInsert(Trigger.new);
        //CaseTriggerHandler.OnAfterInsertAsync(Trigger.newMap.keySet());
    }
    
    else if (Trigger.isUpdate && Trigger.isBefore){
    	CaseTwoTriggerHandler.updateCaseOwner(Trigger.New);
        //handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap, Trigger.oldMap);
    }
    else if (Trigger.isUpdate && Trigger.isAfter){
        // Enable if needed
        //handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap, Trigger.oldMap);
        //CaseTriggerHandler.OnAfterUpdateAsync(Trigger.newMap.keySet());
    }
    
    else if (Trigger.isDelete && Trigger.isBefore){
        // Enable if needed
        //handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
    }
    else if (Trigger.isDelete && Trigger.isAfter){
        // Enable if needed
        //handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
        //CaseTriggerHandler.OnAfterDeleteAsync(Trigger.oldMap.keySet());
    }
    
    else if (Trigger.isUnDelete){
        // Enable if needed
        //handler.OnUndelete(Trigger.new);    
    }
}