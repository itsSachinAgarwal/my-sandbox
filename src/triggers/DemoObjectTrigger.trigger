/*******************************************************************
Name  : DemoObjectTrigger
Author: Sachin Agarwal
Date  : Mar 2, 2016
Description: Trigger which will fire when a demo object is inserted 
 *************************************************************************/
trigger DemoObjectTrigger on Demo_Object__c (before insert) {
	if(trigger.isBefore){
		if(trigger.isInsert){
			DemoObjectTriggerHandler.processDemoRecords(trigger.new);
		}
	}   
}