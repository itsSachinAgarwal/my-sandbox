trigger OpportunityStageChange on Opportunity (before update) 
{
	for(ID oppId:Trigger.newMap.keyset())
    {
        if((Trigger.newMap.get(oppId).StageName=='Closed Won' || Trigger.newMap.get(oppId).StageName=='Closed Lost') && Trigger.oldMap.get(oppId).StageName!='Closed Lost' && Trigger.oldMap.get(oppId).StageName!='Closed Won')
        {
            Trigger.newMap.get(oppId).CloseDate=System.today();
        }
    }
}