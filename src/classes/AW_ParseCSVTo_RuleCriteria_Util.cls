public with sharing class AW_ParseCSVTo_RuleCriteria_Util {
	
	private static String filterCriteria;
	public AW_ParseCSVTo_RuleCriteria_Util(){}
	
	public static AW_ParseCSV_RuleWrapper  parseRuleCriteria(AW_ParseCSV_RuleWrapper ruleWrapper){
		String[] spaceSplitRuleCriteria = ruleWrapper.displayCriteria.trim().split(' ');
		filterCriteria = ruleWrapper.displayCriteria;
		// prepare Criteria List in String form
		List<String> childLogicalOperator = prepareListForCriterias(spaceSplitRuleCriteria);
		// prepare Criteria List in RuleCriteriaWrapper form
		ruleWrapper.ruleCriteriaList = prepareRuleCriteriaList(childLogicalOperator,ruleWrapper.displayCriteria, ruleWrapper.rule.Object__c);
		System.debug('::filterCriteria'+filterCriteria);
		ruleWrapper.rule.Filter_Logic__c = filterCriteria.replace('.','').replace('(' , ' ( ').replace(')', ' ) ');
		ruleWrapper.displayCriteria = ruleWrapper.displayCriteria.replace('.AND.' , 'AND').replace('.OR.' , 'OR');
		validateRuleCriteria(ruleWrapper);
		return ruleWrapper;
	}
	
	// prepare Criteria List in String form
	private static List<String> prepareListForCriterias(String[] spaceSplitRuleCriteria){
		List<String> childLogicalOperator = new List<String>();
		String singleCriteria = '';
		String tempSingleCriteria = '';
		Integer criteriaNum = 1;
		Pattern listPattern = Pattern.compile('\\(+|\\)+');
		for(String ssrc : spaceSplitRuleCriteria){
			System.debug('::ssrc'+ssrc);
			Matcher matcher = listPattern.matcher(ssrc);
			if(AW_Rule_Constants.logicalOperatorSet.contains(ssrc.trim()) || matcher.matches()){
				//mainLogicalOperator.add(ssrc);
				
				System.debug(':::singleCriteria'+singleCriteria);
				if(singleCriteria != ''){
					childLogicalOperator.add(singleCriteria);
					System.debug(':::::fl'+filterCriteria.contains(tempSingleCriteria.trim())+'::::'+tempSingleCriteria);
					if(tempSingleCriteria.contains('$') || tempSingleCriteria.contains('+') || tempSingleCriteria.contains('*') || tempSingleCriteria.contains('?') || tempSingleCriteria.contains('^')){
						tempSingleCriteria = tempSingleCriteria.replace('$', '\\$').replace('+','\\+').replace('*','\\*').replace('?','\\?').replace('^','\\^');
					}
					if(tempSingleCriteria.contains('(') || tempSingleCriteria.contains(')') || tempSingleCriteria.contains('{') || tempSingleCriteria.contains('}') || tempSingleCriteria.contains('[') || tempSingleCriteria.contains(']')){
						filterCriteria = filterCriteria.replace(tempSingleCriteria.trim() , '.' + String.valueOf(criteriaNum) + '.');
					}else{
						filterCriteria = filterCriteria.replaceFirst(tempSingleCriteria.trim() , '.' + String.valueOf(criteriaNum) + '.');
					}
					criteriaNum++;
				}
				singleCriteria = '';
				tempSingleCriteria = '';
			}else{
				if(ssrc != ''){
			  		singleCriteria += ' ' + ssrc.trim(); 
				}
				tempSingleCriteria += ' ' + ssrc;
			} 
		}
		if(singleCriteria.trim() != ''){
			childLogicalOperator.add(singleCriteria.trim());
			if(tempSingleCriteria.contains('$') || tempSingleCriteria.contains('+') || tempSingleCriteria.contains('*') || tempSingleCriteria.contains('?') || tempSingleCriteria.contains('^')){
				tempSingleCriteria = tempSingleCriteria.replace('$', '\\$').replace('+','\\+').replace('*','\\*').replace('?','\\?').replace('^','\\^');
			}
			if(tempSingleCriteria.contains('(') || tempSingleCriteria.contains(')') || tempSingleCriteria.contains('{') || tempSingleCriteria.contains('}') || tempSingleCriteria.contains('[') || tempSingleCriteria.contains(']')){
				filterCriteria = filterCriteria.replace(tempSingleCriteria.trim() , '.' + String.valueOf(criteriaNum) + '.');
			}else{
				filterCriteria = filterCriteria.replaceFirst(tempSingleCriteria.trim() , '.' + String.valueOf(criteriaNum) + '.');
			}
		}
		System.debug(childLogicalOperator.size() + '::childLogicalOperator' + childLogicalOperator); 
		return childLogicalOperator;
	}
	
	// prepare Criteria List in RuleCriteriaWrapper form
	public static List<AW_ParseCSV_RuleCriteriaWrapper> prepareRuleCriteriaList( List<String> childLogicalOperator , String displayCriteria, String relatedObject){
		List<AW_ParseCSV_RuleCriteriaWrapper>  ruleCriteriaList = new List<AW_ParseCSV_RuleCriteriaWrapper>();
	//	Integer criteriaNum = 1;
		//filterCriteria = displayCriteria;
		for(String criteria : childLogicalOperator){
			if(AW_Rule_Constants.logicalOperatorSet.contains(criteria)){
				continue;
			}
		//    filterCriteria  = filterCriteria.replace(criteria, String.valueOf(criteriaNum));
		  //  criteriaNum++;    
			System.debug(':: '+displayCriteria);
			String[] criteriaFields = splitByOperator(criteria);
			if(criteriaFields == null){
				criteriaFields = criteria.trim().split(' ');
			}
			//String[] criteriaFields = criteria.trim().split(' ');
			System.debug('::criteriaFields'+criteriaFields);
			if(criteriaFields != null && criteriaFields.size() > 1){
				System.debug(criteriaFields.size() +':::'+criteriaFields[1]+':::'+AW_Rule_Constants.matchingTypeMap.keySet().contains(criteriaFields[1].trim().toUpperCase()));
				if(AW_Rule_Constants.matchingTypeMap.keySet().contains(criteriaFields[1].trim().toUpperCase())){
					System.debug(':::Operator'+AW_Rule_Constants.matchingTypeMap.get(criteriaFields[1].trim().toUpperCase()));
					AW_ParseCSV_RuleCriteriaWrapper rcWrapper = prepareRuleCriteriaWrapper(criteriaFields,relatedObject);
					rcWrapper.ruleCriteria.Related_Object__c = relatedObject;
					rcWrapper.ruleCriteria.Matching_Type__c = AW_Rule_Constants.matchingTypeMap.get(criteriaFields[1].trim().toUpperCase());
					rcWrapper = validateRuleCriteriaField(rcWrapper);
					ruleCriteriaList.add(rcWrapper);
				}else{
					throw new AW_Exception(criteriaFields[1].trim() + ' is not a matching type please use proper matching type.');
				}
			}
		}
		System.debug(':::ruleCriteriaList'+ruleCriteriaList);  
		System.debug(':::ruleCriteriaList size'+ruleCriteriaList.size());
		return  ruleCriteriaList;
	}
	
	private static AW_ParseCSV_RuleCriteriaWrapper prepareRuleCriteriaWrapper(String[] criteriaFields, String relatedObject){
		AW_ParseCSV_RuleCriteriaWrapper ruleCriteriaWrapper = new AW_ParseCSV_RuleCriteriaWrapper(new RuleCriteria__c());
		if(criteriaFields.size() >= 2){
			// TO DO for some special 
			String value = '';
			System.debug(':::criteriaFields[0]'+criteriaFields[0]);
			if(criteriaFields[0].trim().startsWith(AW_Rule_Constants.CROSSFORMULA)){
				ruleCriteriaWrapper.ruleCriteria.CrossFormula__c = criteriaFields[0].trim().substring(criteriaFields[0].indexOf('.')+ 1 , criteriaFields[0].length());
				ruleCriteriaWrapper.ruleCriteria.Field_Name__c = AW_Rule_Constants.CROSSFORMULA;
				ruleCriteriaWrapper.ruleCriteria.Field_API_Name__c = AW_Rule_Constants.CROSSFORMULA;
				ruleCriteriaWrapper.ruleCriteria.Field_Type__c = 'String';
				ruleCriteriaWrapper.isCrossFormula = true; 
			}else if(criteriaFields[0].trim().contains(AW_Rule_Constants.GLOBAL_VARIABLE_CSV)){
				String field = criteriaFields[0].trim().replace(relatedObject.trim() +'.' , '').replaceFirst('\\$' , '-');
				ruleCriteriaWrapper.ruleCriteria.Field_Name__c = field.toLowerCase();
				ruleCriteriaWrapper.ruleCriteria.Field_API_Name__c = field.toLowerCase();
				ruleCriteriaWrapper.ruleCriteria.Field_Type__c = 'String';
				ruleCriteriaWrapper.isGlobalVariable = true;
			}else{
				if(criteriaFields[0].trim().contains('.')){
					String[] spField = criteriaFields[0].trim().split('\\.');
					String field = (spField[1].trim() == AW_Rule_Constants.CSV_RT ? AW_Rule_Constants.CRITERIA_RT.trim() : spField[1].trim());
					ruleCriteriaWrapper.ruleCriteria.Field_Name__c = field.toLowerCase();
					ruleCriteriaWrapper.ruleCriteria.Field_API_Name__c = field.toLowerCase();
				}else{
					String field = (criteriaFields[0].trim() == AW_Rule_Constants.CSV_RT ? AW_Rule_Constants.CRITERIA_RT.trim() : criteriaFields[0].trim());
					ruleCriteriaWrapper.ruleCriteria.Field_Name__c = field.toLowerCase();
					ruleCriteriaWrapper.ruleCriteria.Field_API_Name__c = field.toLowerCase();
				}
				
			}
			if (criteriaFields[1].trim()== AW_Rule_Constants.BETWEEN){
				ruleCriteriaWrapper.ruleCriteria.Minimum_Value__c = '';
				Integer valCount = 2;
				while(criteriaFields[valCount] != AW_Rule_Constants.BETWEEN_OPERATOR && criteriaFields.size() > valCount){
					if(criteriaFields[valCount] != ''){
						// remove ( from Between
						if(criteriaFields[valCount].trim().startsWith('(') && !criteriaFields[valCount].trim().contains(')')){
							criteriaFields[valCount] = criteriaFields[valCount].trim().substring(1,criteriaFields[valCount].trim().length());
						}
						ruleCriteriaWrapper.ruleCriteria.Minimum_Value__c += criteriaFields[valCount] + ' ';
					}
					valCount++; 
				}
				ruleCriteriaWrapper.ruleCriteria.Maximum_Value__c = '';
				if(criteriaFields.size() > 0){
					for(valCount = valCount + 1; valCount < criteriaFields.size();valCount++){
						ruleCriteriaWrapper.ruleCriteria.Maximum_Value__c += criteriaFields[valCount] + '';
					}
					if(ruleCriteriaWrapper.ruleCriteria.Maximum_Value__c != null  && ruleCriteriaWrapper.ruleCriteria.Maximum_Value__c.trim().endsWith(')') && !ruleCriteriaWrapper.ruleCriteria.Maximum_Value__c.trim().contains('(')){
						ruleCriteriaWrapper.ruleCriteria.Maximum_Value__c  = ruleCriteriaWrapper.ruleCriteria.Maximum_Value__c.trim().subString(0,ruleCriteriaWrapper.ruleCriteria.Maximum_Value__c.trim().length() - 1);
					}
				}
				System.debug(':::ruleCriteriaWrapper.ruleCriteria.Maximum_Value__c '+ ruleCriteriaWrapper.ruleCriteria.Maximum_Value__c);
				System.debug(':::ruleCriteriaWrapper.ruleCriteria.minimum_Value__c '+ ruleCriteriaWrapper.ruleCriteria.minimum_Value__c);
				
			}
			else if(criteriaFields.size() > 2){
				for(Integer i = 2; i< criteriaFields.size();i++){
					value += criteriaFields[i] + ' ';
				}
				if(criteriaFields[1].trim() == 'ChangedFromTo'){
					ruleCriteriaWrapper.ruleCriteria.Matching_Value__c  = value;
					System.debug('::::Matching Value for changedFrom'+ruleCriteriaWrapper.ruleCriteria.Matching_Value__c );
				}else {
					ruleCriteriaWrapper.ruleCriteria.Matching_Value__c  = value;
					System.debug('::::Matching Value '+ruleCriteriaWrapper.ruleCriteria.Matching_Value__c );
				}
			}
		}
		return ruleCriteriaWrapper;
	}
	
	private static AW_ParseCSV_RuleCriteriaWrapper  validateRuleCriteriaField(AW_ParseCSV_RuleCriteriaWrapper ruleCriteriaWrapper){
		 if(ruleCriteriaWrapper.ruleCriteria.Related_Object__c != null && ruleCriteriaWrapper.ruleCriteria.Field_API_Name__c != null && ruleCriteriaWrapper.ruleCriteria.Field_API_Name__c != AW_Rule_Constants.CROSSFORMULA && ruleCriteriaWrapper.ruleCriteria.Field_API_Name__c != 'OwnerName' && !ruleCriteriaWrapper.ruleCriteria.Field_API_Name__c.trim().contains(AW_Rule_Constants.GLOBAL_VARIABLE)){
        	AW_ParseCSVTo_AdvanceWorkflow_Util.addFieldsInWrapperMap(ruleCriteriaWrapper.ruleCriteria , 'Field_API_Name__c',ruleCriteriaWrapper.ruleCriteria.Related_Object__c.trim());
        	System.debug('::fieldWrapperMap'+AW_ParseCSVTo_AdvanceWorkflow_Util.fieldWrapperMap);
        	Map<String,Map<String, AW_ListRules.FieldWrapper>> fieldWrapperMap = AW_ParseCSVTo_AdvanceWorkflow_Util.fieldWrapperMap;
        	if(fieldWrapperMap.get(ruleCriteriaWrapper.ruleCriteria.Related_Object__c).containsKey(ruleCriteriaWrapper.ruleCriteria.field_API_Name__c.toLowerCase().trim())){
        		ruleCriteriaWrapper.ruleCriteria.Field_Type__c = fieldWrapperMap.get(ruleCriteriaWrapper.ruleCriteria.Related_Object__c).get(ruleCriteriaWrapper.ruleCriteria.field_API_Name__c.toLowerCase().trim()).dataType;
        		ruleCriteriaWrapper.ruleCriteria.Field_API_Name__c = fieldWrapperMap.get(ruleCriteriaWrapper.ruleCriteria.Related_Object__c).get(ruleCriteriaWrapper.ruleCriteria.field_API_Name__c.toLowerCase().trim()).apiName;
        		ruleCriteriaWrapper.ruleCriteria.Field_Name__c = ruleCriteriaWrapper.ruleCriteria.Field_API_Name__c;
        		
        	}else{
        		// ERROR 
        		System.debug('Error');
        		throw new AW_Exception('Field ' + ruleCriteriaWrapper.ruleCriteria.Field_API_Name__c + ' is not exist on ' + ruleCriteriaWrapper.ruleCriteria.Related_Object__c );
        	}
        }
        return ruleCriteriaWrapper; 
	}
	
	private static Boolean validateRuleCriteria(AW_ParseCSV_RuleWrapper ruleWrapper){
		// validate rule criteria format
		Pattern listPattern = Pattern.compile('((\\()*[1-9][0-9]|[1-9][0-9]|(\\()*[1-9]|[1-9]|[1-9](\\))*|[1-9][0-9](\\))*)(((\\s(AND|And|Or|OR|and|or)\\s)|(AND|And|Or|OR|and|or))((\\()*[1-9][0-9]|[1-9][0-9]|(\\()*[1-9]|[1-9]|[1-9](\\))*|[1-9][0-9](\\))*))*(\\))*');
		String patterns = ruleWrapper.rule.Filter_Logic__c.trim();
		System.debug(':patterns'+patterns);
		patterns = patterns.replace(' ','');
		Matcher matcher = listPattern.matcher(patterns);
		//error encountered 
		if(matcher.matches()== false){
			throw new AW_Exception('Wrong Format! Follow  format eg:1AND(3OR4) or there is some issue in Criteria format');
		}
		// validate Rule Criteria Count
		Advance_Workflow_Limits__c ruleCriteriaLimits = Advance_Workflow_Limits__c.getInstance('NumCriteriaperRule');
		if(ruleCriteriaLimits != null && ruleCriteriaLimits.value__c != null && ruleWrapper.ruleCriteriaList.size() > ruleCriteriaLimits.value__c){
			throw new AW_Exception('Criteria Limit of '+ruleCriteriaLimits.value__c+' Exceeded in rule ' + ruleWrapper.rule.Name);
		}
		
		return true;
	}
	
	
	private static String[] splitByOperator(String criteria){
		String operator;
		String[] criterias;
		if(criteria.contains('>=')){
			criterias = criteria.split('>=');
			operator = '>=';
		}
		else if(criteria.contains('<=')){
			criterias = criteria.split('<=');
			operator = '<=';
		}else if(criteria.contains('!=NULL')){
			criterias = criteria.split('!=NULL');
			operator = '!=NULL';
		}
		else if(criteria.contains('!=')){
			criterias = criteria.split('!=');
			operator = '!=';
		}else if(criteria.contains('=NULL')){
			criterias = criteria.split('=NULL');
			operator = '=NULL';
		}
		else if(criteria.contains('=')){
			criterias = criteria.split('=');
			operator = '=';
		}
		else if(criteria.contains('>')){
			criterias = criteria.split('>');
			operator = '>';
		}
		else if(criteria.contains('<')){
			criterias = criteria.split('<');
			operator = '<';
		}
		if(criterias != null && criterias.size() > 0){
			List<String> newCriteria = new List<String>();
			newCriteria.add(criterias[0].trim());
			newCriteria.add(operator);
			if(criterias.size() > 1){
				newCriteria.add(criterias[1].trim());
			}
			return newCriteria;
		}
		return null;
	}

}