public class CreatePurchaseOrder {
    public list<Order_Line_Item__c> OrderLineItemsList{get;set;}
    public list<Order_Line_Item__c> delOrderLineItemsList{get;set;}
    public Purchase_Order__c PurchaseOrder;
    public Order_Line_Item__c del;

    public Integer rowIndex {get;set;}
        
	public CreatePurchaseOrder(ApexPages.StandardController controller)
    {
        PurchaseOrder = (Purchase_Order__c)controller.getRecord();
        OrderLineItemsList=[select id, Product__c, Quantity__c, Total__c, product__r.Product_Name__c, product__r.Unit_Price__c from Order_Line_Item__c where Purchase_Order__c=:PurchaseOrder.id];
        delOrderLineItemsList= new list<Order_Line_Item__c>();
        
    }
    public void deleteRow()
    {
    	rowIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('rowIndex'));
 		System.debug('rowbe deleted ' + rowIndex );
 		System.debug('rowm to be deleted '+OrderLineItemsList[rowIndex]);
 		del = OrderLineItemsList.remove(rowIndex);
 		delOrderLineItemsList.add(del);   
    }
    public void addRow()
    {
 		OrderLineItemsList.add(new Order_Line_Item__c(Purchase_Order__c = PurchaseOrder.Id));
 	}
    public PageReference save()
    {
        
 		upsert OrderLineItemsList;
        list<Order_Line_Item__c> delOrderLineItemsListTemp=[select id, name from Order_Line_Item__c where id IN :delOrderLineItemsList];
 		delete delOrderLineItemsListTemp;
 		//return null;
         return (new ApexPages.StandardController(PurchaseOrder)).view();
 } 
    
}