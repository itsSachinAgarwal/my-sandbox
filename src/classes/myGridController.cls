public class myGridController 
{
	public String GcolumnFields { get; set; }
    public String Gsoql { get; set; }
    public String GcolumnTitles { get; set; }
    public String soqlsort {get;set;}
    public integer noOfRecords{get;set;}
    public Integer pageNo{get;set;}
    public String sortDir {
        // To set a Direction either in ascending order or descending order.
                                get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;}
        set;
    }

    // the current field to sort by. defaults to last name
    public String sortField {
        // To set a Field for sorting.
                                get  { 
                                    
                                    if (sortField == null) 
									{
                                       // list<String> collist=columnFieldsList();
                                        sortField = columnFieldsList[0]; 
                                    } return sortField;  
                                }
        set;
    } 
    public integer size{
        get{
            if(size == null)
            {
                size=10;
            }
            return size;
        }
        
        set;}
    
    public ApexPages.StandardSetController setCon {
        get{
            if(setCon == null){
                
                string queryString = Gsoql+' order by '+ sortField + ' ' +sortDir;
                System.debug('+++++++++++queryString '+size);
                System.debug('+++++++++++Database.getQueryLocator(queryString) '+Database.getQueryLocator(queryString));
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
                setCon.setPageSize(Integer.valueof(size));
                noOfRecords = setCon.getResultSize();
            }
            return setCon;
        }set;
    }
    
    public List<String> columnFieldsList {
        get {
            if(columnFieldsList == null && GcolumnFields != null) {
                columnFieldsList = GcolumnFields.split(',');
                for(Integer x = 0; x < columnFieldsList.size(); x++) {
                    columnFieldsList[x] = columnFieldsList[x].trim();
                }
            }
            return columnFieldsList;
        }
        set;
    }
     
    public List<String> columnTitlesList {
        get {
            if(columnTitlesList == null && GcolumnTitles != null) {
                columnTitlesList = GcolumnTitles.split(',');
                for(Integer x = 0; x < columnTitlesList.size(); x++) {
                    columnTitlesList[x] = columnTitlesList[x].trim();
                }
            }
            return columnTitlesList;
        }
        set;
    }
     
    public List<sObject> records {
        get {
            //if(records == null && Gsoql != null) {
                records = setCon.getRecords();
            //}
            return records;
        }
        set;
    }
    
    public Boolean hasNext {
        get {
            return setCon.getHasNext();
        }
        set;
    }
    public Boolean hasPrevious {
        get {
            return setCon.getHasPrevious();
        }
        set;
    }
  
    public Integer pageNumber {
        get {
            return setCon.getPageNumber();
        }
        set;
    }
  
    public void first() {
        setCon.first();
    }
  
    public void last() {
        setCon.last();
    }
  
    public void previous() {
        setCon.previous();    
    }
    
    public void next() {
        setCon.next();
    }
    
    public void updatePageNumber()
    {
        setCon.setPageNumber(pageNo);
    }
    
    public void toggleSort() 
    {
        sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
        string queryString = Gsoql+' order by '+ sortField + ' ' +sortDir;
        System.debug('INtogglesort'+size);
        System.debug('+++++++++++Database.getQueryLocator(queryString) '+Database.getQueryLocator(queryString));
        setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
        setCon.setPageSize(Integer.valueof(size));
        noOfRecords = setCon.getResultSize();
        //records=setCon.getRecords();
    }
}