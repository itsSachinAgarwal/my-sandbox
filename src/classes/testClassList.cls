@istest
public class testClassList 
{
	public static testmethod void testLoad()
    {
        ClassList obj=new ClassList(new ApexPages.StandardController(new Class__c()));
        Class__c singlClass=new Class__c(name='1st');            
        insert singlClass;
        obj.selectedClassEdit=singlClass.id;
        obj.loadRecord();
		obj.saveObj();
        obj.cancelobj();
    }
}