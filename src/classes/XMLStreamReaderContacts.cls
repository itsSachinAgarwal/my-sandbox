public class XMLStreamReaderContacts {
public	list<contact> parseContacts(XmlStreamReader reader)
    {
        list<contact> listOfContacts=new list<contact>();
        while(reader.hasNext())
        {
            if(reader.getEventType()==XMLTag.START_ELEMENT)
            {
                if(reader.getLocalName()=='contact')
                {
                    Contact singleContact=parseContact(reader);
                    listOfContacts.add(singleContact);
                }
            }
            reader.next();
        }
        return listOfContacts;
    }
    
    Contact parseContact(XmlStreamReader reader)
    {
        Contact ct=new Contact();
        while(reader.hasNext())
        {
            if (reader.getEventType() == XmlTag.END_ELEMENT) 
            {
           		break;
        	} 
           else if(reader.getEventType() == XMLTag.START_ELEMENT)
            {
                if(reader.getLocalName()=='description')
                {
                    ct.description=parseContactFields(reader);
                }
                if(reader.getLocalName()=='email') 
                {
                    ct.Email=parseContactFields(reader);
                }
                if(reader.getLocalName()=='experience')
                {
                    ct.Experience__c=Decimal.valueof(parseContactFields(reader));
                }
                if(reader.getLocalName()=='mobilephone')
                {
                    ct.MobilePhone=parseContactFields(reader);
                }
                if(reader.getLocalName()=='lastname')
                {
                    ct.lastname=parseContactFields(reader);
                }
            }
            reader.next();
        }
        return ct;
	}

	String parseContactFields(XmlStreamReader reader)
    {
        String value=null;
        while(reader.hasNext()) 
        {
        	if (reader.getEventType() == XmlTag.END_ELEMENT) 
            {
           		break;
        	}
            else if (reader.getEventType() == XmlTag.CHARACTERS) 
            {
           		value = reader.getText();
        	}
        	reader.next();
     	}
        return value;
    }
}