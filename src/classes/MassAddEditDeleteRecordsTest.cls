/**
  Test class for validating the test code coverage for MassAddEditDeleteRecordsController
  and MassAddEditDeleteRecordsUtil
 */
@isTest
private class MassAddEditDeleteRecordsTest {
	static Account account; 
	
	// Creating test data
	static void createTestData() {
		account = new Account();
		account.Name = 'Test New Account#01';
		account.BillingCountry = 'Sweden';
		insert account;
		
		Contact contact1 = new Contact(Lastname = 'Contact#1', Firstname = 'Test New#1', AccountId = account.Id, Phone = '12345001', Description = 'Test contact #1');
		Contact contact2 = new Contact(Lastname = 'Contact#2', Firstname = 'Test New#2', AccountId = account.Id, Phone = '34599002', Description = 'Test contact #2');
		Contact contact3 = new Contact(Lastname = 'Contact#3', Firstname = 'Test New#3', AccountId = account.Id, Phone = '56721006', Description = 'Test contact #3');
		Contact contact4 = new Contact(Lastname = 'Contact#4', Firstname = 'Test New#4', AccountId = account.Id, Phone = '41780051', Description = 'Test contact #4');
		Contact contact5 = new Contact(Lastname = 'Contact#5', Firstname = 'Test New#5', AccountId = account.Id, Phone = '70087310', Description = 'Test contact #5');
		Contact contact6 = new Contact(Lastname = 'Contact#6', Firstname = 'Test New#6', AccountId = account.Id, Phone = '90087610', Description = 'Test contact #6');
		insert new List<Contact>{contact1, contact2, contact3, contact4, contact5, contact6};
	}
	
	/*
		init method positive test
	*/
    static testMethod void initTest() {
        createTestData();
        
        Test.setCurrentPage(Page.MassAddEditDeleteRecords);
        
        ApexPages.currentPage().getParameters().put('parentId', account.Id);
        ApexPages.currentPage().getParameters().put('childObject', 'Contact');
        ApexPages.currentPage().getParameters().put('parentField', 'AccountId');
        ApexPages.currentPage().getParameters().put('rowSize', '8');
        ApexPages.currentPage().getParameters().put('addMore', '2');
        
        // Field set on contact.
        ApexPages.currentPage().getParameters().put('fieldSet', 'Add_Edit_Delete_Contact_Fieldset');
        
        Test.startTest();
        MassAddEditDeleteRecordsController controller = new MassAddEditDeleteRecordsController();
        
        system.assert(controller.isEverythingOk);
        system.assertEquals(account.Name, controller.parentRecordName);
        system.assert(controller.childSObjectMockUp != null);
        system.assert(controller.fieldSetMemberList != null);
        
        system.assert(controller.sObjectRowWrapperList != null);
        system.assertEquals(8, controller.sObjectRowWrapperList.size());
        
        Test.stopTest();
    }
    
    /*
		init method with missing parentId parameter. 
	*/
    static testMethod void initWithMissingParentIdParameterTest() {
        createTestData();
        
        Test.setCurrentPage(Page.MassAddEditDeleteRecords);
        
        ApexPages.currentPage().getParameters().put('childObject', 'Contact');
        ApexPages.currentPage().getParameters().put('parentField', 'AccountId');
        ApexPages.currentPage().getParameters().put('rowSize', '8');
        ApexPages.currentPage().getParameters().put('addMore', '2');
        
        // Field set on contact.
        ApexPages.currentPage().getParameters().put('fieldSet', 'Add_Edit_Delete_Contact_Fieldset');
        
        Test.startTest();
        MassAddEditDeleteRecordsController controller = new MassAddEditDeleteRecordsController();
        
        system.assert(!controller.isEverythingOk);
        
        system.assert(Apexpages.hasMessages());
        Apexpages.Message message1 = Apexpages.getMessages().get(0);
        
        system.assert(message1.getDetail().contains(system.label.Mass_Add_Edit_Delete_Error_Required_parameter_missing_parentId));
        system.assertEquals(ApexPages.Severity.ERROR, message1.getSeverity());
        
        Test.stopTest();
    }
    
    /*
		init method with missing childObject parameter. 
	*/
    static testMethod void initWithMissingChildObjectParameterTest() {
        createTestData();
        
        Test.setCurrentPage(Page.MassAddEditDeleteRecords);
        
        ApexPages.currentPage().getParameters().put('parentId', account.Id);
        //ApexPages.currentPage().getParameters().put('childObject', 'Contact');
        ApexPages.currentPage().getParameters().put('parentField', 'AccountId');
        ApexPages.currentPage().getParameters().put('rowSize', '8');
        ApexPages.currentPage().getParameters().put('addMore', '2');
        
        // Field set on contact.
        ApexPages.currentPage().getParameters().put('fieldSet', 'Add_Edit_Delete_Contact_Fieldset');
        
        Test.startTest();
        MassAddEditDeleteRecordsController controller = new MassAddEditDeleteRecordsController();
        
        system.assert(!controller.isEverythingOk);
        
        system.assert(Apexpages.hasMessages());
        Apexpages.Message message1 = Apexpages.getMessages().get(0);
        
        system.assert(message1.getDetail().contains(system.label.Mass_Add_Edit_Delete_Error_Required_parameter_missing_childObject));
        system.assertEquals(ApexPages.Severity.ERROR, message1.getSeverity());
        
        Test.stopTest();
    }
    
    /*
		init method with missing parentField parameter. 
	*/
    static testMethod void initWithMissingParentFieldParameterTest() {
        createTestData();
        
        Test.setCurrentPage(Page.MassAddEditDeleteRecords);
        
        ApexPages.currentPage().getParameters().put('parentId', account.Id);
        ApexPages.currentPage().getParameters().put('childObject', 'Contact');
        //ApexPages.currentPage().getParameters().put('parentField', 'AccountId');
        ApexPages.currentPage().getParameters().put('rowSize', '8');
        ApexPages.currentPage().getParameters().put('addMore', '2');
        
        // Field set on contact.
        ApexPages.currentPage().getParameters().put('fieldSet', 'Add_Edit_Delete_Contact_Fieldset');
        
        Test.startTest();
        MassAddEditDeleteRecordsController controller = new MassAddEditDeleteRecordsController();
        
        system.assert(!controller.isEverythingOk);
        
        system.assert(Apexpages.hasMessages());
        Apexpages.Message message1 = Apexpages.getMessages().get(0);
        
        system.assert(message1.getDetail().contains(system.label.Mass_Add_Edit_Delete_Error_Required_parameter_missing_parentField));
        system.assertEquals(ApexPages.Severity.ERROR, message1.getSeverity());
        
        Test.stopTest();
    }
    
    /*
		init method with missing fieldset parameter.  
	*/
    static testMethod void initWithMissingFieldSetParameterTest() {
        createTestData();
        
        Test.setCurrentPage(Page.MassAddEditDeleteRecords);
        
        ApexPages.currentPage().getParameters().put('parentId', account.Id);
        ApexPages.currentPage().getParameters().put('childObject', 'Contact');
        ApexPages.currentPage().getParameters().put('parentField', 'AccountId');
        ApexPages.currentPage().getParameters().put('rowSize', '8');
        ApexPages.currentPage().getParameters().put('addMore', '2');
        
        Test.startTest();
        MassAddEditDeleteRecordsController controller = new MassAddEditDeleteRecordsController();
        
        system.assert(!controller.isEverythingOk);
        
        system.assert(Apexpages.hasMessages());
        Apexpages.Message message1 = Apexpages.getMessages().get(0);
        
        system.assert(message1.getDetail().contains(system.label.Mass_Add_Edit_Delete_Required_parameter_missing_fieldSet));
        system.assertEquals(ApexPages.Severity.ERROR, message1.getSeverity());
        
        Test.stopTest();
    }
    
    /*
		saveAll method positive test
	*/
    static testMethod void saveAllTest() {
        createTestData();
        
        Test.setCurrentPage(Page.MassAddEditDeleteRecords);
        
        ApexPages.currentPage().getParameters().put('parentId', account.Id);
        ApexPages.currentPage().getParameters().put('childObject', 'Contact');
        ApexPages.currentPage().getParameters().put('parentField', 'AccountId');
        ApexPages.currentPage().getParameters().put('rowSize', '8');
        ApexPages.currentPage().getParameters().put('addMore', '2');
        
        // Field set on contact.
        ApexPages.currentPage().getParameters().put('fieldSet', 'Add_Edit_Delete_Contact_Fieldset');
        
        Test.startTest();
        MassAddEditDeleteRecordsController controller = new MassAddEditDeleteRecordsController();
        
        system.assert(controller.isEverythingOk);
        system.assertEquals(account.Name, controller.parentRecordName);
        system.assert(controller.childSObjectMockUp != null);
        system.assert(controller.fieldSetMemberList != null);
        system.assert(controller.sObjectRowWrapperList != null);
        system.assertEquals(8, controller.sObjectRowWrapperList.size());
        
        String updateRecordId = controller.sObjectRowWrapperList.get(0).record.Id;
        
        system.assertEquals('Saved', controller.sObjectRowWrapperList.get(0).rowStatus);
        controller.sObjectRowWrapperList.get(0).record.put('description', 'Update description text');
        controller.sObjectRowWrapperList.get(0).isRowdirty = true;
        
        List<Contact> contacts = [Select Id from Contact where AccountId =: account.Id];
        system.assertEquals(6, contacts.size());
        
        system.assert(String.isBlank(controller.sObjectRowWrapperList.get(7).record.id));
        // Setting details for blank contact record.
        controller.sObjectRowWrapperList.get(7).record.put('LastName', 'New Contact#7');
        controller.sObjectRowWrapperList.get(7).record.put('AccountId', account.Id);
        controller.sObjectRowWrapperList.get(7).record.put('description', 'New description text for New record');
        controller.sObjectRowWrapperList.get(7).isRowdirty = true;
        
        controller.saveAll();
        
        system.assert(Apexpages.hasMessages());
        Apexpages.Message message1 = Apexpages.getMessages().get(0);
        system.assert(message1.getDetail().contains(system.label.Mass_Add_Edit_Delete_Info_Message_All_records_saved_Successfully));
        system.assertEquals(ApexPages.Severity.INFO, message1.getSeverity());
        
        contacts = [Select Id from Contact where AccountId =: account.Id];
        // One addition record created. so total count as 7 
        system.assertEquals(7, contacts.size());
        
        Contact updatedContact = [Select Id, Name, description from Contact where id =: updateRecordId];
        system.assertEquals('Update description text', updatedContact.description);
        
        Pagereference ref = controller.cancel();
        system.assert(ref != null);
        
        Test.stopTest();
    }
    
    /*
		addNewRows and deleteRow method positive test
	*/
    static testMethod void addAndDeleteRowsTest() {
        createTestData();
        
        Test.setCurrentPage(Page.MassAddEditDeleteRecords);
        
        ApexPages.currentPage().getParameters().put('parentId', account.Id);
        ApexPages.currentPage().getParameters().put('childObject', 'Contact');
        ApexPages.currentPage().getParameters().put('parentField', 'AccountId');
        ApexPages.currentPage().getParameters().put('rowSize', '8');
        ApexPages.currentPage().getParameters().put('addMore', '2');
        
        // Field set on contact.
        ApexPages.currentPage().getParameters().put('fieldSet', 'Add_Edit_Delete_Contact_Fieldset');
        
        Test.startTest();
        MassAddEditDeleteRecordsController controller = new MassAddEditDeleteRecordsController();
        
        system.assert(controller.isEverythingOk);
        system.assertEquals(account.Name, controller.parentRecordName);
        system.assert(controller.childSObjectMockUp != null);
        system.assert(controller.fieldSetMemberList != null);
        system.assert(controller.sObjectRowWrapperList != null);
        system.assertEquals(8, controller.sObjectRowWrapperList.size());
        
        // Add New rows will add 2 blank rows
        controller.addNewRows();
        system.assertEquals(10, controller.sObjectRowWrapperList.size());
        
        String deletedRecordId = controller.sObjectRowWrapperList.get(0).record.Id;
        controller.selectedRowIndexToDelete = ''+controller.sObjectRowWrapperList.get(0).rowIndex;
        
        controller.deleteRow();
        system.assertEquals(9, controller.sObjectRowWrapperList.size());
        
        List<Contact> deletedContact = [Select Id, Name, description from Contact where id =: deletedRecordId];
        // No record found for the recordId that being deleted above.
        system.assert(deletedContact.isEmpty());
        
        // Selecting 3 row for delete
        controller.sObjectRowWrapperList.get(0).isSelected = true;
        controller.sObjectRowWrapperList.get(1).isSelected = true;
        controller.sObjectRowWrapperList.get(8).isSelected = true;
        
        controller.deleteSelectedRows();
        system.assertEquals(6, controller.sObjectRowWrapperList.size());
        
        Test.stopTest();
    }
    
    /*
		updateFieldValueToRows method test
	*/
    static testMethod void massFieldUpdateTest() {
        createTestData();
        
        Test.setCurrentPage(Page.MassAddEditDeleteRecords);
        
        ApexPages.currentPage().getParameters().put('parentId', account.Id);
        ApexPages.currentPage().getParameters().put('childObject', 'Contact');
        ApexPages.currentPage().getParameters().put('parentField', 'AccountId');
        ApexPages.currentPage().getParameters().put('rowSize', '8');
        ApexPages.currentPage().getParameters().put('addMore', '2');
        
        // Field set on contact.
        ApexPages.currentPage().getParameters().put('fieldSet', 'Add_Edit_Delete_Contact_Fieldset');
        
        Test.startTest();
        MassAddEditDeleteRecordsController controller = new MassAddEditDeleteRecordsController();
        
        system.assert(controller.isEverythingOk);
        system.assertEquals(account.Name, controller.parentRecordName);
        system.assert(controller.childSObjectMockUp != null);
        system.assert(!controller.showEditFieldPopup);
        system.assert(controller.fieldSetMemberList != null);
        system.assert(controller.sObjectRowWrapperList != null);
        system.assertEquals(8, controller.sObjectRowWrapperList.size());
        
        system.assert(String.isBlank(controller.selectedFieldForMassUpdate));
        controller.selectedFieldForMassUpdate = 'Phone';
        
        controller.showEditFieldPopup();
        system.assert(controller.showEditFieldPopup);
        system.assertEquals('Phone', controller.selectedFieldForMassUpdate);
        
        controller.childSObjectMockUp.put('Phone', '100980088');
        controller.updateFieldValueToRows();
        system.assert(!controller.showEditFieldPopup);
        system.assert(String.isBlank(controller.selectedFieldForMassUpdate));
        
        // Phone field Value of all rows will be set as '100980088'
        for(MassAddEditDeleteRecordsUtil.SObjectRowWrapper rw : controller.sObjectRowWrapperList) {
        	system.assertEquals('100980088', rw.record.get('Phone'));
        }
        
        controller.selectedFieldForMassUpdate = 'Description';
        controller.showEditFieldPopup();
        system.assert(controller.showEditFieldPopup);
        system.assertEquals('Description', controller.selectedFieldForMassUpdate);
        
        controller.closeEditFieldPopup();
     	system.assert(!controller.showEditFieldPopup);
        system.assert(String.isBlank(controller.selectedFieldForMassUpdate));
        
     	Test.stopTest();   
    }
}