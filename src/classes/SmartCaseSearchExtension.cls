public with sharing class SmartCaseSearchExtension {
    
    public String caseNumber {get;set;}
    public String caseCreatedDateStart {get;set;}
    public String caseCreatedDateEnd {get;set;}
    public String accountName {get;set;}
    public Boolean ownedByMe {get;set;}
    public String accountZipCode {get;set;}
    public Boolean orAndAccZipCountry {get;set;}
    public String accountCountry {get;set;}
    public Boolean orAndAccCountryStreet {get;set;}
    public String accountStreet {get;set;}
    public String contactName {get;set;}
    public String contactEmail {get;set;}
    public String contactPhoneNo {get;set;}
    public String subjectKeyword {get;set;}
    public String emailBodyKeyword {get;set;}
    public String contactZipCode {get;set;}
    public Boolean orAndCtZipCountry {get;set;}
    public String contactCountry {get;set;}
    public Boolean orAndCtCountryStreet {get;set;}
    public String contactStreet {get;set;}
	public Case firstCase{get;set;}
	public Case SecondCase{get;set;}
    public String resetTextBoxName {set; get;}
     
    public integer searchCount{set; get;}
    public string searchStatus{set; get;}
    public string sortField{set;get;}
    private string previousSortField;
    private string sortOrder;
    public integer limitRecords{set; get;}   
    public boolean isAsc{set; get;}
    public Integer showingFrom{get;set;}
    public Integer showingTo{get;set;}
    public string query ;    
    public boolean showCaseButton{set; get;}
    public boolean hasNext{get;set;}
    public boolean hasPrevious{get;set;}
    public String requestedPage {get;set;}
    
    public integer totalResults {set; get;}
    
    public Integer totalPage {set; get;}
    public String casePrefix{set;get;}
    public String  OtherPageCalling {get;set;}
    public boolean showAllCaseButton{set; get;}
    
    public ApexPages.StandardSetController caseResults;
     
    public Boolean isBroaderSearchOn{get;set;}
    
    //Constructor
    public SmartCaseSearchExtension(ApexPages.StandardController controller) { 
        isBroaderSearchOn = true;   
        ownedByMe = false;
        resetSearchStatus();
        getcasePrefix();
        firstCase = new Case();
        secondCase = new Case();   
    }    
    public SmartCaseSearchExtension() {           
        resetSearchStatus();
        getcasePrefix();          
    }

    /*@jazzyrocksr : Make a describe call get KeyPrefix for Contact Object to build dynamic URLs*/
    private void getcasePrefix(){        
        Schema.DescribeSObjectResult dsr = Case.SObjectType.getDescribe();
        casePrefix=dsr.getKeyPrefix();
    }
    //set to default status of page
    public void resetSearchStatus(){        
        //Reset Contact fields
        showCaseButton = false;
        //cases  = new List<Contact>();
        searchCount = 0;
        searchStatus = '';
        sortOrder = IConstants.DEFAULT_SORT_ORDER;
        sortField = 'CaseNumber';
        previousSortField = 'CaseNumber';
        
        if(resetTextBoxName == ''){
            caseNumber = '';
            firstCase.testing__c = null;
            secondCase.testing__c = null;
            ownedByMe = false;
            orAndAccZipCountry = false;
            orAndAccCountryStreet = false;
            orAndCtZipCountry = false;
            orAndCtCountryStreet = false;
            accountName = '';
            accountZipCode = '';
            accountCountry = '';
            accountStreet = '';
            contactName = '';
            contactEmail = '';
            contactPhoneNo = '';
            subjectKeyword = '';
            emailBodyKeyword = '';
            contactZipCode = '';
            contactCountry = '';
            contactStreet = '';
        }
        isAsc = true;
        hasPrevious = false;
        hasNext = false;//if(leadSearch!=null) leadSearch.resetSearchStatus();           
    }
    
    public list<Case> cases {
        get{
            return cases;
        }set;
    }
    
    public PageReference cancel(){ 
        Pagereference pg = null;         
        return pg;  
        
    }
    
    public String findSearchCondition(String query){
    	//caseNumber == null  && domesticPhoneNo == null && internationalPhoneNo == null && contactEmail == null && domain == null  && 
          // contactName == null && contactAddress == null && subjectKeyword == null && emailBodyKeyword == null && ownedByMe == null  && coreAccountNo == null
        //Searching case number     
		if(caseNumber != null && caseNumber != ''){
        	String caseNumberCopy = caseNumber.replace('*', '%').replace('?', '_');
          	caseNumberCopy = String.escapeSingleQuotes(caseNumberCopy);
          	if(caseNumber.contains('*') || caseNumber.contains('?')){
          		if(query.toUpperCase().contains('WHERE')){
	            	query += ' AND CaseNumber like \'' + caseNumberCopy.Trim() + '\'';
	          	}else{
	            	query += ' WHERE CaseNumber like \'' + caseNumberCopy.Trim() +  '\'';
	          	}
          	}
          	else{
          		if(query.toUpperCase().contains('WHERE')){
	            	query += ' AND CaseNumber like \'%' + caseNumberCopy.Trim() + '%\'';
	          	}else{
	            	query += ' WHERE CaseNumber like \'%' + caseNumberCopy.Trim() +  '%\'';
	          	}
          	}
      	}
      	//Searching account name
      	if(accountName != null && accountName != ''){
        	String accountNameCopy = accountName.replace('*', '%').replace('?', '_');
          	accountNameCopy = String.escapeSingleQuotes(accountNameCopy);
          	if(accountName.contains('*') || accountName.contains('?')){
          		if(query.toUpperCase().contains('WHERE')){
	            	query += ' AND Account.Name like \'' + accountNameCopy.Trim() + '\'';
	          	}else{
	            	query += ' WHERE Account.Name like \'' + accountNameCopy.Trim() +  '\'';
	          	}
          	}
          	else{
          		if(query.toUpperCase().contains('WHERE')){
	            	query += ' AND Account.Name like \'%' + accountNameCopy.Trim() + '%\'';
	          	}else{
	            	query += ' WHERE Account.Name like \'%' + accountNameCopy.Trim() +  '%\'';
	          	}
          	}
      	}
      	
      	//Searching account name
      	if(accountZipCode != null && accountZipCode != ''){
        	String accountZipCodeCopy = accountZipCode.replace('*', '%').replace('?', '_');
          	accountZipCodeCopy = String.escapeSingleQuotes(accountZipCodeCopy);
          	if(accountZipCode.contains('*') || accountZipCode.contains('?')){
          		if(query.toUpperCase().contains('WHERE')){
	            	query += ' AND (Account.BillingPostalCode like \'' + accountZipCodeCopy.Trim() + '\'';
	          	}else{
	            	query += ' WHERE (Account.BillingPostalCode like \'' + accountZipCodeCopy.Trim() +  '\'';
	          	}
          	}
          	else{
          		if(query.toUpperCase().contains('WHERE')){
	            	query += ' AND (Account.BillingPostalCode  like \'%' + accountZipCodeCopy.Trim() + '%\'';
	          	}else{
	            	query += ' WHERE (Account.BillingPostalCode like \'%' + accountZipCodeCopy.Trim() +  '%\'';
	          	}
          	}
      	}
      	
      	//Searching account country
      	if(accountCountry != null && accountCountry != ''){
        	String accountCountryCopy = accountCountry.replace('*', '%').replace('?', '_');
          	accountCountryCopy = String.escapeSingleQuotes(accountCountryCopy);
          	if(accountCountry.contains('*') || accountCountry.contains('?')){
	          	if(query.toUpperCase().contains('WHERE')){
          			if(query.containsIgnoreCase('Account.BillingPostalCode')){
          				if(orAndAccZipCountry == true){
          					query += ' OR Account.BillingCountry  like \'' + accountCountryCopy.Trim() + '\'';
          				}
          				else{
          					query += ' AND Account.BillingCountry  like \'' + accountCountryCopy.Trim() + '\'';
          				}
          			}
          			else{
          				query += ' AND (Account.BillingCountry  like \'' + accountCountryCopy.Trim() + '\'';
          			}
	          	}else{
	            	query += ' WHERE (Account.BillingCountry like \'' + accountCountryCopy.Trim() +  '\'';
	          	}
          	}
          	else{
          		if(query.toUpperCase().contains('WHERE')){
          			if(query.containsIgnoreCase('Account.BillingPostalCode')){
          				if(orAndAccZipCountry == true){
          					query += ' OR Account.BillingCountry  like \'%' + accountCountryCopy.Trim() + '%\'';
          				}
          				else{
          					query += ' AND Account.BillingCountry  like \'%' + accountCountryCopy.Trim() + '%\'';
          				}
          			}
          			else{
          				query += ' AND (Account.BillingCountry  like \'%' + accountCountryCopy.Trim() + '%\'';
          			}
	          	}else{
	            	query += ' WHERE (Account.BillingCountry like \'%' + accountCountryCopy.Trim() +  '%\'';
	          	}
          	}
      	}
      	
      	//Searching account street
      	if(accountStreet != null && accountStreet != ''){
        	String accountStreetCopy = accountStreet.replace('*', '%').replace('?', '_');
          	accountStreetCopy = String.escapeSingleQuotes(accountStreetCopy);
          	if(accountStreet.contains('*') || accountStreet.contains('?')){
          		
	          	if(query.toUpperCase().contains('WHERE')){
          			if(query.containsIgnoreCase('Account.BillingPostalCode') || query.containsIgnoreCase('Account.BillingCountry')){
          				if(orAndAccCountryStreet == true){
          					query += ' OR Account.BillingStreet  like \'' + accountStreetCopy.Trim() + '\'';
          				}
          				else{
          					query += ' AND Account.BillingStreet  like \'' + accountStreetCopy.Trim() + '\'';
          				}
          			}
          			else{
          				query += ' AND (Account.BillingStreet  like \'' + accountStreetCopy.Trim() + '\'';
          			}
	          	}else{
	            	query += ' WHERE (Account.BillingStreet like \'' + accountStreetCopy.Trim() +  '\'';
	          	}
          	}
          	else{
          		if(query.toUpperCase().contains('WHERE')){
          			if(query.containsIgnoreCase('Account.BillingPostalCode') || query.containsIgnoreCase('Account.BillingCountry')){
          				if(orAndAccCountryStreet == true){
          					query += ' OR Account.BillingStreet  like \'%' + accountStreetCopy.Trim() + '%\'';
          				}
          				else{
          					query += ' AND Account.BillingStreet  like \'%' + accountStreetCopy.Trim() + '%\'';
          				}
          			}
          			else{
          				query += ' AND (Account.BillingStreet  like \'%' + accountStreetCopy.Trim() + '%\'';
          			}
	          	}else{
	            	query += ' WHERE (Account.BillingStreet like \'%' + accountStreetCopy.Trim() +  '%\'';
	          	}
          	}
      	}
      	if(query.containsIgnoreCase('Account.BillingPostalCode') || query.containsIgnoreCase('Account.BillingCountry') || query.containsIgnoreCase('Account.BillingStreet')){
      		query += ' )';
      	}
      	//Searching contact email
      	if(contactEmail != null && contactEmail != ''){
          	String contactEmailCopy = contactEmail.replace('*', '%').replace('?', '_');
          	contactEmailCopy = String.escapeSingleQuotes(contactEmailCopy);
          	if(contactEmail.contains('*') || contactEmail.contains('?')){
          		if(query.toUpperCase().contains('WHERE')){
	            	query += ' AND Contact.Email like \'' + contactEmailCopy.Trim() + '\'';
	          	}else{
	            	query += ' WHERE Contact.Email like \'' + contactEmailCopy.Trim() +  '\'';
	          	}
          	}
          	else{
	          	if(query.toUpperCase().contains('WHERE')){
	           		query += ' AND Contact.Email like \'%' + contactEmailCopy.Trim() + '%\'';
	          	}else{
	            	query += ' WHERE Contact.Email like \'%' + contactEmailCopy.Trim() +  '%\'';
	          	}
          	}
      	}
      	//Searching contact name
      	if(contactName != null && contactName != ''){
        	String contactNameCopy = contactName.replace('*', '%').replace('?', '_');
          	contactNameCopy = String.escapeSingleQuotes(contactNameCopy);
          	if(contactName.contains('*') || contactName.contains('?')){
          		if(query.toUpperCase().contains('WHERE')){
	            	query += ' AND Contact.Name like \'' + contactNameCopy.Trim() + '\'';
	          	}else{
	            	query += ' WHERE Contact.Name like \'' + contactNameCopy.Trim() +  '\'';
	          	}
          	}
          	else{
          		if(query.toUpperCase().contains('WHERE')){
	            	query += ' AND Contact.Name like \'%' + contactNameCopy.Trim() + '%\'';
	          	}else{
	            	query += ' WHERE Contact.Name like \'%' + contactNameCopy.Trim() +  '%\'';
	          	}
          	}
      	}
      	//Searching contact phone
      	if(contactPhoneNo != null && contactPhoneNo != ''){
        	String contactPhoneNoCopy = contactPhoneNo.replace('*', '%').replace('?', '_');
          	contactPhoneNoCopy = String.escapeSingleQuotes(contactPhoneNoCopy);
          	if(contactPhoneNo.contains('*') || contactPhoneNo.contains('?')){
          		if(query.toUpperCase().contains('WHERE')){
	            	query += ' AND Contact.Phone like \'' + contactPhoneNoCopy.Trim() + '\'';
	          	}else{
	            	query += ' WHERE Contact.Phone like \'' + contactPhoneNoCopy.Trim() +  '\'';
	          	}
          	}
          	else{
          		if(query.toUpperCase().contains('WHERE')){
	            	query += ' AND Contact.Phone like \'%' + contactPhoneNoCopy.Trim() + '%\'';
	          	}else{
	            	query += ' WHERE Contact.Phone like \'%' + contactPhoneNoCopy.Trim() +  '%\'';
	          	}
          	}
      	}
      	
      	//Searching contact phone
      	if(firstCase.testing__c != null && secondCase.testing__c != null){
      		if(query.toUpperCase().contains('WHERE')){
      			query += ' AND CreatedDate >= ' + firstCase.testing__c.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'') + ' AND CreatedDate <= ' + secondCase.testing__c.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
      		}
      		else{
      			query += ' WHERE CreatedDate >= ' + firstCase.testing__c.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'') + ' AND CreatedDate <= ' + secondCase.testing__c.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
      		}
      	}
      	else if(firstCase.testing__c == null && secondCase.testing__c != null){
      		if(query.toUpperCase().contains('WHERE')){
      			query += ' AND CreatedDate <= ' + secondCase.testing__c.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
      		}
      		else{
      			query += ' WHERE CreatedDate <= ' + secondCase.testing__c.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
      		}
      	}
      	else if(firstCase.testing__c != null && secondCase.testing__c == null){
      		if(query.toUpperCase().contains('WHERE')){
      			query += ' AND CreatedDate >= ' + firstCase.testing__c.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
      		}
      		else{
      			query += ' WHERE CreatedDate >= ' + firstCase.testing__c.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
      		}
      	}
      	//Searching subject keyword
      	if(subjectKeyword != null && subjectKeyword != ''){
        	String subjectKeywordCopy = subjectKeyword.replace('*', '%').replace('?', '_');
          	subjectKeywordCopy = String.escapeSingleQuotes(subjectKeywordCopy);
          	if(subjectKeyword.contains('*') || subjectKeyword.contains('?')){
          		if(query.toUpperCase().contains('WHERE')){
	            	query += ' AND Subject like \'' + subjectKeywordCopy.Trim() + '\'';
	          	}else{
	            	query += ' WHERE Subject like \'' + subjectKeywordCopy.Trim() +  '\'';
	          	}
          	}
          	else{
          		if(query.toUpperCase().contains('WHERE')){
	            	query += ' AND Subject like \'%' + subjectKeywordCopy.Trim() + '%\'';
	          	}else{
	            	query += ' WHERE Subject like \'%' + subjectKeywordCopy.Trim() +  '%\'';
	          	}
          	}
      	}
      	//Searching email body keyword
      	if(emailBodyKeyword != null && emailBodyKeyword != ''){
        	String emailBodyKeywordCopy = emailBodyKeyword.replace('*', '%').replace('?', '_');
          	emailBodyKeywordCopy = String.escapeSingleQuotes(emailBodyKeywordCopy);
          	if(emailBodyKeyword.contains('*') || emailBodyKeyword.contains('?')){
          		if(query.toUpperCase().contains('WHERE')){
	            	query += ' AND Description like \'' + emailBodyKeywordCopy.Trim() + '\'';
	          	}else{
	            	query += ' WHERE Description like \'' + emailBodyKeywordCopy.Trim() +  '\'';
	          	}
          	}
          	else{
          		if(query.toUpperCase().contains('WHERE')){
	            	query += ' AND Description like \'%' + emailBodyKeywordCopy.Trim() + '%\'';
	          	}else{
	            	query += ' WHERE Description like \'%' + emailBodyKeywordCopy.Trim() +  '%\'';
	          	}
          	}
      	}
      	//Searching on the owner basis
      	if(ownedByMe != null && ownedByMe == true){
        	String currentUserId = UserInfo.getUserId();
          	if(query.toUpperCase().contains('WHERE')){
            	query += ' AND ownerid = \'' + currentUserId + '\'';
          	}else{
            	query += ' WHERE ownerid = \'' + currentUserId +  '\'';
          	}
      	}
      	
      	//Searching contact name
      	if(contactZipCode != null && contactZipCode != ''){
        	String contactZipCodeCopy = contactZipCode.replace('*', '%').replace('?', '_');
          	contactZipCodeCopy = String.escapeSingleQuotes(contactZipCodeCopy);
          	if(contactZipCode.contains('*') || contactZipCode.contains('?')){
          		if(query.toUpperCase().contains('WHERE')){
	            	query += ' AND (Contact.MailingPostalCode like \'' + contactZipCodeCopy.Trim() + '\'';
	          	}else{
	            	query += ' WHERE (Contact.MailingPostalCode like \'' + contactZipCodeCopy.Trim() +  '\'';
	          	}
          	}
          	else{
          		if(query.toUpperCase().contains('WHERE')){
	            	query += ' AND (Contact.MailingPostalCode  like \'%' + contactZipCodeCopy.Trim() + '%\'';
	          	}else{
	            	query += ' WHERE (Contact.MailingPostalCode like \'%' + contactZipCodeCopy.Trim() +  '%\'';
	          	}
          	}
      	}
      	
      	//Searching contact country
      	if(contactCountry != null && contactCountry != ''){
        	String contactCountryCopy = contactCountry.replace('*', '%').replace('?', '_');
          	contactCountryCopy = String.escapeSingleQuotes(contactCountryCopy);
          	if(contactCountry.contains('*') || contactCountry.contains('?')){
	          	if(query.toUpperCase().contains('WHERE')){
          			if(query.containsIgnoreCase('Contact.MailingPostalCode')){
          				if(orAndCtZipCountry == true){
          					query += ' OR Contact.MailingCountry  like \'' + contactCountryCopy.Trim() + '\'';
          				}
          				else{
          					query += ' AND Contact.MailingCountry  like \'' + contactCountryCopy.Trim() + '\'';
          				}
          			}
          			else{
          				query += ' AND (Contact.MailingCountry  like \'' + contactCountryCopy.Trim() + '\'';
          			}
	          	}else{
	            	query += ' WHERE (Contact.MailingCountry like \'' + contactCountryCopy.Trim() +  '\'';
	          	}
          	}
          	else{
          		if(query.toUpperCase().contains('WHERE')){
          			if(query.containsIgnoreCase('Contact.MailingPostalCode')){
          				if(orAndCtZipCountry == true){
          					query += ' OR Contact.MailingCountry  like \'%' + contactCountryCopy.Trim() + '%\'';
          				}
          				else{
          					query += ' AND Contact.MailingCountry  like \'%' + contactCountryCopy.Trim() + '%\'';
          				}
          			}
          			else{
          				query += ' AND (Contact.MailingCountry  like \'%' + contactCountryCopy.Trim() + '%\'';
          			}
	          	}else{
	            	query += ' WHERE (Contact.MailingCountry like \'%' + contactCountryCopy.Trim() +  '%\'';
	          	}
          	}
      	}
      	
      	//Searching contact street
      	if(contactStreet != null && contactStreet != ''){
        	String contactStreetCopy = contactStreet.replace('*', '%').replace('?', '_');
          	contactStreetCopy = String.escapeSingleQuotes(contactStreetCopy);
          	if(contactStreet.contains('*') || contactStreet.contains('?')){
          		
	          	if(query.toUpperCase().contains('WHERE')){
          			if(query.containsIgnoreCase('Contact.MailingPostalCode') || query.containsIgnoreCase('Contact.MailingCountry')){
          				if(orAndCtCountryStreet == true){
          					query += ' OR Contact.MailingStreet  like \'' + contactStreetCopy.Trim() + '\'';
          				}
          				else{
          					query += ' AND Contact.MailingStreet  like \'' + contactStreetCopy.Trim() + '\'';
          				}
          			}
          			else{
          				query += ' AND (Contact.MailingStreet  like \'' + contactStreetCopy.Trim() + '\'';
          			}
	          	}else{
	            	query += ' WHERE (Contact.MailingStreet like \'' + contactStreetCopy.Trim() +  '\'';
	          	}
          	}
          	else{
          		if(query.toUpperCase().contains('WHERE')){
          			if(query.containsIgnoreCase('Contact.MailingPostalCode') || query.containsIgnoreCase('Contact.MailingCountry')){
          				if(orAndCtCountryStreet == true){
          					query += ' OR Contact.MailingStreet  like \'%' + contactStreetCopy.Trim() + '%\'';
          				}
          				else{
          					query += ' AND Contact.MailingStreet  like \'%' + contactStreetCopy.Trim() + '%\'';
          				}
          			}
          			else{
          				query += ' AND (Contact.MailingStreet  like \'%' + contactStreetCopy.Trim() + '%\'';
          			}
	          	}else{
	            	query += ' WHERE (Contact.MailingStreet like \'%' + contactStreetCopy.Trim() +  '%\'';
	          	}
          	}
      	}
      	if(query.containsIgnoreCase('Contact.MailingPostalCode') || query.containsIgnoreCase('Contact.MailingCountry') || query.containsIgnoreCase('Contact.MailingStreet')){
      		query += ' )';
      	}
      	system.debug('query======' + query );
    	return query;
  	}
    
    public void performSearch() {       
        system.debug('query======' + query );
         
        if(caseNumber == null  && firstCase.testing__c == null && secondCase.testing__c == null && accountName == null && ownedByMe == null  && 
           accountZipCode == null && orAndAccZipCountry == null && accountCountry == null && orAndAccCountryStreet == null && accountStreet == null  && contactName == null &&
           contactEmail == null && contactPhoneNo == null && subjectKeyword == null && emailBodyKeyword == null && contactZipCode == null  && orAndCtZipCountry == null &&
           contactCountry == null && orAndCtCountryStreet == null && contactStreet == null)
            return;
        
        searchContact();     
        
        System.debug('cases slist ::::::::::::' + cases);
    }
    
    //method to search Contact and make list according to pagesize
    private void searchContact(){
		showCaseButton = true;
        limitRecords=250;
        //List<Schema.FieldSetMember> fsm= Schema.SObjectType.Cases.FieldSets.Contact_Search_Result.getFields();
        String query = 'SELECT ';
        // for(Schema.FieldSetMember f : fsm) {
        //  query += f.getFieldPath() + ', ';
        //}
        query += 'Subject, OwnerId, Owner.Name, Description, Contact.MailingAddress, Contact.Email, Contact.Name, ContactId, CaseNumber, Account.Name, Contact.MobilePhone FROM Case';
        
		query = findSearchCondition(query);
        System.debug('QUERY+++++++++:' + query);
        query += ' ORDER BY ' + sortField + sortOrder + ' nulls last' + ' LIMIT ' + limitRecords ;
          
        try{                         
            cases = new list<Case>();
            system.debug('-------------- final query---------------'+ query);
            caseResults = new ApexPages.StandardSetController(Database.query(query));
            caseResults.setPageSize(IConstants.DEFAULT_RESULTS_PER_PAGE);
            cases = caseResults.getRecords();
           
            searchCount = caseResults.getResultSize();
            if (searchCount >= limitRecords) {
                searchStatus = 'Search returned more than ' + limitRecords + ' records. Please refine your search';
            }
        }catch(Exception e){
            searchCount = 0;
        }  
        if (searchCount  == 0){
            searchStatus = 'No Matching Results';
        }
        requestedPage = String.valueOf(caseResults.getPageNumber());
        showingFrom = 1;
        
        
        totalResults = 0;
        for (List<Sobject> recordBatch:Database.query(query))  {
             totalResults = totalResults + recordBatch.size();
         }
        totalPage = 0;
        totalPage = totalResults / caseResults.getPageSize() ; 
        if (totalPage * caseResults.getPageSize() < totalResults){
          totalPage++;
        }
        if(searchCount < caseResults.getPageSize()) {
            showingTo = searchCount;
        } else {
            showingTo = caseResults.getPageSize();
        }
        if(caseResults.getHasNext()) {
            hasNext = true;
        } else {
            hasNext = false;
        }
        hasPrevious = false;
    }
    
    public PageReference nextCasePage(){
        if(caseResults.getHasNext()) {
            cases = new List<Case>();
            caseResults.next();
            cases = caseResults.getRecords();
            showingFrom = showingFrom + caseResults.getPageSize();
            showingTo =  showingTo + cases.size();
            if(caseResults.getHasNext()) {
                hasNext = true;
            }
            else {
                hasNext = false;
            }
            hasPrevious = true; 
        }
        requestedPage = String.valueOf(caseResults.getPageNumber());
        return null;
    }  
    public PageReference previousCasePage(){
        if(caseResults.getHasPrevious()) {
            showingTo =  showingTo - cases.size();
            cases= new List<Case>();
            caseResults.previous();
            cases = caseResults.getRecords();
            showingFrom = showingFrom - caseResults.getPageSize();
            hasNext = true;
            if(caseResults.getHasPrevious()) {
                hasPrevious = true;
            } else {
                hasPrevious = false;
            }
        }
        requestedPage = String.valueOf(caseResults.getPageNumber());  
        return null;
    }
    public PageReference requestedCasePage(){
        
        boolean check = pattern.matches('[0-9]+', requestedPage); 
        Integer pageNo = check? Integer.valueOf(requestedPage) : 0;
        if(pageNo == 0 || pageNo > totalPage){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid page number')); 
            return null;       
        }  
        caseResults.setPageNumber(pageNo);
        cases = caseResults.getRecords();
        if(caseResults.getHasPrevious()) {
            hasPrevious = true;
         } else {
            hasPrevious = false;
         }
         if(caseResults.getHasNext()) {
            hasNext = true;
        } else {
            hasNext = false;
        }
        showingFrom  = (pageNo - 1) * caseResults.getPageSize() + 1;
       
        showingTo = showingFrom + cases.size() - 1;
        if(showingTo > totalResults) {
            showingTo = totalResults;
        }
        return null;
    }  
    //used to sort
    public void sortData(){
        if (previousSortField.equals(sortField)){
          isAsc = !isAsc;  
        }else{
            isAsc = true;
        }   
        sortOrder = isAsc ? IConstants.SORT_ASC_ORDER : IConstants.SORT_DESC_ORDER;
        previousSortField = sortField;
        searchContact();
    }
    
    public SmartCaseSearchExtension getThis() {
        return this;
    }
}