public class ContactExtension 
{
    public ID selectedContactId{get;set;}
    public string selectedCtName{get;set;}
    public list<Contact> listOfContacts{get;set;}
    List<SelectOption> options;
    public ContactExtension(ApexPages.StandardController controller)
    {		
        string managerid=ApexPages.currentPage().getParameters().get('manager');
        listOfContacts=[select id, name from contact where account.id=:managerid];
        
    }
     public List<SelectOption> getItems() {
        options = new List<SelectOption>(); 
        for(Contact singleContact:listOfContacts)
        {
            options.add(new SelectOption(singleContact.id,singleContact.name));     
        }       
        return options; 
    }
    public string getSelectedContactName()
    {
       for(SelectOption so:options)
       {
           if(so.getValue()== (String)selectedContactId)
           {
               selectedCtName=so.getLabel();
           }
       }
        return selectedCtName;
    }
}