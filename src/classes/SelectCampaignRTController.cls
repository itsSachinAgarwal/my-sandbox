/**=====================================================================
 * Appirio, Inc
 * Class Name               : SelectCampaignRTController
 * Description              : Controller for SelectCampaignRT.page
 * Created Date             : 06/26/2015
 * Created By               : Sachin Agarwal
 * 
 * Date Modified                Modified By             Description of the update
 * mm/dd/yyy                   FirsName LastName          Reason
 =====================================================================*/
public with sharing class SelectCampaignRTController {
    public Campaign cmp{get;set;}
    public FINAL String CAMPAIGN = 'Campaign';
    public FINAL String CAMPAIGN_MEMBER = 'CampaignMember';
    //Constructor
    public SelectCampaignRTController(){
        cmp = new Campaign();
    }
    
    //Method which will be called on clicking Continue button.
    //This will redirect the user to the Campaign edit page after calculating the suitable
    //campaign member record type
    public PageReference editWithRecordType() {
        PageReference nextPage = new PageReference('/701/e');//get your object 3 digit code here
        nextPage.getParameters().put('RecordType', cmp.RecordTypeId);
        nextPage.getParameters().put('ent', 'Campaign');//replace it with from your default url 
        nextPage.getParameters().put('retUrl', ApexPages.currentPage().getParameters().get('retUrl'));
        map<String, String> mapBetweenCampaingMemberRT = createCampaignRTMemberRTMap();
        if(mapBetweenCampaingMemberRT.containsKey(cmp.RecordTypeId)){
            nextPage.getParameters().put('p8', mapBetweenCampaingMemberRT.get(cmp.RecordTypeId));
            nextPage.getParameters().put('CMRT', mapBetweenCampaingMemberRT.get(cmp.RecordTypeId));
        }
        nextPage.getParameters().put('nooverride', '1');
        //nextPage.setRedirect(true);
        return nextPage;
    }
    //Method that will be called on clicking the cancel button.
    //This will redirect the user to original page
    public PageReference cancel(){
        PageReference pageRef;
        map<string,string> URLParameters = ApexPages.currentPage().getParameters();
        if(URLParameters.containsKey('retURL')){
            pageRef = new PageReference(URLParameters.get('retURL'));
        }       
        return pageRef;
    }
     //----------------------------------------------------------------------------------------------------------------
    //This method will create the mapping between Campaign RecordTypeIDs and CampaignMember RecordType IDs
    //----------------------------------------------------------------------------------------------------------------
    public map<String, String> createCampaignRTMemberRTMap(){
        map<String, CampaignRT_MemberRT_Mapping__c> fieldsMap = CampaignRT_MemberRT_Mapping__c.getAll();
        map<String, String> mapBetweenCampaingMemberRT =  new map<String, String>();
        ID campaignID;
        ID campaignMemberID;
        for(CampaignRT_MemberRT_Mapping__c obj : fieldsMap.values()){
            campaignID = getRecordTypeId(CAMPAIGN, obj.Campaign_RT__c);
            campaignMemberID = getRecordTypeId(CAMPAIGN_MEMBER, obj.Campaign_Member_RT__c);
            if(campaignID != null && campaignMemberID != null){
                mapBetweenCampaingMemberRT.put(campaignID, campaignMemberID);           
            }
        }
        return mapBetweenCampaingMemberRT;
    }
    //----------------------------------------------------------------------------------------------------------------
    // Get Record Type Id
    //----------------------------------------------------------------------------------------------------------------
    public Id getRecordTypeId(String sObjectName, String recTypeName){
        Map<String, Schema.SObjectType> GLOBAL_MAP = Schema.getGlobalDescribe();
        Schema.DescribeSObjectResult DescribeSObjectResultObj = GLOBAL_MAP.get(sObjectName).getDescribe();
        Map<String,Schema.RecordTypeInfo> rtMapByName = DescribeSObjectResultObj.getRecordTypeInfosByName();
        Id rcID =  rtMapByName != null && rtMapByName.containsKey(recTypeName) ?
                    rtMapByName.get(recTypeName).getRecordTypeId() : null;  
        return rcID;
    }
}