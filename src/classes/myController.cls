public with sharing class myController {
    @RemoteAction
    public static String getContactData(String name){
    	list<Contact> listOfCons = new list<Contact>([SELECT id, name from contact where name = :name]);
    	list<ContactWrapper> listOfCWs = new list<ContactWrapper>(); 
    	ContactWrapper cw;
    	for(Contact c : listOfCons){
    		listOfCWs.add(new ContactWrapper(c.id, c.name));
    	}
    	return JSON.serialize(listOfCWs);
    	
    }
    
    public class ContactWrapper{
    	String id;
    	String name;
    	
    	ContactWrapper(String ide, String namee){
    		id = ide;
    		name = namee;
    	}
    }
}