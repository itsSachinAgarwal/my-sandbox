/**
* Name:  OpenLookupPopupCtrl_Test
* Purpose: This is a test class for testing OpenLookupPopupCtrl                                                         
* History                                                           
* 
* VERSION  AUTHOR                           DATE                Purpose                    
* 1.0 -    Sachin Agarwal ( Appirio JDC)   28/04/2015      INITIAL DEVELOPMENT
**/

@isTest
private class OpenLookupPopupCtrl_Test {
	//----------------------------------------------------------------------------------------------------------
    // Method containing the main logic to test the class
    //----------------------------------------------------------------------------------------------------------
    static testMethod void unitTest() {
    	
    	Account newAccount = createAccount('Test Account', true);
    	
        PageReference pgRef = Page.OpenLookupPopup;
	    Test.setCurrentPageReference (pgRef);
	    ApexPages.CurrentPage().getParameters().put('Object', 'Account');
	    ApexPages.CurrentPage().getParameters().put('fieldName', 'Name');
	    ApexPages.CurrentPage().getParameters().put('searchText', 'Test Account');
	    ApexPages.CurrentPage().getParameters().put('fieldSetName', 'account_search_result');
	    OpenLookupPopupCtrl obj = new OpenLookupPopupCtrl();
	    
	    System.assertEquals(1, obj.listRecords.size()); 
    }
    //----------------------------------------------------------------------------------------------------------
    // Method being used to create sample Account
    //----------------------------------------------------------------------------------------------------------
	public static Account createAccount(String accountName, Boolean isInsert){
	    Account account = new Account();
	    account.Name = accountName;
	    if(isInsert){
	        insert account;
	    }
	    return account;
	}
}