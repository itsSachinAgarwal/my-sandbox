public class VisualforceCSVUploader_Con {
    public Blob csvFile{get;set;}
    public String csvFileName{get;set;}
    public String csvString{get;set;}
    public Integer failedCount{get;set;}
    public Integer successCount{get;set;}
    private String objectToInsert;
    
    /*
    *This method retrieves the config according to the TEMPLATE FILE NAME
    ***/
    public Map<String,String> getConfig(){
        Map<String,String> tempColumnToFieldMap = new Map<String,String>();
        //split the file name to remove the extension
        csvFileName = csvFileName.split('\\.')[0];
        //append a wild character for like search
        csvFileName = csvFileName+'%';
        //query for a matching active config record
        for(CSV_Uploader_Field_Mapping__c mapping :  [SELECT Id ,CSV_Column_Name__c,Field_API_Name__c,CSV_Uploader_Config__r.Object_API_Name__c FROM  CSV_Uploader_Field_Mapping__c WHERE (CSV_Uploader_Config__r.Template_Name__c LIKE:csvFileName OR CSV_Uploader_Config__r.Name LIKE:csvFileName) AND CSV_Uploader_Config__r.isActive__c=true]){
            tempColumnToFieldMap.put(mapping.CSV_Column_Name__c.toLowerCase(),mapping.Field_API_Name__c.toLowerCase());
            objectToInsert = mapping.CSV_Uploader_Config__r.Object_API_Name__c;
        }    
        return tempColumnToFieldMap;
    }
    
    public void upload(){
        if( csvFileName!=null && csvFileName!='' && csvFileName.endsWith('.csv')){
            //convert the blob to extract the csv body
            csvString = csvFile.toString();
            //assign null to blob to flush the memory
            csvFile = null;
            //initialize the counters
            successCount= failedCount=0;
            //reterive the config
            Map<String,String> columnToFieldMap = getConfig();
            //check if any active config is returned
            if(!columnToFieldMap.isEmpty()){
                
                //check if a valid object describe can be found
                Schema.sObjectType objectDef = Schema.getGlobalDescribe().get(objectToInsert );
                if(objectDef !=null){
                    try{
                        //parse the csv file
                        List<List<String>> parsedCSV = parseCSV(csvString,false);
                        //check if the template is valid
                        if(validateTemplate(getHeaders(parsedCSV ),columnToFieldMap )){    
                            //map the parsed csv into sobject       
                            list<sObject> objects = mapSobject(parsedCSV,columnToFieldMap,objectDef);     
                            list<sObject> objectsToInsert = new list<sObject>();
                            Integer counter =1;
                           
                            for(sObject sObj: objects ){
                                objectsToInsert.add(sObj);
                                //if there are large number of records,insert the records by breaking them into small chunks
                                if(Math.mod(counter,Limits.getLimitDMLRows())==0 || counter == objects.size()){
                                     //insert the records
                                     Database.SaveResult[] insertSaveResult = Database.Insert(objectsToInsert , false);
                                     objectsToInsert.clear();
                                     //check whether record were inserted or not
                                     for(Database.SaveResult sr:insertSaveResult ){
                                       if(!sr.isSuccess()){
                                           failedCount++;
                                       }
                                       else{
                                           successCount++;
                                       }
                                         
                                    }
                                }
                               
                                counter++; 
                            }
                            //show a confirmation message                   
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,successCount+' Were Successfully Inserted.'));
                            if(failedCount>0)ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,failedCount+' Were Not Inserted.'));
                        }
                        else{
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Invalid Template.'));
                        }
                    }
                    catch(Exception ex){
                        ApexPages.addMessages(ex);
                    }
                }
                else{
                    //if configuration is not valid
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Configuration Error No Object Found.Please Contact Your Administrator.'));
                }
            }
            else{
                //if no matching config found
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'No Active Config Found For This Template.Please Contact Your Administrator.'));
            }
        }
        else{
            csvFile = null;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'File Format Error.Please Select A CSV File.'));
        }
    }
    /*
    *This method returns the field name and field data type of a 
    *passed object
    ***/
    private Map<String,String> describeDestinationObj(String objectToInsert){
        Map<String,String> fieldToDataTypeMap = new Map<String,String>();
        Schema.SObjectType objectDesc = Schema.getGlobalDescribe().get(objectToInsert);
        Schema.DescribeSObjectResult objectDescResult = objectDesc.getDescribe();
        //describe fields and create a fieldname and soaptype map
        for(Schema.SObjectField schObjField :  objectDescResult.fields.getMap().values()){
            Schema.DescribeFieldResult fieldDescribe = schObjField.getDescribe();
            fieldToDataTypeMap.put(fieldDescribe.getName().toLowerCase(),String.valueOf(fieldDescribe.getSoapType()).toLowerCase());
        }
        return fieldToDataTypeMap ;
    }
    
     /*
    *This method returns the csv file header
    ***/
    private List<String> getHeaders(List<List<String>> parsedCSV){
       List<string> headers = new List<string>();
        
        for(List<string> row : parsedCSV){
            for(string col : row){
                headers.add(col.toLowerCase());
            }
            break;
        }
        return headers;
    }
    
    /*
    *
    ***/
    public Boolean validateTemplate(List<string> headers,Map<String,String> columnToFieldMap){
        Boolean flag = (true && !headers.isEmpty());
        
        for(string key: headers){
            flag = columnToFieldMap.containsKey(key.trim());
           
            if(flag==false ){
                return false;
            }
        }
        
        return flag;
    }
    
     /*
    *This method maps the csv into sobject according to the mapping provided
    ***/
    private List<Sobject> mapSobject(List<List<String>> parsedCSV,Map<String,String> columnToFieldMap, Schema.sObjectType objectDef){
        list<sObject> objects = new list<sObject>();
        integer rowNumber = 0;
        List<string> headers =getHeaders(parsedCSV);
        Map<String,String> fieldToDataTypeMap = describeDestinationObj(objectToInsert);
        for(list<string> row : parsedCSV){
           
            if(rowNumber == 0){
                rowNumber++;
                continue;
            }
            else{
                sObject thisObj = objectDef.newSobject();
                integer colIndex = 0;
                for(string col : row){                   
                    string headerName = headers[colIndex].trim();
                    
                    //pasre the data according to the field data type
                    if(headerName.length() > 0){
                        String fieldName = columnToFieldMap.get(headerName);
                            
                        if(fieldToDataTypeMap.get(fieldName )=='integer'){
                            thisObj.put(fieldName ,Integer.valueOf(col.trim()));
                        }
                        else if(fieldToDataTypeMap.get(fieldName )=='double'){
                             thisObj.put(fieldName ,Double.valueOf(col.trim()));
                        }
                        else if(fieldToDataTypeMap.get(fieldName )=='boolean'){
                             thisObj.put(fieldName ,Boolean.valueOf(col.trim()));
                        }
                        else if(fieldToDataTypeMap.get(fieldName )=='date'){
                             thisObj.put(fieldName ,Date.parse(col.trim()));
                        }
                        else if(fieldToDataTypeMap.get(fieldName )=='datetime'){
                             thisObj.put(fieldName ,DateTime.parse(col.trim()));
                        }
                        else{
                            thisObj.put(fieldName ,col.trim());
                        }
                        colIndex++;
                    }
                } 
                objects.add(thisObj);
                rowNumber++;
            }       
        }
        return objects;
    }
    /* 
    *Credit to 
    *http://wiki.developerforce.com/index.php/Code_Samples#Parse_a_CSV_with_APEX
    **/
    public static List<List<String>> parseCSV(String contents,Boolean skipHeaders) {
        List<List<String>> allFields = new List<List<String>>();
       
        // replace instances where a double quote begins a field containing a comma
        // in this case you get a double quote followed by a doubled double quote
        // do this for beginning and end of a field
        contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
        // now replace all remaining double quotes - we do this so that we can reconstruct
        // fields with commas inside assuming they begin and end with a double quote
        contents = contents.replaceAll('""','DBLQT');
        // we are not attempting to handle fields with a newline inside of them 
        // so, split on newline to get the spreadsheet rows
        List<String> lines = new List<String>();
        lines = contents.split('\r');
        Integer num = 0;
        for(String line : lines) {
            // check for blank CSV lines (only commas)
            if (line.replaceAll(',','').trim().length() == 0) break;
            
            List<String> fields = line.split(',');  
            List<String> cleanFields = new List<String>();
            String compositeField;
            Boolean makeCompositeField = false;
            for(String field : fields) {
                if (field.startsWith('"') && field.endsWith('"')) {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                } else if (field.startsWith('"')) {
                    makeCompositeField = true;
                    compositeField = field;
                } else if (field.endsWith('"')) {
                    compositeField += ',' + field;
                    cleanFields.add(compositeField.replaceAll('DBLQT','"'));
                    makeCompositeField = false;
                } else if (makeCompositeField) {
                    compositeField +=  ',' + field;
                } else {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                }
            }
            
            allFields.add(cleanFields);
        }
        if (skipHeaders) allFields.remove(0);
        return allFields;       
    }
}