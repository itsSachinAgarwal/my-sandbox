public class StudentController 
{
    public ID selectedStudentId{get;set;}
    public map<id, student__c> listOfStudents{get;set;}
    public list<SelectOption> options{get;set;}
   // public Student__c selectedStud{get;set;}
    public StudentController()
    {
     	listOfStudents=new map<id, Student__c>([select id, last_name__c, first_name__c, age__c, sex__c, class__r.name from student__c]);
        system.debug(listOfStudents);
    }
    public List<SelectOption> getItems() {
        options = new List<SelectOption>();
        integer flag=0;
        for(Student__c singleStudent:listOfStudents.values())
        {
            options.add(new SelectOption(singleStudent.id, singleStudent.last_name__c));     
            if(flag==0)
            {
                selectedStudentId=singleStudent.id;
                flag=1;
            }
        }       
        return options; 
    }
	public Student__c getSelectedStud()
    {
        if(selectedStudentId!=null)
        {
            return (Student__c)listOfStudents.get(selectedStudentId);
        }
        else
        {
            Student__c temp=new Student__c();
            return temp;
        }
    }
}