@istest
public class testReceiveEmail 
{
	public static testmethod void testingEmail()
    {
            Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        
            Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();

          // setup the data for the email
          email.subject = 'Test Job Applicant';
          email.fromname = 'FirstName LastName';
          env.fromAddress = 'someaddress@email.com';
        
          // add an attachment
          Messaging.InboundEmail.TextAttachment attachment = new Messaging.InboundEmail.TextAttachment();
          attachment.body = string.valueOf('<contacts><contact><description>hell</description><email>def@gmail.com</email><experience>1</experience><mobilephone>9351473435</mobilephone><title>Mrs.</title></contact><contact><description>hi everyone</description><email>abc@gmail.com</email><experience>5</experience><mobilephone>9928689813</mobilephone><title>Mr.</title></contact></contacts>');
          attachment.fileName = 'textfile.xml';
          attachment.mimeTypeSubType = 'text/xml';
        
          email.textAttachments =
            new Messaging.inboundEmail.TextAttachment[] { attachment };
        
          // call the email service class and test it with the data in the testMethod
          ReceiveEmail emailProcess = new ReceiveEmail();
          Messaging.InboundEmailResult pp=emailProcess.handleInboundEmail(email, env);
    }
}