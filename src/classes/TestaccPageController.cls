@istest
public class TestaccPageController
{
	public static testmethod void testSearch()
    {
        accSearchController obj=new accSearchController(new ApexPages.StandardController(new contact()));
        obj.billingCountry='US';
        obj.search();
        obj.clear();
        obj.billingCountry=null;
        obj.search();
        obj.billingState='CA';
        obj.search();
        obj.billingState=null;
        obj.billingCity='CA';
        obj.search();
        obj.billingState='CA';
        obj.billingCountry='US';
        obj.search();
        obj.billingCity=null;
        obj.search();
        obj.billingCity='CA';
        obj.billingState=null;
        obj.search();
        obj.billingCountry=null;
        obj.billingState='CA';
        obj.search();
    }
}