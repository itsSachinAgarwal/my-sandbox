public class stack
{
    private list<integer> stackofints;
    public stack()
    {
        stackofints=new list<integer>();
    }
    public void push(integer value)
    {
        stackofints.add(value);
    }
    public integer pop()
    {
        if(stackofints.size()==0)
        {
            system.debug('No more items to be removed');
            return -1;
        }
        else
        {
            return stackofints.remove(stackofints.size()-1);
        }
    }
}