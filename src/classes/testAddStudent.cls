@istest
public class testAddStudent 
{
	public static testmethod void testingStu()
    {
        Class__c singlClass=new Class__c(name='1st');            
        insert singlClass;            
        
		PageReference pr=Page.CustomAddStu;
        pr.getParameters().put('id',singlClass.id);
        Test.setCurrentPageReference(pr);
        AddStudent obj=new AddStudent ();
        obj.Stuname='Check';
        obj.SaveStudent();
    }
}