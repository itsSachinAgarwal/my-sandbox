public class accSearchController 
{
    public list<account> accList{get; set;}
    public string billingCountry{get;set;}
    public string billingState{get;set;}
    public string billingCity{get;set;}
    
    public accSearchController(ApexPages.StandardController stdcontroller) 
    {  
    } 
    public void search()
    {
        if(String.isNotEmpty(billingCountry) && String.isNotEmpty(billingState) && String.isNotEmpty(billingCity))
        {
      		accList=[select id, name from account where billingCountry=:billingCountry and billingState=:billingState and billingCity=:billingCity limit 100];    
        }
        else if(String.isNotEmpty(billingCountry) && String.isNotEmpty(billingState) && String.isEmpty(billingCity))
        {
            accList=[select id, name from account where billingCountry=:billingCountry and billingState=:billingState limit 100];    
        }
        else if(String.isNotEmpty(billingCountry) && String.isEmpty(billingState) && String.isNotEmpty(billingCity))
        {
            accList=[select id, name from account where billingCountry=:billingCountry and billingCity=:billingCity limit 100];        
        }
        else if(String.isEmpty(billingCountry) && String.isNotEmpty(billingState) && String.isNotEmpty(billingCity))
        {
      		accList=[select id, name from account where billingState=:billingState and billingCity=:billingCity limit 100];    
        }
        else if(String.isNotEmpty(billingCountry) && String.isEmpty(billingState) && String.isEmpty(billingCity))
        {
      		accList=[select id, name from account where billingCountry=:billingCountry limit 100];    
        }
        else if(String.isEmpty(billingCountry) && String.isEmpty(billingState) && String.isNotEmpty(billingCity))
        {
      		accList=[select id, name from account where billingCity=:billingCity limit 100];    
        }
        else if(String.isEmpty(billingCountry) && String.isNotEmpty(billingState) && String.isEmpty(billingCity))
        {
      		accList=[select id, name from account where billingState=:billingState limit 100];    
        }
        else
        {
            accList.clear();
        }
    }
    
    public void clear()
    {
        accList.clear();
    }
}