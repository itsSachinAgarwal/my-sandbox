/*
	Controller class holding logic for Mass Add Edit and delete of records. The controller is generic enough
	to support object of any type with required set of parameter passed as input to the controller.
*/
public with sharing class MassAddEditDeleteRecordsController {

	private String parentRecordId;
	private String childObjectAPIName;
	private String parentRelationshipField;
	private Integer rowSize;
	private Integer addMoreSize;
	private String fieldSetName;
	private Map<String, SObjectType> globalDescribe; 
	
	public boolean isEverythingOk {get;set;}
	
	// Variable for holding Field set members 
	public List<Schema.FieldSetMember> fieldSetMemberList {get;set;}
	public List<MassAddEditDeleteRecordsUtil.SObjectRowWrapper> sObjectRowWrapperList {get;set;}
	
	public String parentRecordName {get; private set;}
	
	public sObject childSObjectMockUp {get;set;}
	public String selectedFieldForMassUpdate {get;set;}
	public boolean showEditFieldPopup {get;set;}
	
	// Index for the row to be deleted.
	public String selectedRowIndexToDelete {get;set;}
	
	public MassAddEditDeleteRecordsController() {
		// Getting the values from current page parameters.
		parentRecordId = Apexpages.currentPage().getParameters().get('parentId');
		childObjectAPIName = Apexpages.currentPage().getParameters().get('childObject');
		parentRelationshipField = Apexpages.currentPage().getParameters().get('parentField');
		String rowSizeParam = Apexpages.currentPage().getParameters().get('rowSize');
		String addMoreParam = Apexpages.currentPage().getParameters().get('addMore');
		
		fieldSetName = Apexpages.currentPage().getParameters().get('fieldSet');
		
		isEverythingOk = true;
		showEditFieldPopup = false;
		globalDescribe = Schema.getGlobalDescribe();
		
		try {
			
			// Validating the required parameters.
			MassAddEditDeleteRecordsUtil.validateRequiredParameters(parentRecordId, globalDescribe, childObjectAPIName, parentRelationshipField, fieldSetName);
		
			rowSize = String.isNotBlank(rowSizeParam) ? Integer.valueOf(rowSizeParam) : MassAddEditDeleteRecordsUtil.DEFAULT_ROW_SIZE;
			addMoreSize = String.isNotBlank(addMoreParam) ? Integer.valueOf(addMoreParam) : MassAddEditDeleteRecordsUtil.DEFAULT_ADD_MORE_SIZE;
			
			// Getting the name of the parent record using the parent record Id
			parentRecordName = MassAddEditDeleteRecordsUtil.getParentRecordName(parentRecordId);
			
			init();  
			
		} catch(Exception e) {
			Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, e.getMessage())); 
			isEverythingOk = false;
		}
	}
	
	/*
		Method to load the existing child records using the parent record id 
	*/
	public void init() {
		Schema.SObjectType childsObjectType = globalDescribe.get(childObjectAPIName);
		
		// Creating mock object instance to be used to mass field update
		childSObjectMockUp = childsObjectType.newSObject(null, true);
		
		// Getting the field set member list using the field set name and child object type.
		fieldSetMemberList = MassAddEditDeleteRecordsUtil.getFieldSetFields(childsObjectType, fieldSetName);
		
		// List of object wrapper to show on the UI.
		sObjectRowWrapperList = MassAddEditDeleteRecordsUtil.loadChildRecordsByParentId(fieldSetMemberList, childsObjectType, childObjectAPIName, parentRelationshipField, parentRecordId, rowSize);
	}
	
	/*
		Saving (insert and update) records that are added or edited by user. The reocrds that are 
		marked as dirty are processed for inser and update. 
	*/
	public void saveAll() {
		
		List<SObject> listToInsert = new List<SObject>();
		List<SObject> listToUpdate = new List<SObject>();
		
		SavePoint sp = Database.setSavePoint();
		try {
			for(MassAddEditDeleteRecordsUtil.SObjectRowWrapper rw : sObjectRowWrapperList) {
				
				if(rw.isRowdirty) {
					if(rw.record.id != null) {
						listToUpdate.add(rw.record);
					} else {
						listToInsert.add(rw.record);
					}
				}
			}
			
			if(!listToUpdate.isEmpty()) update listToUpdate;
			if(!listToInsert.isEmpty()) insert listToInsert;
			
			init();
			Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.INFO, system.label.Mass_Add_Edit_Delete_Info_Message_All_records_saved_Successfully));
			
		} catch(Exception e) {
			Database.Rollback(sp);
			Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, e.getMessage()));
		}
	}
	
	/*
		Method to delete row based on the selectedRowIndexToDelete value. Only one row will 
		be deleted at a time. 
	*/
	public void deleteRow() {
		try {
			if(!String.isBlank(selectedRowIndexToDelete)) {
				Integer index = Integer.valueOf(selectedRowIndexToDelete);
				
				MassAddEditDeleteRecordsUtil.deleteSelectedRow(sObjectRowWrapperList, index);
			}
		} catch(Exception ex) {Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, String.format(system.label.Mass_Add_Edit_Delete_Error_Problem_while_deleting_selected_row, new String[]{ex.getMessage()}) )); }
	}
	
	/*
		Method to allow deleting of multiple seleted rows both saved (with record id) and new added rows at a time.
	*/
	public void deleteSelectedRows() {
		
		List<MassAddEditDeleteRecordsUtil.SObjectRowWrapper> undeletedSObjectRowWrapperlist = new List<MassAddEditDeleteRecordsUtil.SObjectRowWrapper>();
		
		List<SObject> listToDelete = new List<SObject>();
		for(MassAddEditDeleteRecordsUtil.SObjectRowWrapper rw : sObjectRowWrapperList) {
			if(rw.isSelected) {
				if(rw.record.id != null) {
					// Adding record that need to be delete from data base.
					listToDelete.add(rw.record);
				}
			} else {
				undeletedSObjectRowWrapperlist.add(rw);
			}
		}
		
		if(!listToDelete.isEmpty()) delete listToDelete;
		
		// assigning the undeleted records to the main list.
		sObjectRowWrapperList = undeletedSObjectRowWrapperlist;
	}
	
	/*
		Method to add empty rows as per the "addMore" parameter passed to controller. 
		Default value as DEFAULT_ADD_MORE_SIZE if addMore parameter is missing.
	*/
	public void addNewRows() {
		Schema.SObjectType childsObjectType = globalDescribe.get(childObjectAPIName);
		
		Integer lastRowIndex = sObjectRowWrapperList.get(sObjectRowWrapperList.size()-1).rowIndex;
		sObjectRowWrapperList.addAll(MassAddEditDeleteRecordsUtil.addBlankRows(childsObjectType, addMoreSize, lastRowIndex+1, parentRelationshipField, parentRecordId));
	}
	
	/*
		Method to show popup on the UI to allow user to enter value of a field and add enter value to all rows.
	*/
	public void showEditFieldPopup() {
		if(String.isNotBlank(selectedFieldForMassUpdate)) showEditFieldPopup = true;
	}
	
	/*
		Method to close the mass edit field popup. 
	*/
	public void closeEditFieldPopup() {
		selectedFieldForMassUpdate = '';
		showEditFieldPopup = false;
	}
	
	/*
		Method to set the entered field value to all rows and close the popup window. 
	*/
	public void updateFieldValueToRows() {
		
		for(MassAddEditDeleteRecordsUtil.SObjectRowWrapper rw : sObjectRowWrapperList) {
			rw.record.put(selectedFieldForMassUpdate, childSObjectMockUp.get(selectedFieldForMassUpdate));
			rw.isRowdirty = true;
		}
		selectedFieldForMassUpdate = '';
		showEditFieldPopup = false;
	}
	
	/*
		Method to redirect to the parent record details page.
	*/
	public Pagereference cancel() {
		return new Pagereference('/'+ parentRecordId);
	}
}