global class GenFieldAPINames 
{    
   webservice static String generateAPInames()
    {
		Schema.DescribeSObjectResult r = Student__c.sObjectType.getDescribe();
		String apiNames='';
		for(string apiName : r.fields.getMap().keySet())
        {
   			apiNames=apiNames+apiName+',';
		}
        if(apiNames.endsWith(','))
        {
            apiNames=apiNames.substring(0,apiNames.lastIndexOf(','));
        }
        return apiNames;
	}
}