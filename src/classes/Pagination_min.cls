public with sharing class Pagination_min 
{
    Public Integer noOfRecords{get; set;}
    Public Integer size{get;set;}
    public list<account> accList{get; set;}
    public string billingCountry{get;set;}
    public string billingState{get;set;}
    public string billingCity{get;set;}
    public string query=null;
    
    public Pagination_min(ApexPages.StandardController controller)
    {
        
    }
    public string getQuery()
    {
          if(String.isNotEmpty(billingCountry) && String.isNotEmpty(billingState) && String.isNotEmpty(billingCity))
        {
      		query='select id, name from account where billingCountry=\''+billingCountry+'\' and billingState=\''+billingState+'\' and billingCity=\''+billingCity+'\'';    
      		
        }
        else if(String.isNotEmpty(billingCountry) && String.isNotEmpty(billingState) && String.isEmpty(billingCity))
        {
            query='select id, name from account where billingCountry=\''+billingCountry+'\' and billingState=\''+billingState+'\'';    
        }
        else if(String.isNotEmpty(billingCountry) && String.isEmpty(billingState) && String.isNotEmpty(billingCity))
        {
            query='select id, name from account where billingCountry=\''+billingCountry+'\' and billingCity=\''+billingCity+'\'';    
        }
        else if(String.isEmpty(billingCountry) && String.isNotEmpty(billingState) && String.isNotEmpty(billingCity))
        {
      		query='select id, name from account where billingState=\''+billingState+'\' and billingCity=\''+billingCity+'\'';    
        }
        else if(String.isNotEmpty(billingCountry) && String.isEmpty(billingState) && String.isEmpty(billingCity))
        {
      		query='select id, name from account where billingCountry=\''+billingCountry+'\'';    
        }
        else if(String.isEmpty(billingCountry) && String.isEmpty(billingState) && String.isNotEmpty(billingCity))
        {
      		query='select id, name from account where billingCity=\''+billingCity+'\'';    
        }
        else if(String.isEmpty(billingCountry) && String.isNotEmpty(billingState) && String.isEmpty(billingCity))
        {
      		query='select id, name from account where billingState=\''+billingState+'\'';    
        }
        else
        {
            query='select id, name from account';
        }
        return query;
    }
    public ApexPages.StandardSetController setCon {
        get{
            if(setCon == null){
                size = 10;
                string queryString = getQuery();
                System.debug('+++++++++++queryString '+queryString);
                System.debug('+++++++++++Database.getQueryLocator(queryString) '+Database.getQueryLocator(queryString));
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(queryString));
                setCon.setPageSize(size);
                noOfRecords = setCon.getResultSize();
            }
            return setCon;
        }set;
    }
     
    Public void getAccounts(){
        List<Account> accListp = new List<Account>();
        for(Account a : (List<Account>)setCon.getRecords())
            accListp.add(a);
         accList=accListp;
    }
     
    public pageReference refresh() {
        setCon = null;
        getAccounts();
        setCon.setPageNumber(1);
        return null;
    }
    
    public Boolean hasNext {
        get {
            return setCon.getHasNext();
        }
        set;
    }
    public Boolean hasPrevious {
        get {
            return setCon.getHasPrevious();
        }
        set;
    }
  
    public Integer pageNumber {
        get {
            return setCon.getPageNumber();
        }
        set;
    }
  
    public void first() {
        
        setCon.first();
        getAccounts();
    }
  
    public void last() {
        setCon.last();
        getAccounts();
    }
  
    public void previous() {
        setCon.previous();
        getAccounts();
    }
    public void clear()
    {
        accList.clear();
        billingCountry=null;
        billingState=null;
        billingCity=null;
        setCon=null;
        getAccounts();
        setCon.setPageNumber(1);
    }
  
    public void next() {
        setCon.next();
        getAccounts();
    }
    public void search()
    {
        setcon=null;
        getAccounts();
    }
}