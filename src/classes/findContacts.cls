public class findContacts
{
    public static void checkContacts(id contactid)
    {
        list<Account> listofallaccounts=[select id, contact_ids__c,name from account];
        for(Account acc:listofallaccounts)
        {
            if(acc.contact_ids__c ==null)
            {
                list<Contact> relatedContacts=[select id from contact where account.id=:acc.id];
                for(Contact rContact:relatedContacts)
                {
                    if(acc.contact_ids__c==null)
                    {
                        acc.contact_ids__c=rContact.id+',';
                    }
                    else
                    {
                        acc.contact_ids__c+=rContact.id+',';
                    }
                }
                acc.contact_ids__c= acc.contact_ids__c == null ? null : acc.contact_ids__c.substring(0,acc.contact_ids__c.length()-1);
            }
            else
            {
                integer flag=0;
                list<id> contactids=acc.contact_ids__c.split(',');
                for(id a:contactids)
                {
                    if(a==contactid)
                    {
                        flag=1;
                    }
                    
                }
                if(flag==0)
                {
                    acc.contact_ids__c+=','+contactid;
                }
            }
        }
        update listofallaccounts;
    }
}