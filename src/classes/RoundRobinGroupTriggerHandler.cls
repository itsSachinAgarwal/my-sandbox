/**
* Name:  RoundRobinGroupTriggerHandler
* Purpose: Class which will be used to handle RoundRobinGroup Trigger                                                        
* History                                                           
* 
* VERSION  AUTHOR            				DATE           		Purpose                    
* 1.0 -    Sachin Agarwal ( Appirio JDC)   11/09/2015      INITIAL DEVELOPMENT     
**/
public with sharing class RoundRobinGroupTriggerHandler {
	//Fires whenever the queue associated with Round Robin Group is changed.
	//It validates whether the name given is a valid Queue name or not
	public static void validateQueueIdsBeforeInsert(list<Round_Robin_Group__c> listOfRRGroups){
		
		map<String, Id> mapOfQueueNameAndIds = new map<String, Id>(); 
		list<id> listOfQueueIds = new list<id>(); 
		//Finding all the queues
		for(Group g : [SELECT id, name 
					   FROM Group 
					   WHERE Type = 'Queue']){
			if(g.name != null){
				mapOfQueueNameAndIds.put(g.name.trim().toLowerCase(), g.id);
			}
		}
		list<Round_Robin_Group__c> listOfAlreadyExistingRRGroups = new list<Round_Robin_Group__c>();
		set<String> setOfQueuesAlreadyInUse = new set<String>(); 
		listOfAlreadyExistingRRGroups = [SELECT Queue_Name__c
										 FROM Round_Robin_Group__c];
		for(Round_Robin_Group__c rrg : listOfAlreadyExistingRRGroups){
			setOfQueuesAlreadyInUse.add(rrg.Queue_Name__c.trim().toLowerCase());
		}
		//Going through all the newly created/updated Round Robin Groups and 
		//validating their associated queue names
		for(Round_Robin_Group__c singleRRGroup : listOfRRGroups){
			if(singleRRGroup.Queue_Name__c != null && !String.isBlank(singleRRGroup.Queue_Name__c.trim()) && 
			   mapOfQueueNameAndIds.containsKey(singleRRGroup.Queue_Name__c.trim().toLowerCase()) && (!setOfQueuesAlreadyInUse.contains(singleRRGroup.Queue_Name__c.trim().toLowerCase()))){
				singleRRGroup.Queue_Name__c = singleRRGroup.Queue_Name__c.trim();
			}
			else{
				if(setOfQueuesAlreadyInUse.contains(singleRRGroup.Queue_Name__c.trim().toLowerCase())){
					singleRRGroup.addError('Queue is already in use');
				}
				else{
					singleRRGroup.addError('No such queue exists');
				}
			}
		}
	}
	//Populate Queue Members once the Round Robin Group record is created 
	public static void populateQueueMembers(list<Round_Robin_Group__c> listOfRRGroups){
		map<String, Group> mapOfGroups = new map<String, Group>();
		list<Group> listOfGroups = new list<Group>();
		set<String> setOfUsedQueueIds = new set<String>();		 
		map<Id, set<id>> mapOfQueueAndUsers = new map<Id, set<id>>(); 
		list<Queue_Member__c> listOfQueueMembers = new list<Queue_Member__c>(); 
		//Creating a set of all the queues associated with the affected Round Robin Groups
		for(Round_Robin_Group__c singleRRGroup : listOfRRGroups){
			if(singleRRGroup.Queue_Name__c != null){
				setOfUsedQueueIds.add(singleRRGroup.Queue_Name__c.toLowerCase());	
			}    		
    	}
    	//getting all the queues defined in the system
    	listOfGroups = [SELECT ID, name, (SELECT UserOrGroupId 
	    							 	  FROM GroupMembers)
	 				    FROM Group
	 				    WHERE name IN :setOfUsedQueueIds];
	 	for(Group g : listOfGroups){
	 		mapOfGroups.put(g.name.trim().toLowerCase(), g);
	 	}
	   	//Creating Queue Members
    	for(Round_Robin_Group__c singleRRGroup : listOfRRGroups){
    		if(mapOfGroups.containsKey(singleRRGroup.Queue_Name__c.toLowerCase())){
    			list<GroupMember> listOfAssociatedGroupMembers = mapOfGroups.get(singleRRGroup.Queue_Name__c.toLowerCase()).GroupMembers;
    			if(listOfAssociatedGroupMembers != null){
    				for(GroupMember gm : listOfAssociatedGroupMembers){
    					if(gm.UserOrGroupId != null && String.valueOf(gm.UserOrGroupId).startsWith('005')){
    						listOfQueueMembers.add(new Queue_Member__c(Ready_for_Round_Robin_Assignment__c = true, Round_Robin_Group__c = singleRRGroup.id, User__c = gm.UserOrGroupId));
    					}
    				}
    			}
    		}
    	}
    	insert listOfQueueMembers;
	}
	//Method that will identify all the Round Robin Groups for which the queue has been changed and populate the related list of Queue Members
	public static void removeAndPopulateNewQueueMembersOnQueueChange(map<id, Round_Robin_Group__c> mapOfOldRRGroups, map<id, Round_Robin_Group__c> mapOfNewRRGroups){
		list<Round_Robin_Group__c> listOfAffectedRRs = new list<Round_Robin_Group__c>(); 
		//Finding all the Round Robin Groups which have been affected
		for(Round_Robin_Group__c singleRRGroup : mapOfNewRRGroups.Values()){
			if(singleRRGroup.Queue_Name__c != mapOfOldRRGroups.get(singleRRGroup.id).Queue_Name__c){
				listOfAffectedRRs.add(singleRRGroup);
			}
		}
		//Finding all the queue members which are already associated with the affected
		//Round Robin Groups
		list<Queue_Member__c> listOfAllQueueMembers = new list<Queue_Member__c>(); 
		listOfAllQueueMembers = [SELECT id
								 FROM Queue_Member__c
								 WHERE Round_Robin_Group__c IN :listOfAffectedRRs];
		//Deleting the old queue members
		delete listOfAllQueueMembers;
		//Populate the new members as per the new queue
		populateQueueMembers(listOfAffectedRRs);
	}
	public static void validateQueueIdsBeforeUpdate(list<Round_Robin_Group__c> listOfRRGroups, map<id, Round_Robin_Group__c> mapOfOldRRGroups){
		list<Round_Robin_Group__c> listOfAffectedRRs = new list<Round_Robin_Group__c>(); 
		//Finding all the Round Robin Groups which have been affected
		for(Round_Robin_Group__c singleRRGroup : listOfRRGroups){
			if(singleRRGroup.Queue_Name__c != mapOfOldRRGroups.get(singleRRGroup.id).Queue_Name__c){
				listOfAffectedRRs.add(singleRRGroup);
			}
		}
		validateQueueIdsBeforeInsert(listOfAffectedRRs);
	}
}