@RestResource (urlMapping='/v1/accounts/*')
global with sharing class RestServiceCheck{
    @HttpGet
    global static Account doGet(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.Response;
        String accountId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        Account result = [select id, name from account where id=:accountId];
        return result;
    }
}