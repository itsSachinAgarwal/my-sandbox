/**
 * Test class covering code coverage for following classes : 
 	1. CustomDataloaderTemplateController
 	2. CustomDataloaderController
 */
@isTest
private class CustomDataloaderTest {
	
	static Opportunity opportunity1, opportunity2, opportunity3;
	static Lead lead1, lead2, lead3;
	
	// Method for creating test data
	static void createTestData() {
        Account acc = new Account(Name='TestOppAcc');
        insert acc;
        
        opportunity1 = new Opportunity(Name = 'Test Opportunity091' , CloseDate = system.today().addDays(8), StageName = 'Qualification', AccountId = acc.id);
        opportunity2 = new Opportunity(Name = 'Test Opportunity092' , CloseDate = system.today().addDays(9), StageName = 'Qualification', AccountId = acc.id);
        opportunity3 = new Opportunity(Name = 'Test Opportunity093' , CloseDate = system.today().addDays(10), StageName = 'Qualification', AccountId = acc.id);
        insert new List<Opportunity>{opportunity1, opportunity2, opportunity3};
        
        lead1 = new Lead(FirstName = 'Test Lead', LastName = '1001', Company = 'Test Company01', Description = 'Test Lead desc01');
        lead2 = new Lead(FirstName = 'Test Lead', LastName = '1002', Company = 'Test Company02', Description = 'Test Lead desc02');
        lead3 = new Lead(FirstName = 'Test Lead', LastName = '1003', Company = 'Test Company03', Description = 'Test Lead desc03');
        insert new List<Lead>{lead1, lead2, lead3};
	}
	
	
	/*
		Insert template generation use case.
	*/
    static testMethod void customDataloaderTemplateControllerTest1() {
        
        Test.setCurrentPage(Page.CustomDataloaderTemplate);
        
        Apexpages.currentPage().getParameters().put('objtype', 'Account');
        Apexpages.currentPage().getParameters().put('temtype', DataloaderUtil.TEMPLATE_TYPE_INSERT);
        
        Test.startTest();
        CustomDataloaderTemplateController controller = new CustomDataloaderTemplateController();
        
        system.assert(controller.showTemplateSection, 'No error while generating the insert template');
        
        system.assert(!controller.selectedObjFieldList.isEmpty() , 'Selected object field list is not empty');
        
        system.assert(!controller.selectedObjFieldList.get(0).equalsIgnoreCase('Id') , 'The first field in the list should not be Id field for insert template');
        Test.stopTest();
    }
    
    /*
    	update template generation use case.
    */
    static testMethod void customDataloaderTemplateControllerTest2() {
        
        Test.setCurrentPage(Page.CustomDataloaderTemplate);
        
        Apexpages.currentPage().getParameters().put('objtype', 'Account');
        Apexpages.currentPage().getParameters().put('temtype', DataloaderUtil.TEMPLATE_TYPE_UPDATE);
        
        Test.startTest();
        CustomDataloaderTemplateController controller = new CustomDataloaderTemplateController();
        
        system.assert(controller.showTemplateSection, 'No error while generating the update template');
        
        system.assert(!controller.selectedObjFieldList.isEmpty() , 'Selected object field list is not empty');
        
        system.assert(controller.selectedObjFieldList.get(0).equalsIgnoreCase('Id') , 'The first field in the list should be Id field for update template');
        Test.stopTest();
    }
    
    /*
    	Update template generation use case with missing objtype parameter.
    */
    static testMethod void customDataloaderTemplateControllerTest3() {
        
        Test.setCurrentPage(Page.CustomDataloaderTemplate);
        
        //Apexpages.currentPage().getParameters().put('objtype', 'Account');
        Apexpages.currentPage().getParameters().put('temtype', DataloaderUtil.TEMPLATE_TYPE_UPDATE);
        
        Test.startTest();
        CustomDataloaderTemplateController controller = new CustomDataloaderTemplateController();
        
        system.assert(!controller.showTemplateSection, 'Error while generating the insert template with missing objtype parameter');
        
        system.assert(controller.failureMessage.contains('Required object type is missing. Failed to genetate dataloader template'));
        Test.stopTest();
    }
    
    /*
    	Update template generation use case with invalid objtype parameter.
    */
    static testMethod void customDataloaderTemplateControllerTest4() {
        
        Test.setCurrentPage(Page.CustomDataloaderTemplate);
        
        //Passing invalid object name
        Apexpages.currentPage().getParameters().put('objtype', 'Test_Object');
        Apexpages.currentPage().getParameters().put('temtype', DataloaderUtil.TEMPLATE_TYPE_UPDATE);
        
        Test.startTest();
        CustomDataloaderTemplateController controller = new CustomDataloaderTemplateController();
        
        system.assert(!controller.showTemplateSection, 'Error while generating the insert template with missing objtype parameter');
        
        system.assert(controller.failureMessage.contains('Exception while generating data loader template for object'));
        Test.stopTest();
    }
    
    /*
    	Performing insert upload successfully. 
    */
    static testMethod void customDataloaderControllerInsertTest1() {
        
        Test.setCurrentPage(Page.CustomDataloader);
        
        Test.startTest();
        CustomDataloaderController controller = new CustomDataloaderController();
        
        system.assert(!controller.sObjectSelectOptionList.isEmpty() , 'object list is not empty');
        
        controller.selectedObject = 'Account';
        controller.generateUploadTemplate();
        
        controller.nameFile = 'TestAccountTemplate.csv';
        String csvFile = 'Name,AccountNumber,Description\n';
		csvFile += 'TestAccount1,12001,"test account #1"\n';
		csvFile += 'TestAccount2,"12002","test account #2"\n';
		csvFile += 'TestAccount3, ,"test account #3"\n';

		Blob csvBlob = Blob.valueOf(csvFile);
        controller.contentFile = csvBlob;
        
        List<Account> accounts = [Select Name, AccountNumber, Description from Account limit 10];
        system.assert(accounts.isEmpty(), 'There is no account records exist');
        
        controller.insertFileRecords();
        
        accounts = [Select Name, AccountNumber, Description from Account order by name limit 10 ];
        system.assert(!accounts.isEmpty(), 'Account records list is not empty.');
        system.assertEquals(3, accounts.size(), 'Account records list is not empty.');
        system.assertEquals('TestAccount1', accounts.get(0).Name);
        
        system.assert(Apexpages.hasMessages());
        Apexpages.Message message1 = Apexpages.getMessages().get(0);
        
        system.assert(message1.getDetail().contains('3 records inserted successfully'), message1.getDetail());
        system.assertEquals(ApexPages.Severity.INFO, message1.getSeverity());
    	
    	system.assert(controller.showSuccessAndFailureReport, 'There are some Success log to shown on UI');
    	
        Test.stopTest();
    }
    
    /*
    	Performing insert upload without header columns or file content . 
    */
    static testMethod void customDataloaderControllerInsertTest2() {
        
        Test.setCurrentPage(Page.CustomDataloader);
        
        Test.startTest();
        CustomDataloaderController controller = new CustomDataloaderController();
        
        system.assert(!controller.sObjectSelectOptionList.isEmpty() , 'object list is not empty');
        
        system.assert(controller.disableUploadButtons, 'Upload buttons will be disable');
        
        controller.selectedObject = 'Account';
        controller.generateUploadTemplate();
        
        system.assert(!controller.disableUploadButtons, 'Upload buttons will not be disable once object is selected');
        
        controller.nameFile = 'TestAccountTemplate.csv';
        String csvFile = '';

		Blob csvBlob = Blob.valueOf(csvFile);
        controller.contentFile = csvBlob;
        
        controller.insertFileRecords();

        system.assert(Apexpages.hasMessages());
        Apexpages.Message message1 = Apexpages.getMessages().get(0);
        
        system.assert(message1.getDetail().contains('Exception while parsing uploaded file'), message1.getDetail());
        system.assertEquals(ApexPages.Severity.ERROR, message1.getSeverity());
        
        system.assert(!controller.showSuccessAndFailureReport, 'There no log to shown on UI');
    
        Test.stopTest();
    }
    
    //---------------- Update test case --------------
    
    /*
    	Performing update upload successfully. 
    */
    static testMethod void customDataloaderControllerUpdateTest1() {
        createTestData();
        
        Test.setCurrentPage(Page.CustomDataloader);
        
        Test.startTest();
        CustomDataloaderController controller = new CustomDataloaderController();
        
        system.assert(!controller.sObjectSelectOptionList.isEmpty() , 'object list is not empty');
        
        controller.selectedObject = 'Opportunity';
        controller.generateUploadTemplate();
        
        controller.nameFile = 'TestOpportunityTemplate.csv';
        String csvFile = 'Id,Name,StageName\n';
		csvFile += opportunity1.Id+',Test Opportunity091,Closed Won\n'; // updating opp stage 
		csvFile += opportunity2.Id+',Test Opportunity2092,Qualification\n'; //updating opp name
		csvFile += opportunity3.Id+',Test Opportunity093,"Qualification"\n'; // no change 

		Blob csvBlob = Blob.valueOf(csvFile);
        controller.contentFile = csvBlob;
        
        controller.updateFileRecords();
        
        List<Opportunity> Opportunities = [Select Name, StageName from Opportunity order by name limit 10 ];
        
        system.assert(!Opportunities.isEmpty(), 'Opportunity records list is not empty.');
        system.assertEquals(3, Opportunities.size(), 'Opportunity records list is not empty.');
        
        system.assertEquals('Closed Won', Opportunities.get(0).StageName);
        
        system.assert(Apexpages.hasMessages());
        Apexpages.Message message1 = Apexpages.getMessages().get(0);
        
        system.assert(message1.getDetail().contains('3 records updated successfully'), message1.getDetail());
        system.assertEquals(ApexPages.Severity.INFO, message1.getSeverity());
    	
    	system.assert(controller.showSuccessAndFailureReport, 'There are some Success log to shown on UI');
    	
        Test.stopTest();
    }
    
    /*
    	Performing update upload without header columns or file content . 
    */
    static testMethod void customDataloaderControllerUpdateTest2() {
        createTestData();
        
        Test.setCurrentPage(Page.CustomDataloader);
        
        Test.startTest();
        CustomDataloaderController controller = new CustomDataloaderController();
        
        system.assert(!controller.sObjectSelectOptionList.isEmpty() , 'object list is not empty');
        
        system.assert(controller.disableUploadButtons, 'Upload buttons will be disable');
        
        controller.selectedObject = 'Opportunity';
        controller.generateUploadTemplate();
        
        system.assert(!controller.disableUploadButtons, 'Upload buttons will not be disable once object is selected');
        
        controller.nameFile = 'TestOpportunityTemplate.csv';
        String csvFile = '';

		Blob csvBlob = Blob.valueOf(csvFile);
        controller.contentFile = csvBlob;
        
        controller.updateFileRecords();

        system.assert(Apexpages.hasMessages());
        Apexpages.Message message1 = Apexpages.getMessages().get(0);
        
        system.assert(message1.getDetail().contains('Exception while parsing uploaded file.'), message1.getDetail());
        system.assertEquals(ApexPages.Severity.ERROR, message1.getSeverity());
        
        system.assert(!controller.showSuccessAndFailureReport, 'There no log to shown on UI');
    
        Test.stopTest();
    }
    
    // ------------ Upsert case ------------------
    
    /*
    	Performing upsert upload successfully. 
    */
    static testMethod void customDataloaderControllerUpsertTest1() {
        createTestData();
        
        Test.setCurrentPage(Page.CustomDataloader);
        
        Test.startTest();
        CustomDataloaderController controller = new CustomDataloaderController();
        
        system.assert(!controller.sObjectSelectOptionList.isEmpty() , 'object list is not empty');
        
        controller.selectedObject = 'Lead';
        controller.generateUploadTemplate();
        
        controller.nameFile = 'TestLeadTemplate.csv';
        String csvFile = 'Id,FirstName,LastName,Company,Description\n';
		csvFile += lead1.Id+',,,, Test desc update 01\n'; // updating opp stage 
		csvFile += lead2.Id+',,,, Test desc update 02\n'; //updating opp name
		csvFile += lead3.Id+',,,, Test desc update 03\n'; // no change 
		csvFile += ',New Lead,2001,Test Company4, \n'; // new opp to insert
		csvFile += ',New Lead,2002,Test Company5, Test desc update 05\n'; // new opp to insert

		Blob csvBlob = Blob.valueOf(csvFile);
        controller.contentFile = csvBlob;
        
        List<Lead> leads = [Select FirstName, LastName, Company  from Lead order by name limit 10 ];
        system.assert(!leads.isEmpty(), 'Lead records list is not empty.');
        system.assertEquals(3, leads.size(), 'Lead records list is not empty.');
        
        controller.upsertFileRecords();
        
        leads = [Select FirstName, LastName, Company from Lead order by name limit 10 ];
        system.assert(!leads.isEmpty(), 'leads records list is not empty.');
        system.assertEquals(5, leads.size());
        
        system.assert(Apexpages.hasMessages());
        Apexpages.Message message1 = Apexpages.getMessages().get(0);
        system.assert(message1.getDetail().contains('3 records update successfully'), message1.getDetail());
        system.assertEquals(ApexPages.Severity.INFO, message1.getSeverity());
        
        message1 = Apexpages.getMessages().get(1);
        system.assert(message1.getDetail().contains('2 records insert successfully'), message1.getDetail());
        system.assertEquals(ApexPages.Severity.INFO, message1.getSeverity());
    	
    	system.assert(controller.showSuccessAndFailureReport, 'There are some Success log to shown on UI');
    	
        Test.stopTest();
    }
    
    /*
    	Performing upsert upload without header columns or file content . 
    */
    static testMethod void customDataloaderControllerUpsertTest2() {
        createTestData();
        
        Test.setCurrentPage(Page.CustomDataloader);
        
        Test.startTest();
        CustomDataloaderController controller = new CustomDataloaderController();
        
        system.assert(!controller.sObjectSelectOptionList.isEmpty() , 'object list is not empty');
        
        system.assert(controller.disableUploadButtons, 'Upload buttons will be disable');
        
        controller.selectedObject = 'Lead';
        controller.generateUploadTemplate();
        
        system.assert(!controller.disableUploadButtons, 'Upload buttons will not be disable once object is selected');
        
        controller.nameFile = 'TestLeadTemplate.csv';
        String csvFile = '';

		Blob csvBlob = Blob.valueOf(csvFile);
        controller.contentFile = csvBlob;
        
        controller.upsertFileRecords();

        system.assert(Apexpages.hasMessages());
        Apexpages.Message message1 = Apexpages.getMessages().get(0);
        
        system.assert(message1.getDetail().contains('Exception while parsing uploaded file.'), message1.getDetail());
        system.assertEquals(ApexPages.Severity.ERROR, message1.getSeverity());
        
        system.assert(!controller.showSuccessAndFailureReport, 'There no log to shown on UI');
    
        Test.stopTest();
    }
    
    
}