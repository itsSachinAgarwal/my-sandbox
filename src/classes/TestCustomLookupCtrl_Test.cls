/**
* Name:  TestCustomLookupCtrl_Test
* Purpose: This is a test class for testing TestCustomLookupCtrl                                                         
* History                                                           
* 
* VERSION  AUTHOR                           DATE                Purpose                    
* 1.0 -    Sachin Agarwal ( Appirio JDC)   28/04/2015      INITIAL DEVELOPMENT
**/

@isTest
private class TestCustomLookupCtrl_Test {
	//----------------------------------------------------------------------------------------------------------
    // Method containing the main logic to test the class
    //----------------------------------------------------------------------------------------------------------
    static testMethod void unitTest() {
        PageReference pgRef = Page.TestCustomLookup;
	    Test.setCurrentPageReference (pgRef);
	    TestCustomLookupCtrl obj = new TestCustomLookupCtrl(); 
	    obj.companyDiscussed = 'test';
    }
}