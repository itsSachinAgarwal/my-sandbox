public with sharing class DataloaderUtil {
	
	public static String DEFAULT_TEMPLATE_TYPE = 'upsert';
	public static String TEMPLATE_TYPE_INSERT = 'insert';
	public static String TEMPLATE_TYPE_UPDATE = 'update';
	public static String TEMPLATE_TYPE_UPSERT = 'upsert';
	
	/*
		Added method to describe to get list of all standard and custom objects to whom user have access. 
	*/
	public static List<SelectOption> getSObjectList(Map<String, Schema.SObjectType> globalDescribe) {
		List<SelectOption> sobjectsSelectOptList = new List<SelectOption>();
		
		//set ot store an ordered list of sobjects (by name)
		Set<String> objsOrderName = new Set<String>();
		
		Map<String,String> sobjNamesLabels = new Map<String,String>();
		
		for(Schema.Sobjecttype sot : globalDescribe.values())
		{
			Schema.Describesobjectresult dr = sot.getDescribe();
			
			// Checking for user access (object CRUD) on the described object list.
			if(dr.isAccessible() && dr.isCustomSetting()==false  && dr.isDeprecatedAndHidden()==false
				&& dr.isQueryable() && dr.isUpdateable() && dr.isCreateable())
			{
				sobjNamesLabels.put(dr.getName(), dr.getlabel());
				objsOrderName.add(dr.getName());
			}
		}
		
		//creates a list to make it order its elements
		List<String> nameList = new List<String>(objsOrderName);
		nameList.sort();
		
		sobjectsSelectOptList.add(new selectoption('', '--None--'));
		for(String name : nameList)
		{
			sobjectsSelectOptList.add(new SelectOption(name,sobjNamesLabels.get(name)));
		}
		return sobjectsSelectOptList;
	}
	
	/*
		Method making field describe and preparing select option. 
	*/
	public static List<String> getObjectFields(String objectName, Map<String, Schema.SObjectType> globalDescribe, String templateType) {
		List<String> fieldList = new List<String>();
		
		// objectName is null or blan, return from the method.
		if(String.isBlank(objectName)) return fieldList;
		
		// Describing object get it field api names.
		Map<String, Schema.sObjectField> fieldMap = globalDescribe.get(objectName).getDescribe().fields.getMap();
		
		
		for(Schema.SObjectField sObjectField : fieldMap.values()){
        
            Schema.DescribeFieldResult dfr = sObjectField.getDescribe();
            
            // If its a Id fiel eskipe it as it will always be added as first column in generated template. 
            if(dfr.getName().equalsIgnoreCase('Id')) continue;
             
            // Checking FLS on described fields. 
            if(dfr.isAccessible() && dfr.isCreateable() && dfr.isUpdateable() ) {
            	fieldList.add(dfr.getName());
            }
		}
		fieldList.sort();
		
		// Adding the id at the top to render as first column in excel file. Id is only added if tempalte type is not insert. 
		if(!String.isBlank(templateType) && !templateType.equalsIgnoreCase(TEMPLATE_TYPE_INSERT)) {
			fieldList.add(0, 'Id');
		}
		
		return fieldList;
	}
	
	/*
		Method for preapring list of record to be inserted. The list is prepared as generic sObject type so 
		any object type will be supported. 
	*/
	public static List<sObject> prepareRecordToInsertObjList(Schema.SObjectType objectType, List<List<String>> recordFieldToInsert, List<String> headerFields) {
		
		Map<String, Schema.DescribeFieldResult> describeFieldResultMap = getDescribeFieldResultMap(objectType, headerFields);
		
	    List<sObject> objToInsertList = new List<sObject>();   	
       	for(List<String> fieldValues : recordFieldToInsert) {
       		sObject objToInsert = objectType.newSObject();
        	for(Integer i=0; i< headerFields.size();i++) {
        		
        		// Checking create access on the field for the user. 
        		if( describeFieldResultMap.get(headerFields.get(i)) != null &&  describeFieldResultMap.get(headerFields.get(i)).isCreateable()) {
	        		// Checking for any null or blank value and skiping Id field as it is of no use while preparing insert object list.
	        		if(!headerFields.get(i).equalsIgnoreCase('Id') && !String.isBlank(fieldValues.get(i))) objToInsert.put(headerFields.get(i), fieldValues.get(i));
        		}
        	}
        	objToInsertList.add(objToInsert);
       	}
       	return objToInsertList;
	}
	
	/*
		Method for create DescribeFieldResult Map by field name. The map is used for checking the create and update permission on 
		fields while insert and update/upsert. 
	*/
	public static Map<String, Schema.DescribeFieldResult> getDescribeFieldResultMap(Schema.SObjectType objectType, List<String> headerFields) {
		// Describing object get it field api names.
		Map<String, Schema.sObjectField> fieldMap = objectType.getDescribe().fields.getMap();
		
		Map<String, Schema.DescribeFieldResult> describeFieldResultMap = new Map<String, Schema.DescribeFieldResult>();
		for(String field : headerFields) {
        	if(fieldMap.get(field) != null) describeFieldResultMap.put(field, fieldMap.get(field).getDescribe());
		}
		return describeFieldResultMap;
	}
	
	/*
    	Method for parsing passed content string and preparing list of list for each parsed row. 
    */
    public static List<List<String>> parseCSV(String contents, Boolean skipHeaders) {
	    List<List<String>> allFields = new List<List<String>>();
	
	    // replace instances where a double quote begins a field containing a comma
	    // in this case you get a double quote followed by a doubled double quote
	    // do this for beginning and end of a field
	    contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
	    // now replace all remaining double quotes - we do this so that we can reconstruct
	    // fields with commas inside assuming they begin and end with a double quote
	    contents = contents.replaceAll('""','DBLQT');
	    // we are not attempting to handle fields with a newline inside of them
	    // so, split on newline to get the spreadsheet rows
	    List<String> lines = new List<String>();
	    try {
	        lines = contents.split('\n');
	    } catch (System.ListException e) {
	        throw new CustomDataloaderController.CustomDataloaderException('Limits exceeded. Error ' + e.getMessage());
	    }
	    
	    system.debug('Line num i s'+lines.size());
	    Integer num = 0;
	    for(String line : lines) {
	    	
	        // check for blank CSV lines (only commas)
	        if (line.replaceAll(',','').trim().length() == 0) break;
	        
	        List<String> fields = line.split(',');  
	        List<String> cleanFields = new List<String>();
	        String compositeField;
	        Boolean makeCompositeField = false;
	        for(String field : fields) {
	            if (field.startsWith('"') && field.endsWith('"')) {
	                cleanFields.add(field.replaceAll('DBLQT','"'));
	            } else if (field.startsWith('"')) {
	                makeCompositeField = true;
	                compositeField = field;
	            } else if (field.endsWith('"')) {
	                compositeField += ',' + field;
	                cleanFields.add(compositeField.replaceAll('DBLQT','"'));
	                makeCompositeField = false;
	            } else if (makeCompositeField) {
	                compositeField +=  ',' + field;
	            } else {
	                cleanFields.add(field.replaceAll('DBLQT','"'));
	            }
	        }
	        
	        allFields.add(cleanFields);
	    }
	    
	    // Skip the header as that will contain the field detail. 
	    if (skipHeaders) allFields.remove(0);
	    return allFields;       
	} 
	
	/*
		Method for preparing a success failure report for insert action to show on UI.
	*/
	public static List<SuccessAndFailureReport> prepareSuccessAndFailureReport(Database.SaveResult[] saveResult) {
		
		List<SuccessAndFailureReport> reportList = new List<SuccessAndFailureReport>();
		
		for(Database.SaveResult sr : saveResult) {
            if(sr.isSuccess()) {
            	reportList.add(new SuccessAndFailureReport(sr.getId(), '', '', 'Success'));
            } else {
                Database.Error err = sr.getErrors()[0];
                reportList.add(new SuccessAndFailureReport('', ''+err.getStatusCode(), err.getMessage(), 'Fail'));
            }
        }
        return reportList;
	}
	
	/*
		Method for preparing a success failure report for upsert action to show on UI.
	*/
	public static List<SuccessAndFailureReport> prepareSuccessAndFailureReport(Database.UpsertResult[] upsertResult) {
		
		List<SuccessAndFailureReport> reportList = new List<SuccessAndFailureReport>();
		
		for(Database.UpsertResult sr : upsertResult) {
            if(sr.isSuccess()) {
            	reportList.add(new SuccessAndFailureReport(sr.getId(), '', '', 'Success'));
            } else {
                Database.Error err = sr.getErrors()[0];
                reportList.add(new SuccessAndFailureReport('', ''+err.getStatusCode(), err.getMessage(), 'Fail'));
            } 
            // If reportList is more then 999 break as records mmore then 99 can not be rendered on UI. 
            if(reportList.size() == 999) break;
        }
        return reportList;
	}
	
	/*
		Wrapper class holding success and failure details.
	*/
	public class SuccessAndFailureReport {
		public String recordId {get; set;}
		public String statusCode {get; set;}
		public String errMessage {get; set;}
		public String status {get; set;}
		
		public SuccessAndFailureReport(String recordId, String statusCode, String errMessage, String status) {
			this.recordId = recordId;
			this.statusCode = statusCode;
			this.errMessage = errMessage;
			this.status = status;
		}
		
	}
}