public with sharing class Testing {
	public void chk(){
		pageReference pdf = Page.Example;
		list<attachment> insertAttachment = new list<attachment>();
		list<Account> accountIdList = new list<Account>();
		accountIdList = [select id from account];
        for(Account acc: accountIdList){
            //create a pageReference instance of the VF page.
            String accId = acc.id;
            //pass the Account Id parameter to the class.
            pdf.getParameters().put('id',accId);
            Attachment attach = new Attachment();
            Blob body;
            if(!test.isRunningTest()){
                body = pdf.getContent();
            }else{
                body=blob.valueOf('TestString');
            }
            attach.Body = body;
            attach.Name = 'pdfName'+accId+'.pdf';
            attach.IsPrivate = false;
            attach.ParentId = accId;//This is the record to which the pdf will be attached
            insertAttachment.add(attach);
         }
         //insert the list
         insert insertAttachment;
	}
	
    }