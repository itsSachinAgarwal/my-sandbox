/**=====================================================================
 * Appirio, Inc
 * Name: DynamicTableController_Test
 * Description: Class for testing DynamicTableController_Test
 * Created Date: May 15, 2015
 * Created By: Sachin Agarwal (Appirio)
 =====================================================================*/
@isTest
private class DynamicTableController_Test {

    static testMethod void unitTest() {
        // TO DO: implement unit test
        DynamicTableController obj = new DynamicTableController();
        Account acc = new Account();
    	acc.Name = 'TestAcc';
    	insert acc;
    	
    	PageReference testPage = Page.DynamicTable;
        Test.setCurrentPage(testPage);
        System.debug('Hi ' + obj.supportedObject);
        for(SelectOption so : obj.supportedObject){
        	System.debug('Value is ' + so.getValue());
        	if(so.getValue() == 'Account'){
				obj.SelectedObject = so.getValue();
				break;
			}   
        }
        obj.ObjectFields();
        obj.ShowTable();
        System.assert(obj.ObjectList.size() == 1);
	}
}