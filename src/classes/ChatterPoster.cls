public with sharing class ChatterPoster {
	public static void getRecipeEightTopPosters() 
     {
        List<AggregateResult> results =  [
            SELECT ParentId pid, Parent.Name pname, COUNT(id) fcount 
            FROM UserFeed 
            WHERE Type='UserStatus' 
            AND CreatedDate = THIS_WEEK
            GROUP BY Parent.Name, ParentId
            ORDER BY Count(id) DESC LIMIT 10];
 
        List<Recommendation> top = new List<Recommendation>();
        Recommendation r = null;
 
        for (AggregateResult ar : results) 
        {
            r = new Recommendation();
            r.count = ar.get('fcount') + '';
            r.name = ar.get('pname')+'';
            r.entityid = ar.get('pid')+'';
            top.add(r);
        }
        System.debug('Names are ' + top);
    }
    public class Recommendation{
    	public String count;
    	public String name;
    	public ID entityid;
    }
}