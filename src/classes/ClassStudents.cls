public class ClassStudents
{

    public static list<student__c> findStudentNames(id classid)
    {
        list<student__c> studentnames=[select first_name__c from student__c where class__c=:classid];
        return studentnames;
    }
    
    public integer countworkingdays(date startdate, date enddate)
    {
        datetime stdate=datetime.newInstance(startdate.year(),startdate.month(),startdate.day());
        datetime edate=datetime.newInstance(enddate.year(),enddate.month(),enddate.day());
        Integer i = 0;

        while (stdate < =edate) 
        {
            if (stdate.format('EEE') != 'Sat' && stdate.format('EEE') != 'Sun') 
            {
                i = i + 1;
            }
            stdate = stdate.addDays(1);
        }

        return i;
    }
    public map<class__c, set<id>> getStudentIDs()
    {
        map<class__c, set<id>> mymap=new  map<class__c, set<id>>();
        list<class__c> classlist=[select id,name from class__c];
        for(class__c singleclass:classlist)
        {
            list<student__c> studentlist=[select id from student__c where class__r.id=:singleclass.id];
            set<id> myset=new set<id>();
            for(student__c singlestudent: studentlist)
            {
                myset.add(singlestudent.id);
            }
            mymap.put(singleclass, myset);
            
        }
        return mymap;
    }
}