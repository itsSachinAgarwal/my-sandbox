/**
* Name:  TaskAssignment_Test
* Purpose: This is a test class for testing TaskAssignment                                                         
* History                                                           
* 
* VERSION  AUTHOR                           DATE                Purpose                    
* 1.0 -    Sachin Agarwal ( Appirio JDC)   27/04/2015      INITIAL DEVELOPMENT
**/

@isTest
private class TaskAssignment_Test {
    //----------------------------------------------------------------------------------------------------------
    // Main method responsible for testing the logic
    //----------------------------------------------------------------------------------------------------------
    static testMethod void unitTest() {
    	Account newAccount = createAccount('Test Account', true);  
    	  	
    	Contact newContact1 = createContact(newAccount.Id, 'Test', 'Ing', 'a@a.com','test', 'India', 'Rajasthan','Test', '111111', true);
    	Contact newContact2 = createContact(newAccount.Id, 'Test', 'Ing', 'a@a.com','test', 'India', 'Rajasthan','Test', '111111', true);
    	
    	Task t1 = createTask('Call', 'In Progress', newContact1.id, true);
    	Task t2 = createTask('Call', 'In Progress', newContact1.id, true);
    	Task t3 = createTask('Email', 'Not Started', null, true);
    	Task t4 = createTask('Call', 'Not Started', null, true);
    	
    	TaskAssignment taskAssignObj = new TaskAssignment();
    	
    	list<Task> listOfTasks = new list<Task>();
    	listOfTasks = taskAssignObj.getTaskList();
    	
    	System.assertEquals(listOfTasks.size(), 4);
    	
    	list<Contact> listOfContacts = new list<Contact>();
    	listOfContacts = taskAssignObj.getContactList();
    	
    	System.assertEquals(listOfContacts.size(), 2);
    	
    	String status = TaskAssignment.assignTaskToContact(t3.id, newContact2.id);
    	
    	Task t = [SELECT id, whoID
    			  FROM Task
    			  WHERE id = :t3.id];
    	System.assertEquals(newContact2.id, t.whoID);
    	
    	status = TaskAssignment.unAssignTaskToContact(t3.id);
    	
    	Task tc = [SELECT id, whoID
    			   FROM Task
    			   WHERE id = :t3.id];
    	System.assertEquals(null, tc.whoID);
    }
  	//----------------------------------------------------------------------------------------------------------
  	// Returns instance of Contact
  	//----------------------------------------------------------------------------------------------------------
  	public static Contact createContact(ID accID, String firstName, String lastName, String email,
  									  String mailingCity, String mailingCountry, String mailingState,
  									  String mailingStreet, String mailingPostalCode, Boolean isInsert){
  		Contact contactObj = new Contact();
  	
		contactObj.accountID = accID;
		contactObj.LastName = lastName;
		contactObj.FirstName = firstName;
		contactObj.Email = email;
		contactObj.experience__c = 2;
		contactObj.MailingCity = mailingCity;
		contactObj.MailingCountry = mailingCountry;
		contactObj.MailingState = mailingState;
		contactObj.MailingStreet = mailingStreet;
		contactObj.MailingPostalCode = mailingPostalCode;
		if (isInsert) {
      		insert contactObj;
    	}
    	return contactObj;
  	}	
    //----------------------------------------------------------------------------------------------------------
    // Method being used to create sample Task
    //----------------------------------------------------------------------------------------------------------
    public static Task createTask(String subject, String status, Id whoID, Boolean isInsert){
    	Task u = new Task();
	    u.Subject = subject;
	    u.Status = status;
	    if(isInsert){
	        insert u;
	    }
	    return u;      
    }
    //----------------------------------------------------------------------------------------------------------
    // Method being used to create sample Account
    //----------------------------------------------------------------------------------------------------------
	public static Account createAccount(String accountName, Boolean isInsert){
	    Account account = new Account();
	    account.Name = accountName;
	    if(isInsert){
	        insert account;
	    }
	    return account;
	}
}