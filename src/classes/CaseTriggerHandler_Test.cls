/**
* Name:  CaseTriggerHandler_Test
* Purpose: This is a test class for testing CaseTriggerHandler                                                         
* History                                                           
* 
* VERSION  AUTHOR                           DATE                Purpose                    
* 1.0 -    Sachin Agarwal ( Appirio JDC)   28/04/2015      INITIAL DEVELOPMENT
**/

@isTest
private class CaseTriggerHandler_Test {
	
	//----------------------------------------------------------------------------------------------------------
    // Method containing the main logic to test the class
    //----------------------------------------------------------------------------------------------------------
    static testMethod void unitTest() {
    	Account acc = createAccount('Test Account', true);

		Contact newContact = createContact(acc.Id, 'Test', 'Ing', 'a@a.com','test', 'India', 'Rajasthan','Test', '111111', true);
		Contact newContact1 = createContact(acc.Id, 'Test1', 'Ing', 'a@ab.com','test', 'India', 'Rajasthan','Test', '111111', true);
		
		Case caseObj = createCase('Sub', newContact.id, true);
		
		list<Case_Ownership_History__c> listOfCaseOwnershipRecs = new list<Case_Ownership_History__c>(); 
		listOfCaseOwnershipRecs = [SELECT case__c, ownerid, Initial_Status__c, Date_Assigned__c, Date_Transferred__c
		 						   FROM Case_Ownership_History__c
		 						   WHERE case__c = :caseObj.id];
		System.assertEquals(listOfCaseOwnershipRecs.size(), 1);
		for(Case_Ownership_History__c singleCaseOwnershipRec : listOfCaseOwnershipRecs){
			DateTime dt = singleCaseOwnershipRec.Date_Assigned__c;
			System.assertEquals(date.today().format(), date.newinstance(dT.year(), dT.month(), dT.day()).format());
			System.assertEquals(singleCaseOwnershipRec.Date_Transferred__c, null);
		}
		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testor.com');
		insert u;
		//User userObj = SH_TestUtil.createUser(acc.id, 'Test', 'Test', 'a@bnsf.com', 'Jaipur', 'India', 'Rajasthan', 'Test', 'Test', 'B1145', heldRailPortalSettings.get('RoadMaster Profile Name'), heldRailPortalSettings.get('user id suffix'), 'test', true);
		
		caseObj.ownerid = u.id;
		update caseObj;
		
		listOfCaseOwnershipRecs = [SELECT case__c, ownerid, Initial_Status__c, Date_Assigned__c, Date_Transferred__c
		 						   FROM Case_Ownership_History__c
		 						   WHERE case__c = :caseObj.id];
		System.assertEquals(2, listOfCaseOwnershipRecs.size());
		
    }
    //----------------------------------------------------------------------------------------------------------
  	// Returns instance of Contact
	//----------------------------------------------------------------------------------------------------------
  	public static Contact createContact(ID accID, String firstName, String lastName, String email,
  									  String mailingCity, String mailingCountry, String mailingState,
  									  String mailingStreet, String mailingPostalCode, Boolean isInsert){
	  	Contact contactObj = new Contact();
	  	
		contactObj.accountID = accID;
		contactObj.LastName = lastName;
		contactObj.FirstName = firstName;
		contactObj.Email = email;
		contactObj.Experience__c = 2;
		contactObj.MailingCity = mailingCity;
		contactObj.MailingCountry = mailingCountry;
		contactObj.MailingState = mailingState;
		contactObj.MailingStreet = mailingStreet;
		contactObj.MailingPostalCode = mailingPostalCode;
		if (isInsert) {
	      insert contactObj;
	    }
	    return contactObj;
  	}
  	//-----------------------------------------------------------------------------------------------------------
  	// Return instance of Case
  	//----------------------------------------------------------------------------------------------------------
  	public static Case createCase(String subject, Id cntId, Boolean isInsert){
	    Case cs = new Case();
	    cs.Subject = subject;
	    cs.ContactId = cntId;
	    if (isInsert) {
	      insert cs;
	    }
	    return cs;
  	}
  
  	//----------------------------------------------------------------------------------------------------------
  	// Return instance of Account
 	//----------------------------------------------------------------------------------------------------------
  	public static Account createAccount(String accountName, Boolean isInsert){
	    Account account = new Account();
	    account.Name = accountName;
	    if(isInsert){
	        insert account;
	    }
	    return account;
	}
}