/**=====================================================================
 * Appirio, Inc
 * Class Name               : CampaignTriggerHandler
 * Description              : Trigger Handler for CampaignTrigger.trigger
 * Created Date             : 06/26/2015
 * Created By               : Sachin Agarwal
 * 
 * Date Modified                Modified By             Description of the update
 * mm/dd/yyy                   FirsName LastName          Reason
 =====================================================================*/
public with sharing class CampaignTriggerHandler {
	//----------------------------------------------------------------------------------------------------------------
    // Method executes on before Insert of Campaign
    //----------------------------------------------------------------------------------------------------------------
    public static void beforeInsert(List<Campaign> lstNew){
        updateCampaignMemberType(lstNew);
    }
    //Updates the CampaignMemberRecordTypeID appropriately as per the Campaign Record ID
    public static void updateCampaignMemberType(list<Campaign> lstNew){
    	SelectCampaignRTController sc = new SelectCampaignRTController();
    	map<String, String> mapBetweenCampaingMemberRT = sc.createCampaignRTMemberRTMap();
    	for(Campaign cmp : lstNew){
    		if(mapBetweenCampaingMemberRT.containsKey(cmp.RecordTypeId)){
    			cmp.CampaignMemberRecordTypeId = mapBetweenCampaingMemberRT.get(cmp.RecordTypeId);
    		}
    	}
    }
}