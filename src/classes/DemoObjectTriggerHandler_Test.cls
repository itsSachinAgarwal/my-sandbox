/*******************************************************************
Name  : DemoObjectTriggerHandler_Test
Author: Sachin Agarwal
Date  : Mar 2, 2016
Description: Test class for DemoObjectTriggerHandler
 *************************************************************************/
@isTest
private class DemoObjectTriggerHandler_Test {
    static testMethod void myUnitTest() {
    	list<Demo_Object__c> listOfDOs = new list<Demo_Object__c>();
    	listOfDOs.add(new Demo_Object__c(Category__c = 'Opportunity'));
    	listOfDOs.add(new Demo_Object__c(Category__c = 'Nothing'));
    	
    	insert listOfDOs;
    	listOfDOs.clear();
    	listOfDOs = [SELECT Opportunity__c
    				 FROM Demo_Object__c];
    	// Since first demo object record has Opportunity as category, therefore,
    	// Opportunity__c should get populated automatically with newly created opportunity 
    	System.assert(listOfDOs[0].Opportunity__c != null);
    	
    	// Since second demo object record doesn't have Opportunity as category, therefore,
    	// Opportunity__c shouldn't get populated automatically
    	System.assert(listOfDOs[1].Opportunity__c == null);
    }
}