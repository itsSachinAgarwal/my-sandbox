/**=====================================================================
 * Appirio, Inc
 * Class Name               : GeneratePDFController_Test
 * Description              : Test class for testing GeneratePDFController
 * Created Date             : 14/07/2015
 * Created By               : Sachin Agarwal
 * 
 * Date Modified                Modified By             Description of the update
 * mm/dd/yyy                   FirsName LastName          Reason
 =====================================================================*/
@isTest
private class GeneratePDFController_Test {
	//Method responsible for doing the actual testing.
    static testMethod void unitTest() {
    	String htmlCode = '<table><tr><td></td></tr></table>';
    	HTMLHolder__c holder = new HTMLHolder__c();
    	holder.htmlBody__c = htmlCode;
    	insert holder;
    	Test.startTest();
    	PageReference pg = Page.GeneratePDF;
    	Test.setCurrentPage(pg);
    	GeneratePDFController g = new GeneratePDFController();
    	list<HTMLHolder__c> listOfHolders = new list<HTMLHolder__c>();
    	listOfHolders = [SELECT id, htmlBody__c
    					 FROM HTMLHolder__c];
    	for(HTMLHolder__c h : listOfHolders){
    		System.assert(h.htmlBody__c == htmlCode);
    	}
    	Test.stopTest();
    }
}