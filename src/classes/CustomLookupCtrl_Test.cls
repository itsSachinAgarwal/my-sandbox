/**
* Name:  CustomLookupCtrl_Test
* Purpose: This is a test class for testing CustomLookupCtrl                                                         
* History                                                           
* 
* VERSION  AUTHOR                           DATE                Purpose                    
* 1.0 -    Sachin Agarwal ( Appirio JDC)   28/04/2015      INITIAL DEVELOPMENT
**/
@isTest
private class CustomLookupCtrl_Test {

    //----------------------------------------------------------------------------------------------------------
    // Main method responsible for testing the logic
    //----------------------------------------------------------------------------------------------------------
    static testMethod void unitTest() {    	
    	
    	Account newAccount = createAccount('Test Account', true);
        OpenLookupPopupCtrl obj = new OpenLookupPopupCtrl();
        	    
	    obj.selectedObject = 'Account';
        obj.fieldName = 'Name';
        obj.searchText = 'Test Account';
        obj.fieldSetName = 'account_search_result';
        obj.runSearch();
        CustomLookupCtrl customLookupCtrlObj = new CustomLookupCtrl();
        
        for(Sobject singleRecord : obj.listRecords){
        	customLookupCtrlObj.selectedId = singleRecord.id;
        	customLookupCtrlObj.fieldName = 'Name';
        	customLookupCtrlObj.objName = 'Account';
        	customLookupCtrlObj.getSelectedRecord();
        	System.assertEquals(customLookupCtrlObj.selectedRecordName, 'Test Account');
        }
    }
    //----------------------------------------------------------------------------------------------------------
    // Method being used to create sample Account
    //----------------------------------------------------------------------------------------------------------
	public static Account createAccount(String accountName, Boolean isInsert){
	    Account account = new Account();
	    account.Name = accountName;
	    if(isInsert){
	        insert account;
	    }
	    return account;
	}
}