global class BirthdayName implements Schedulable
{
    global void execute (SchedulableContext ctx) 
    { 
        sendBirthdayEmail(); 
    }

    public void sendBirthdayEmail()
    {
        EmailTemplate template = [Select id, Body from EmailTemplate where name = 'Birthday Wish'];
        for(Contact con : [SELECT name, Id FROM Contact WHERE birthdate = : system.Today().addDays(2)])
        {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
            mail.setTemplateId(template.id); 
            mail.setTargetObjectId(con.id); 
            mail.setSaveAsActivity(false);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail}); 
        }
    }
}