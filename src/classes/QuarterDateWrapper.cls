//
// (c) 2012 Appirio, Inc.
//
// Class for holding quarter start dates and end dates
//
// 16 Dec 2015 Sachin Agarwal Original
//
public with sharing class QuarterDateWrapper {
    public Date startDate{get;set;}
    public Date endDate{get;set;}

    // Constructor for initializing the class variables
    // @param startDateP a date representing the start date of a quarter
    // @param endDateP a date representing the end date of a quarter
    public QuarterDateWrapper(Date startDateP, Date endDateP){
        startDate = startDateP;
        endDate = endDateP;
    }
}