/**=====================================================================
 * Appirio, Inc
 * Class Name               : CampaignTriggerHandler_Test 
 * Description              : Test class for testing CampaignTriggerHandler.cls
 * Created Date             : 06/26/2015
 * Created By               : Sachin Agarwal
 * 
 * Date Modified                Modified By             Description of the update
 * mm/dd/yyy                   FirsName LastName          Reason
 =====================================================================*/
@isTest
private class CampaignTriggerHandler_Test {

    static testMethod void unitTest() {
        list<CampaignRT_MemberRT_Mapping__c> crt = new list<CampaignRT_MemberRT_Mapping__c>();
        crt.add(new CampaignRT_MemberRT_Mapping__c(Name = '1', Campaign_Member_RT__c = 'Application Based Program Member', Campaign_RT__c = 'Application Based Program'));
        crt.add(new CampaignRT_MemberRT_Mapping__c(Name = '2', Campaign_Member_RT__c = 'Mentorship Based Program Member', Campaign_RT__c = 'Mentorship Based Program'));
        insert crt;
        Campaign cm = new Campaign();
        cm.Name = 'Test';
        SelectCampaignRTController sc = new SelectCampaignRTController();
        cm.RecordTypeId = sc.getRecordTypeId('Campaign', 'Application Based Program');
        insert cm;
        
        Campaign chk = [SELECT ID, CampaignMemberRecordTypeId
                        FROM Campaign
                        WHERE ID = :cm.id];
        System.assertEquals(chk.CampaignMemberRecordTypeId, sc.getRecordTypeId('CampaignMember', 'Application Based Program Member'));
    }
}