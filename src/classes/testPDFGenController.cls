@istest
public class testPDFGenController
{
	public static testmethod void testPDfController()
    {
        PDFGenController pg=new PDFGenController(new ApexPages.StandardController(new Student__c()));
        Class__c singlClass=new Class__c(name='1st');            
        insert singlClass;            
        Student__c singleStu=new Student__c(first_name__c='Rana', last_name__c='Singh', class__c=singlClass.id);

        PageReference pr=Page.StudentPDFGen;
        pr.getParameters().put('id',singleStu.id);
       	pr=pg.genPDF();
	}
}