public with sharing class ContactController {

    private ApexPages.StandardController controller {get; set;}
    
    public list<Contact> listOfContacts{get;set;}
    // the actual contact
    private Contact c;
    public ContactController(ApexPages.StandardController controller) {

        //initialize the stanrdard controller
        this.controller = controller;
        this.c = (Contact)controller.getRecord();
        
		setValues();
    }   
    
    public void setValues(){
    	Contact singleCt = [SELECT ID, accountid
    						FROM Contact
    						WHERE id = :c.id];
    	if(singleCt != null){
    		listOfContacts = [SELECT id, name
    						  FROM Contact
    						  WHERE accountid = :singleCt.accountid AND id <> :c.id];
    	}
    }
}