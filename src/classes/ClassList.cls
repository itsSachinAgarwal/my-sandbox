public class ClassList 
{
    public list<Class__c> listClasses{get;set;}
    public list<Class__c> obj{get;set;}
    public Class__c editObj{get;set;}
    public string selectedClassEdit{get;set;}
    
    public ClassList(ApexPages.StandardController controller)
    {
        listClasses=[select id, name from Class__c];
        editObj=null;
    }
    public void loadRecord()
    {
        obj=[select id,name, Detailed_Description__c,fee__c, maxsize__c from class__c where id=:selectedClassEdit];
    	for(class__c selectedObj:obj)
    	{
        	editObj=selectedObj;
    	}
    }
    public void saveObj()
    {
        try
        {
        		update editObj;
        		editObj=null;       		
        }
        catch(exception e)
        {            
        }
    }
    public void cancelobj()
    {
        editObj=null;
    }
}