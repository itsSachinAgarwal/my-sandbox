//
// (c) 2012 Appirio, Inc.
//
// Class for testing the logic in Util
//
// 28 Dec 2015 Sachin Agarwal Original
//
@isTest
public with sharing class Util_Test {
    //Method that will perform the actual testing
    public static testMethod void unitTest(){
        Date dt = System.today();
        list<QuarterDateWrapper> listOfQDW1s = Util.calculateQuarterDates(dt, 4);
        list<QuarterDateWrapper> listOfQDW2s = Util.calculateQuarterDates(dt.addMonths(4), 4);
        list<QuarterDateWrapper> listOfQDW3s = Util.calculateQuarterDates(dt.addMonths(8), 4);        
        list<QuarterDateWrapper> listOfQDW4s = Util.calculateQuarterDates(dt.addMonths(12), 4);
    }
}