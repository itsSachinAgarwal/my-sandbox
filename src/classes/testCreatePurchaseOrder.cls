@istest
public class testCreatePurchaseOrder
{
	public static testmethod void testingPord()
    {
        Purchase_Order__c Pobj=new Purchase_Order__c();
        insert Pobj;
        CustomProduct__c custPro=new CustomProduct__c(product_name__c='Mouse', unit_price__c=100);
        insert custPro;
        Order_Line_item__c oi=new Order_Line_item__c(product__c=custPro.id, quantity__c=1, Purchase_Order__c=Pobj.id);
        insert oi;
        CreatePurchaseOrder obj=new CreatePurchaseOrder(new ApexPages.StandardController(new Purchase_Order__c()));
        PageReference pr=Page.CreatePurchaseOrder;
        pr.getParameters().put('id',Pobj.id);
        Test.setCurrentPageReference(pr);
        PageReference pg=obj.save();
        
        obj.addRow();
        obj.deleteRow();
    }
}