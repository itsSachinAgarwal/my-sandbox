/**=====================================================================
 * Appirio, Inc
 * Class Name               : HTMLGen
 * Description              : This will generate the HTML code which we can use
 *                            to render as PDF
 * Created Date             : 14/07/2015
 * Created By               : Sachin Agarwal
 * 
 * Date Modified                Modified By             Description of the update
 * mm/dd/yyy                   FirsName LastName          Reason
 =====================================================================*/
@isTest
private class HTMLGen_Test {
	//Method responsible for doing the actual testing.
    static testMethod void unitTest() {
    	HTMLGen gen = new HTMLGen();
    	gen.generateCode();
    	list<HTMLHolder__c> listOfHolders = new list<HTMLHolder__c>();
    	listOfHolders = [SELECT id
    					 FROM HTMLHolder__c];
    	System.assert(listOfHolders.size() == 1);
    }
}