/*
	Static class holding static utility method used by MassAddEditDeleteRecordsController for 
	loading the records, validating the input parameters and processing of records with add,
	edit and delete support.
*/
public with sharing class MassAddEditDeleteRecordsUtil {
	
	public static Integer DEFAULT_ROW_SIZE = 10;
	public static Integer DEFAULT_ADD_MORE_SIZE = 5;
	
	/*
		Method for validating the required parameters passed as URL parameter to the controller.
	*/
	public static void validateRequiredParameters(String parentRecordId, Map<String, SObjectType> globalDescribe, String childObjectAPIName, 
					String parentRelationshipField, String fieldSetName) {
    	
    	if(String.isBlank(parentRecordId)) 
    		throw new MassAddEditDeleteRecordsException(system.label.Mass_Add_Edit_Delete_Error_Required_parameter_missing_parentId);
    	
    	// Validating if childObjectAPIName passed as parameter is valid or not by checking it api name with in map of 
    	// described object with the org.
    	if(String.isBlank(childObjectAPIName) || !globalDescribe.containsKey(childObjectAPIName)) 
    		throw new MassAddEditDeleteRecordsException(system.label.Mass_Add_Edit_Delete_Error_Required_parameter_missing_childObject);
    	
    	Map<String, SObjectField> describefieldMap = globalDescribe.get(childObjectAPIName).getDescribe().fields.getMap();
    	
    	// Validating child object parentRelationshipField passed as parameter. 
    	if(String.isBlank(parentRelationshipField) || !describefieldMap.containsKey(parentRelationshipField)) 
    		throw new MassAddEditDeleteRecordsException(system.label.Mass_Add_Edit_Delete_Error_Required_parameter_missing_parentField);
    	
    	Schema.DescribeSObjectResult describeSObjectResultObj = globalDescribe.get(childObjectAPIName).getDescribe();
    	
    	if(String.isBlank(fieldSetName) || !describeSObjectResultObj.FieldSets.getMap().containsKey(fieldSetName)) 
    		throw new MassAddEditDeleteRecordsException(system.label.Mass_Add_Edit_Delete_Required_parameter_missing_fieldSet);
	}
	
	/* 
    	Method to get field set on sobject using the field set name. 
    */
    public static List<Schema.FieldSetMember> getFieldSetFields(Schema.SObjectType sobjectType, String fieldSetName) {
        List<Schema.FieldSetMember> fieldSetMemberList; 
        if(!String.isBlank(fieldSetName)) {
	        Schema.DescribeSObjectResult describeSObjectResultObj = sobjectType.getDescribe();
		    Schema.FieldSet fieldSetObj = describeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
		    fieldSetMemberList = fieldSetObj.getFields();
        }
	    return fieldSetMemberList; 
    }
    
    /*
    	Method the get the name of the parent record using the parent record id passed as argument.
    */
    public static String getParentRecordName(String parentRecordId) {
    	String parentRecordName = '';
    	
    	Schema.SObjectType parentObjectType = ((ID)parentRecordId).getSObjectType();
	                
        Schema.DescribeSObjectResult dr = parentObjectType.getDescribe();
        String objectTypeName =  dr.getName();
        
        String queryStr = 'select Name from '+ objectTypeName +' where id =: parentRecordId limit 1';
        
        SObject parentRecord = Database.query(queryStr);
        if(parentRecord != null) parentRecordName = (String)parentRecord.get('Name');
        
        return parentRecordName;
    }
    
    /*
    	Method to load all child records using the object type, field set and parent record id passed as argument.
    */
    public static List<SObjectRowWrapper> loadChildRecordsByParentId(List<Schema.FieldSetMember> fieldSetMemberList, Schema.SObjectType sobjectType, String objectName, 
    		String parentFieldAPIName, String parentRecordId, Integer rowSize) {
    	
    	String queryStr = prepareSOQLQuery(fieldSetMemberList, objectName);
    	queryStr += ' where '+ parentFieldAPIName +' =: parentRecordId ';
    	
    	List<sObject> objectList = Database.query(queryStr);
		
		List<SObjectRowWrapper> sObjectRowWrapperList = new List<SObjectRowWrapper>();
		
		Integer counter = 0;
		for(sObject obj  : objectList) {
			// Preparing the object wrapper
			sObjectRowWrapper objectRowWrapper = new SObjectRowWrapper(counter, obj);
			sObjectRowWrapperList.add(objectRowWrapper);
			counter++;
		}
		
		// Adding blank rows
		if(sObjectRowWrapperList.size() < rowSize) {
			Integer lastRowIndex = sObjectRowWrapperList.get(sObjectRowWrapperList.size()-1).rowIndex;
			sObjectRowWrapperList.addAll(addBlankRows(sobjectType, rowSize - sObjectRowWrapperList.size(), lastRowIndex+1, parentFieldAPIName, parentRecordId));
		}
		return sObjectRowWrapperList;
    }
    
    /*
    	Method to add blanks rows as per rowCount passed as parameter. 
    */
    public static List<SObjectRowWrapper> addBlankRows(Schema.SObjectType sobjectType, Integer rowCount, Integer lastRowIndex, 
    			String parentFieldAPIName, String parentRecordId) {
    	List<SObjectRowWrapper> sObjectBlankRowWrapperList = new List<SObjectRowWrapper>();
    	
    	Integer counter = lastRowIndex;
    	for(Integer i = 0; i < rowCount; i++) {
    		// Creating a new sObject record.
    		sObject newObject = sobjectType.newSObject(null, true);
    		// Setting parent field value.
    		newObject.put(parentFieldAPIName, parentRecordId);
    		
    		sObjectBlankRowWrapperList.add(new SObjectRowWrapper(counter, newObject));
    		counter++;
    	}
    	return sObjectBlankRowWrapperList;
    }
    
    /*
		Method for preparing soql query with fields configured using field set list passed as argument
	*/
	public static String prepareSOQLQuery(List<Schema.FieldSetMember> fieldSetMemberList, String objectName) 
	{
	    List<String> fieldAPINameList = new List<String>();
	    for(Schema.FieldSetMember f : fieldSetMemberList) {
	    	fieldAPINameList.add(f.getFieldPath());
	    }
	    String query = 'select ' + String.join(fieldAPINameList, ',') + ' from '+ objectName ;
	    
	    return query;
	}
	
	/*
		Method for deleting selected row from the data structure and also deleteing the related records 
		form the data base if they exist. 
	*/
	public static void deleteSelectedRow(List<SObjectRowWrapper> sObjectRowWrapperList, Integer selectedRowIndex) {
		SObjectRowWrapper removedsObjectRowWrapper = sObjectRowWrapperList.remove(selectedRowIndex);
		
		if(removedsObjectRowWrapper != null) {
			if(removedsObjectRowWrapper.record != null && removedsObjectRowWrapper.record.Id != null) {
				// deleting the record with id. 
				delete removedsObjectRowWrapper.record;
			}
		}
	}
	
	// Custom exception class for handling Mass Add/Edit/Delete of Records
	public class MassAddEditDeleteRecordsException extends Exception {}
	
	/*
		Wrapper class holding sObject and related details to show on UI
	*/
	public class SObjectRowWrapper {
	  	public Integer rowIndex {get;set;}
	  	public sObject record {get;set;}
	  	
	  	public String rowStatus {get; private set;}
	  	public boolean isRowdirty {get;set;}
	  	public boolean isSelected {get;set;}
	  	
	  	public SObjectRowWrapper(Integer index, sObject obj) {
	  		this.rowIndex = index;
	  		this.record = obj;
	  		this.isRowdirty = false;
	  		this.isSelected = false;
	  		this.rowStatus = obj.id != null ? 'Saved' : 'New';
	  	}
	}
}