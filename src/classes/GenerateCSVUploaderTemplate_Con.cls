public class GenerateCSVUploaderTemplate_Con {
    
    public String headers {get;set;}
    public CSV_Uploader_Config__c config{get;set;}
    public String contentType{get;set;}
    String sRecordId;
    
    public GenerateCSVUploaderTemplate_Con(ApexPages.StandardController controller) {
        sRecordId = controller.getId();        
    }
    
    public void generateTemplate(){
        contentType = 'text/html';
        headers ='';
        if(sRecordId!=null){
                config = [SELECT Id,Object_API_Name__c,Template_Name__c,Name,(SELECT CSV_Column_Name__c FROM CSV_Uploader_Field_Mapping__r) FROM CSV_Uploader_Config__c WHERE Id=:sRecordId];
                if(config.Template_Name__c==null){
                    contentType = 'text/plain#'+config.Name+'.csv';
                }
                else{
                    contentType = 'text/plain#'+config.Template_Name__c+'.csv';
                }
                for(CSV_Uploader_Field_Mapping__c mapping : config.CSV_Uploader_Field_Mapping__r){
                    headers = headers +( headers ==''? mapping.CSV_Column_Name__c : ','+ mapping.CSV_Column_Name__c);
                }
            
        }
        
    }

}