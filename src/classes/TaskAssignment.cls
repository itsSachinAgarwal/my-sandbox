/*
Name			: TaskAssignment
Created By		: Nitish Bansal
Created Date 	:
Purpose			:  
*/

// This code can easily be modified to work with any other objects as per the requirement.

/*
This is the controller class for TaskAssignmentPage.
It assigns and un-assign tasks to contacts using VF Remoting on drag-drop of tasks on the UI.
1. getTaskList : to get the list of desired tasks
2. getContactList : to get the list of desired contacts
3. assignTaskToContact : to assign tasks to contacts
4. unAssignTaskToContact : to un-assign tasks from contacts
*/

public without sharing class TaskAssignment{
    
    public List<Task> getTaskList(){
        return [Select t.Id, t.WhoId, t.Subject, t.Status, t.AccountId,  t.CreatedDate From Task t where t.Subject='Call' 
                                or t.Subject='Email' limit 5];
    }
    public LIst<Contact> getContactList(){
        return [Select c.Phone, c.Name, c.MailingCountry, c.Email, (Select t.Id, t.WhoId, t.Subject, t.Status, t.AccountId,  
        t.CreatedDate From Tasks t), c.Birthdate, c.AccountId From Contact c limit 7];
    }
    
    @RemoteAction
    public static String assignTaskToContact(String taskId, String contactId){
        boolean success = false;
        system.debug('### Ids ==>'+taskId+'=='+contactId);
        List<Contact> contacts = [Select c.Phone, c.Name, c.MailingCountry, c.Email, c.Birthdate, c.AccountId From Contact c Where 
                                    id =: contactId];
        List<Task> tasks = [Select t.Id, t.WhoId, t.Subject, t.Status, t.AccountId,  t.CreatedDate From Task t Where id =:taskId];
        if(tasks.size()>0 && contacts.size()>0){
            Task t = tasks.get(0);
            t.WhoId = contacts.get(0).Id;
            update t;
            success = true;
        }
        
        if(success){
            return 'Task Assigned to '+contacts.get(0).Name;
        }else{
            return 'Task not Assigned to '+contacts.get(0).Name;
        }   
    }
    
    @RemoteAction
    public static String unAssignTaskToContact(String taskId){
        boolean success = false;
        system.debug('### Id ==>'+taskId);
        List<Task> tasks = [Select t.Id, t.WhoId, t.Subject, t.Status, t.AccountId,  t.CreatedDate From Task t Where id =:taskId];
        if(tasks.size()>0){
            Task t = tasks.get(0);
            t.WhoId = null;
            update t;
            success = true;
        }
        
        if(success){
            return 'Task Un-Assigned from Contact';
        }else{
            return 'Task not Un-Assigned From Contact';
        }   
    }
}