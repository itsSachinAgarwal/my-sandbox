@istest
public class testDeleteStudentsControl 
{
	public static testmethod void delStudCon()
    {
        Class__c singlClass=new Class__c(name='1st');            
        insert singlClass;            
        Student__c singleStu=new Student__c(first_name__c='Rana', last_name__c='Singh', class__c=singlClass.id);
        insert singleStu;
		PageReference pg=Page.Delete_students;
        pg.getParameters().put('selectedId',singleStu.id);
        Test.setCurrentPage(pg);
        DeleteStudentsControl ds=new DeleteStudentsControl();
        pg=ds.DeleteStudents();
    }
}