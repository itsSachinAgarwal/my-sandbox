@istest
public class TestfindContacts
{
    public static testmethod void testcheckContacts()
    {
        Account acc=new Account(name='Kalpu', contact_ids__c=null);   
        insert acc;
        Contact singleCt=new Contact(lastname='Gupta',experience__c=10, accountid=acc.id);
        insert singleCt;
        Contact secoCt=new Contact(lastname='Jai',experience__c=10);
        insert secoCt;
        
        findContacts.checkContacts(singleCt.id);
        acc.Contact_IDs__c=singleCt.id;
        update acc;
		findContacts.checkContacts(singleCt.id);   
        findContacts.checkContacts(secoCt.id); 
    }
}