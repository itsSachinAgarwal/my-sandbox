/*******************************************************************
Name  : DemoObjectTriggerHandler
Author: Sachin Agarwal
Date  : Mar 2, 2016
Description: Trigger handler for Demo Object trigger 
 *************************************************************************/
public with sharing class DemoObjectTriggerHandler {
	// Method that will process all the inserted demo object records and update the opportunity field
	// as per the requirement
	// @param listOfDOs a list of demo object records which have been recently inserted
	public static void processDemoRecords(list<Demo_Object__c> listOfDOs){
		try{
			list<Opportunity> listOfOpps = new list<Opportunity>(); 
			// Going through all the records to find records where category is equal to Opportunity
			for(Demo_Object__c singleDO : listOfDOs){
				if(singleDO.Category__c == 'Opportunity'){
					listOfOpps.add(new Opportunity(Name = 'Sample', StageName = 'Won', CloseDate = Date.today()));
				}
			}
			// Inserting all the opportunities
			insert listOfOpps;
			integer i = 0;
			// Assigning the oppoortunities to the demo object records
			for(Demo_Object__c singleDO : listOfDOs){
				if(singleDO.Category__c == 'Opportunity'){
					singleDo.Opportunity__c = listOfOpps[i].id;
					i++;
				}
			}
		}
		catch(Exception ex){
			System.debug('Error : ' + ex.getMessage());
		}
	}   
}