/* 
# Created by............: Nathan Shilling
# Created Date..........: 01/10/2013
# Last Modified by......: 
# Last Modified Date....: 

This class is 
    
*/

public without sharing class CaseTriggerHandler {
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    
    public static boolean firstRun = true;
    
    public CaseTriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
        
    public void OnBeforeInsert(Case[] newCases){
        //Example usage
        for(Case newCase : newCases){

        }
    }
    
    public void OnAfterInsert(Case[] newCases){

    }
    
    @future public static void OnAfterInsertAsync(Set<ID> newCaseIDs){
        //Example usage
        List<Case> newCases = [select Id from Case where Id IN :newCaseIDs];
    }
    
    public void OnBeforeUpdate(Case[] oldCases, Case[] updatedCases, Map<ID, Case> CaseMap, Map<ID, Case> CaseMapOld){

        // lead assignment rules execute after 1st pass of triggers
        // they will fire before/after update one more time
        assignCasesRoundRobin(oldCases, updatedCases, CaseMapOld);                              
        return;
    
    }
    
    public void OnAfterUpdate(Case[] oldCases, Case[] updatedCases, Map<ID, Case> CaseMap, Map<ID, Case> CaseMapOld){
        
        if (firstRun) {
            firstRun = false;
        }
        else {
            System.debug('Already ran!');               
            return;
        }       
        
    }
    
    @future public static void OnAfterUpdateAsync(Set<ID> updatedCaseIDs){
        List<Case> updatedCases = [select Id from Case where Id IN :updatedCaseIDs];
    }
    
    public void OnBeforeDelete(Case[] CasesToDelete, Map<ID, Case> CaseMap){
        
    }
    
    public void OnAfterDelete(Case[] deletedCases, Map<ID, Case> CaseMap){
        
    }
    
    @future public static void OnAfterDeleteAsync(Set<ID> deletedCaseIDs){
        
    }
    
    public void OnUndelete(Case[] restoredCases){
        
    }
    
    public boolean IsTriggerContext{
        get{ return m_isExecuting;}
    }
    
    public boolean IsVisualforcePageContext{
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsWebServiceContext{
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsExecuteAnonymousContext{
        get{ return !IsTriggerContext;}
    }

    

    /**
     * assignCasesRoundRobin
     *
     * Use Assignment_Group_Queues__c to round-robin assign Cases
     * Design assumes that out-of-box Case Assignment rules will be used
     * to assign Cases to a Queue. If the Queue is defined in Assignment_Group_Queues__c
     * object, then the next user in the Assignment_Group_Queues__c related list will be assigned instead.  
     */
     private static void assignCasesRoundRobin(Case[] oldCases, Case[] updatedCases, Map<ID, Case> CaseMapOld) 
     {
            
            Map<Integer,Id> queueIds = new Map<Integer,Id>();   //Trigger index --> Queue ID
            Integer idx = 0;
            
            for (Case cse : updatedCases)
            {
            
                if(Trigger.isUpdate) 
                {                     
                    //
                    // detect change in OwnerId
                    //
                    if(cse.OwnerId <> CaseMapOld.get(cse.id).OwnerId) 
                    {  
                        queueIds.put(idx, cse.OwnerId);
                    }           
                }else {
                    queueIds.put(idx, cse.OwnerId);
                }      
                idx++;  
            }
            
            //
            // If no queueid's found, return
            //          
            if (queueIds.isEmpty()) return;
            
            //
            // Find active Assignment Group for Queue
            //
            Map<Integer,Id> asgnGroupNameIds = new Map<Integer,Id>();   //Trigger index --> Assignment_Group_Name ID
            Map<Id,Assignment_Group_Queues__c> asgnGroupQueues = new Map<Id,Assignment_Group_Queues__c>(); //Queue ID --> Assignment Group Queues
            
            for(Assignment_Group_Queues__c[] agq : [SELECT  Assignment_Group_Name__c, 
                                                            QueueId__c
                                                    FROM    Assignment_Group_Queues__c 
                                                    WHERE   QueueId__c in :queueIds.values()
                                                    AND     Active__c = 'True'
                                                    AND     Assignment_Group_Name__r.Type__c = 'Case'])
            {
                for (Integer i = 0; i < agq.size() ; i++) {
                    asgnGroupQueues.put(agq[i].QueueId__c, agq[i]);
                }                                           
            }
            
            // 
            // if no maching Assignment Groups found for the queues, return
            //          
            if (asgnGroupQueues.isEmpty()) return;
        
            for (Integer i : queueIds.keySet()) 
            {
                Assignment_Group_Queues__c agq = asgnGroupQueues.get(queueIds.get(i));
                
                if (agq <> null) 
                {
                    asgnGroupNameIds.put(i, agq.Assignment_Group_Name__c);
                }
                //else no active assignment group queue error
            }
            
            if (asgnGroupNameIds.isEmpty()) return;
            
            //
            // Determine next valid user in Queue/Assignment Group for round robin
            // User with earliest last assignment date wins.
            //
            Map<Id,Assignment_Groups__c[]> asgnGroups = new Map<Id,Assignment_Groups__c[]>(); // Assignment Group Name ID --> Assignment_Groups__c (User ID)
            for(Assignment_Group_Name__c agn : [SELECT  a.Name, 
                                                        a.Id, 
                                                       (SELECT      Id, 
                                                                    Name, 
                                                                    Group_Name__c, 
                                                                    Active__c, 
                                                                    Last_Assignment__c, 
                                                                    Millisecond__c, 
                                                                    Status__c, 
                                                                    User_Active__c, 
                                                                    User__c 
                                                        FROM        Assignment_Groups__r
                                                        WHERE       Active__c = 'True' 
                                                                    AND User_Active__c = 'True'
                                                        ORDER BY    Last_Assignment__c, Millisecond__c) 
                                                FROM    Assignment_Group_Name__c a    
                                                WHERE   a.Id in :asgnGroupNameIds.values()
                                                        AND a.Type__c = 'Case'
                                                FOR UPDATE ])
            {                                               
                asgnGroups.put(agn.ID, agn.Assignment_Groups__r);                                           
            }
               
            if (asgnGroups.isEmpty()) return;
                                                   
            Map<Id,Assignment_Groups__c> updateAssignmentGroups = new Map<Id,Assignment_Groups__c>();
            Map<Id, datetime> latestAGDateTime = new Map<Id,datetime>();
            idx = 0;    
            Map<ID,ID> mapCaseIdOwnerId = new Map<ID,ID>();
            
            for (Integer i : queueIds.keySet())
            {
            
                Assignment_Groups__c[] ags = asgnGroups.get(asgnGroupNameIds.get(i));
            
                if (ags.size()>0)
                {   
                    //
                    // Choose next user in line if user ID has already been used 
                    // but not committed in this trigger batch 
                    //
                    Assignment_Groups__c ag = ags[math.mod(idx, ags.size())];
                        
                    //
                    // Assign User to Case as the new owner
                    //
                    System.debug('>>>>>Owner changed for Case ' + updatedCases[i].Id + ' from '+updatedCases[i].OwnerId+' to '+ ag.User__c);
                    
                    updatedCases[i].OwnerId = ag.User__c;
                            
                    //
                    // Set last assignment datetime
                    //
                    datetime now = datetime.now();
                    ag.Last_Assignment__c = now;
                    ag.Millisecond__c = now.millisecondGMT();
                    
                    //
                    // update only latest Assignment Groups per ID
                    //
                    if (latestAGDateTime.containsKey(ag.id)) {
                        if(latestAGDateTime.get(ag.id) < now) {
                            updateAssignmentGroups.put(ag.id, ag);
                            latestAGDateTime.put(ag.id, now);
                        }
                    } else {
                        updateAssignmentGroups.put(ag.id, ag);
                        latestAGDateTime.put(ag.id,now);
                    }
                    
                    idx++;
                }
            }
                
            //
            // Map --> List/Array for DML update
            //
            List<Assignment_Groups__c> updateAG = new List<Assignment_Groups__c>();
            for (Id agId : updateAssignmentGroups.keySet()) {
                updateAG.add(updateAssignmentGroups.get(agId));
            }
        
            //
            // Update last assignment for Assignment Group in batch
            //
            if (updateAG.size()>0) {
                try {
                    update updateAG;
                    
                } catch (Exception e){
                    for (Integer i : queueIds.keySet())
                    {
                        updatedCases[i].addError('ERROR: Could not update Assignment Group records ' + ' DETAIL: '+e.getMessage());  
                    }
                }
            }           
     } 

}