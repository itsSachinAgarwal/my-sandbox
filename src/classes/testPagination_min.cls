@istest
public class testPagination_min
{
	public static testmethod void testSearch()
    {
        Pagination_min obj=new Pagination_min(new ApexPages.StandardController(new Account()));
        obj.billingCountry='US';
        obj.search();
        obj.clear();
        obj.billingCountry=null;
        obj.search();
        obj.billingState='CA';
        obj.search();
        obj.billingState=null;
        obj.billingCity='CA';
        obj.search();
        obj.billingState='CA';
        obj.billingCountry='US';
        obj.search();
        obj.billingCity=null;
        obj.search();
        obj.billingCity='CA';
        obj.billingState=null;
        obj.search();
        obj.billingCountry=null;
        obj.billingState='CA';
        obj.search();
        boolean flag=obj.hasPrevious;
        flag=obj.hasNext;
        obj.first();
        obj.last();
        integer page=obj.pageNumber;
        obj.next();
        obj.previous();
        PageReference p=obj.refresh();
    }
}