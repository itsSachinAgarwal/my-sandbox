global class NotifyOppurtunityOwner implements Schedulable
{
    global void execute (SchedulableContext ctx) 
    { 
        alertOwner(); 
    }

    public void alertOwner()
    {
      	list <Opportunity> oppLIST=new list<Opportunity>();
        oppLIST=[SELECT owner.id,owner.name,name, Id FROM Opportunity WHERE lastmodifieddate < : system.today().adddays(-30)];
        if(test.isRunningTest())
        {
            oppLIST=[SELECT owner.id,owner.name,name, Id FROM Opportunity];
        }    
       
        for(Opportunity opp : oppLIST)
        {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
            mail.setSubject('Alert:Opportunity details not modified');
            mail.setTargetObjectId(opp.owner.id);
            string body='Hi '+opp.owner.name+',<br/>Opportunity named '+opp.name+' has not been modified for the last 30 days. Kindly do the needful<br/>Thanks.';
            mail.setHTMLBody(body);
            
            mail.setSaveAsActivity(false);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail}); 
        }
    }
}