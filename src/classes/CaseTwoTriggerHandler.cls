public with sharing class CaseTwoTriggerHandler {
	public static void updateCaseOwner(list<Case> listOfCases){
		map<String, Id> mapOfQueueNameAndIds = new map<String, Id>();
		map<Id, String> mapOfIdsAndQueueNames = new map<Id, String>();
		//Finding all the queues
		for(Group g : [SELECT id, name 
					   FROM Group 
					   WHERE Type = 'Queue']){
			if(g.name != null){
				mapOfIdsAndQueueNames.put(g.id, g.name.trim().toLowerCase());
				mapOfQueueNameAndIds.put(g.name.trim().toLowerCase(), g.id);
			}
		}
		map<id, Round_Robin_Group__c> mapOfAlreadyExistingRRGroups = new map<id, Round_Robin_Group__c>();
		map<id, Round_Robin_Group__c> mapOfQueueIdAndRRGrp = new map<id, Round_Robin_Group__c>();
		map<id, Queue_Member__c> mapOfRRGrpAndPrimaryQueueMemberId = new map<id, Queue_Member__c>();
		map<id, Queue_Member__c> mapOfRRGrpAndSecondayQueueMemberId = new map<id, Queue_Member__c>();
		mapOfAlreadyExistingRRGroups = new map<id, Round_Robin_Group__c>([SELECT id, Queue_Name__c, Primary_User_Max_Limit__c, Secondary_User_Max_Limit__c, Primary_Member_Use_Count__c, Seconday_Member_Use_Count__c, Non_Primary_Secondary_Members_Use_Count__c, Number_Of_Non_Primary_Secondary_Members__c, 
																					(SELECT id, Last_Assignment__c, Primary_Member__c, Ready_for_Round_Robin_Assignment__c, Associated_User_Active__c, Secondary_Member__c, User__c
																					 FROM Queue_Members__r 
																					 WHERE Associated_User_Active__c = true AND Ready_for_Round_Robin_Assignment__c = true order by Last_Assignment__c)
										 								  FROM Round_Robin_Group__c]);
		
		for(Round_Robin_Group__c rrGrp : mapOfAlreadyExistingRRGroups.values()){
			if(rrGrp.Queue_Name__c != null){
				if(mapOfQueueNameAndIds.containsKey(rrGrp.Queue_Name__c.trim().toLowerCase())){
					mapOfQueueIdAndRRGrp.put(mapOfQueueNameAndIds.get(rrGrp.Queue_Name__c.trim().toLowerCase()), rrGrp);	
				}
			}
			for(Queue_Member__c qm : rrGrp.Queue_Members__r){
				if(qm.Primary_Member__c){
					mapOfRRGrpAndPrimaryQueueMemberId.put(rrGrp.id, qm);
				}
				else if(qm.Secondary_Member__c){
					mapOfRRGrpAndSecondayQueueMemberId.put(rrGrp.id, qm);
				}
			}
		}
		list<Round_Robin_Group__c> listOfRRGsAffected = new list<Round_Robin_Group__c>();
		list<Queue_Member__c> listOfQueueMembersAffected = new list<Queue_Member__c>(); 								 
		map<id, list<Case>> mapOfQueueIdAndCasesToBeProcessed = new map<id, list<Case>>(); 
		for(Case singleCase : listOfCases){
			System.debug('Hi');
			if(singleCase.Ownerid != null && String.valueOf(singleCase.Ownerid).startsWith('00G') && mapOfQueueIdAndRRGrp.containsKey(singleCase.Ownerid)){
				Round_Robin_Group__c rrGrp = mapOfQueueIdAndRRGrp.get(singleCase.Ownerid);
				if(rrGrp.Primary_Member_Use_Count__c < rrGrp.Primary_User_Max_Limit__c && mapOfRRGrpAndPrimaryQueueMemberId.containsKey(rrGrp.id)){
					singleCase.OwnerId =  mapOfRRGrpAndPrimaryQueueMemberId.get(rrGrp.id).User__c;
					rrGrp.Primary_Member_Use_Count__c += 1;
				}
				else if(rrGrp.Seconday_Member_Use_Count__c < rrGrp.Secondary_User_Max_Limit__c && mapOfRRGrpAndSecondayQueueMemberId.containsKey(rrGrp.id)){
					singleCase.OwnerId =  mapOfRRGrpAndSecondayQueueMemberId.get(rrGrp.id).User__c;	
					rrGrp.Seconday_Member_Use_Count__c += 1;
				}
				else{
					for(Queue_Member__c qm : rrGrp.Queue_Members__r){
						if(!qm.Primary_Member__c && !qm.Secondary_Member__c && rrGrp.Non_Primary_Secondary_Members_Use_Count__c < rrGrp.Number_Of_Non_Primary_Secondary_Members__c){
							System.debug('User is ' + qm.User__c);
							singleCase.OwnerId =  qm.User__c;
							qm.Last_Assignment__c = System.now();
							listOfQueueMembersAffected.add(qm);
							System.debug('qm.Last_Assignment__c' + qm.Last_Assignment__c);
							System.debug('rrGrp.Non_Primary_Secondary_Members_Use_Count__c before ' + rrGrp.Non_Primary_Secondary_Members_Use_Count__c);
							rrGrp.Non_Primary_Secondary_Members_Use_Count__c += 1;
							System.debug('rrGrp.Non_Primary_Secondary_Members_Use_Count__c After' + rrGrp.Non_Primary_Secondary_Members_Use_Count__c);
							break;
						}
					}
				}
				if(rrGrp.Non_Primary_Secondary_Members_Use_Count__c == rrGrp.Number_Of_Non_Primary_Secondary_Members__c){
					rrGrp.Primary_Member_Use_Count__c = 0;
					rrGrp.Seconday_Member_Use_Count__c = 0;
					rrGrp.Non_Primary_Secondary_Members_Use_Count__c = 0;
				}
				listOfRRGsAffected.add(rrGrp);			
			}
		}
		update listOfQueueMembersAffected;
		update listOfRRGsAffected;
	}
}