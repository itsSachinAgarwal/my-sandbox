//
// (c) 2012 Appirio, Inc.
//
// Class for holding utility methods
//
// 16 Dec 2015 Sachin Agarwal Original
//
public with sharing class Util {

    // Method responsible for calculating the quarter start and end dates
    // @param dt a date for which previous quarter dates have to be calculated
    // @param noOfQs an integer representing for how many previous quarters start and end date 
    // is to be provided.
    // @return a list of QuarterDateWrapper representing the start and end dates of previous quarters
    public static list<QuarterDateWrapper> calculateQuarterDates(Date dt, integer noOfQs){
        Set<Integer> Q1 = new Set<Integer>{1,2,3};
        Set<Integer> Q2 = new Set<Integer>{4,5,6};
        Set<Integer> Q3 = new Set<Integer>{7,8,9};
        Set<Integer> Q4 = new Set<Integer>{10,11,12};
        Date lastDateOfPrevQuarter;

        // Get the current date's month
        Integer dateMonth = dt.month();

        // If the current date falls in quarter 1
        if(Q1.contains(dateMonth)){
            Integer numberOfDays = Date.daysInMonth(dt.addYears(-1).year(), 12);
            lastDateOfPrevQuarter = Date.newInstance(dt.addYears(-1).year(), 12, numberOfDays); 
        }
        // If the current date falls in quarter 2
        else if(Q2.contains(dateMonth)){
            Integer numberOfDays = Date.daysInMonth(dt.year(), 3);
            lastDateOfPrevQuarter = Date.newInstance(dt.year(), 3, numberOfDays); 
        }
        // If the current date falls in quarter 3
        else if(Q3.contains(dateMonth)){
            Integer numberOfDays = Date.daysInMonth(dt.year(), 6);
            lastDateOfPrevQuarter = Date.newInstance(dt.year(), 6, numberOfDays); 
        }
        // If the current date falls in quarter 4
        else{
            Integer numberOfDays = Date.daysInMonth(dt.year(), 9);
            lastDateOfPrevQuarter = Date.newInstance(dt.year(), 9, numberOfDays); 
        }

        // Creating a list of all the start and dates for noOfQs previous quarters
        list<QuarterDateWrapper> listOfQDWs = new list<QuarterDateWrapper>();
        Date tempDate = lastDateOfPrevQuarter;
        for(integer i = 0; i < noOfQs ; i++){
            Date copyDate = tempDate;

            integer numberOfDays = 0;
            for(integer j = 0; j < 3 ; j++){
                numberOfDays = Date.daysInMonth(copyDate.year(), copyDate.month());
                copyDate = copyDate.addDays(numberOfDays * (-1));
            }
            listOfQDWs.add(new QuarterDateWrapper(copyDate.addDays(1), tempDate));
            tempDate = copyDate;
        }
        return listOfQDWs; 
    }
}