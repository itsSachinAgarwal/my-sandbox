/*
	@author sfdcdev11981 
	
	Class holding logic for processing the uploaded file and performing following operations: - 
	1. Insert : Performing insert of parse records in the uploaded file. The file uploaded for insert 
		should not contain "Id" column as it is not required for record insert.
	2. Update : Performing update of parsed records. The file uploaded for update must contain first 
		column as "Id" column. Generated file template support this formated as should be used for the same.
	3. Upsert : Performing upsert (insert and update) of parsed records. The file uploaded for 
		upsert must contain first column as "Id" column with option value for it. If Id value is 
		available that record will be consider for update. Else records will be consider for insert.
	
*/
public with sharing class CustomDataloaderController {
	
	// Vaiable holding converted content string 
	public String nameFile {get;set;}
	
	// Variable holding content of the uploadd file.
    public Blob contentFile {get;set;}
   	
   	public String selectedObject {get;set;} 
    public List<SelectOption> sObjectSelectOptionList {get;set;}
    
    public boolean disableUploadButtons {get {return String.isBlank(selectedObject); } private set;}
    
    public boolean showSuccessAndFailureReport {get {return (successAndFailureReports != null && !successAndFailureReports.isEmpty());} set;}
    
    // Vaiable holding success and failure details.
    public List<DataloaderUtil.SuccessAndFailureReport> successAndFailureReports {get;set;}
    
    private Map<String, Schema.SObjectType> globalDescribe;
    
    public CustomDataloaderController() {
    	globalDescribe = Schema.getGlobalDescribe();
    	
    	// getting list of object accessible by user.
    	sObjectSelectOptionList = DataloaderUtil.getSObjectList(globalDescribe);    	
    }
    
    // Empty method just to rerender download template section on UI 
    public void generateUploadTemplate() { }
    
    /*
    	Method for parsing the uploaded file and inserting the records. 
    	There is no Id column to be added or supported in insert file template. The insert file 
    	template generated does not contains id column. So use the same formate. 
    */
    public Pagereference insertFileRecords() {
    	List<sObject> objToInsertList = new List<sObject>();
    	try {
	       	nameFile = contentFile.toString();
	       
	       	List<Id> idList =  new List<Id>();
	       	Map<String,string> map1 = new Map<string,string>();
	       
	       	// parsing the uploaded file.
	       	List<List<String>> parsedFields = DataloaderUtil.parseCSV(nameFile, false);
	       	
	       	// Removing the header row at the top.
	       	List<String> headerFields = parsedFields.remove(0);
	       	
	       	// Checking if the uploaded file contains any column or not.  
	       	if(headerFields == null || headerFields.isEmpty()) throw new CustomDataloaderException('Invalid file. File does not contain any columns.');
	       	
	       	Schema.SObjectType objectType = globalDescribe.get(selectedObject);
	       	objToInsertList = DataloaderUtil.prepareRecordToInsertObjList(objectType, parsedFields, headerFields);
	       	
    	} catch(Exception e) {
    		ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception while parsing uploaded file. Error: '+e.getMessage())); return null;
    	} 
    	
    	Integer recordSize = objToInsertList.size();
    	try
    	{
           Database.SaveResult[] results = Database.insert(objToInsertList, false);
           successAndFailureReports = DataloaderUtil.prepareSuccessAndFailureReport(results);
           
        } catch(DMLException er) {
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception while inserting the processed file records. Error '+er.getMessage())); return null;
        }
   		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, recordSize+ ' records inserted successfully'));
       	return null;
    }
    
    /*
    	Method for parsing the uploaded file and updating the records based on the Id column value. 
    	Id column is added as firt column in the generated update file template. So same formate will be 
    	supported. 
    */
    public Pagereference updateFileRecords() {
    	
    	List<sObject> objToUpdateList = new List<sObject>();
    	try {
	       	nameFile = contentFile.toString();
	       
	       	List<Id> idList =  new List<Id>();
	       	Map<String,string> map1 = new Map<string,string>();
	       
	       	// parsing the uploaded file.
	       	List<List<String>> parsedFields = DataloaderUtil.parseCSV(nameFile, false);
	       
	        // getting all header columns.
	       	List<String> headerFields = parsedFields.remove(0);
	       	 
	       	// Checking if the uploaded file contains any column or not.  
	       	if(headerFields == null || headerFields.isEmpty()) throw new CustomDataloaderException('Invalid file. File does not contain any columns.');
	       	
	       	Map<String, List<String>> objToUpdateMap = new Map<String, List<String>>();
	       	for(List<String> fieldValues : parsedFields) {
	       		// Adding record Id as key and all other field values as map value.
	       		// Checking if record is blank or not. As record will non bank id will only be updated.
	       		if(!String.isBlank(fieldValues.get(0)))  objToUpdateMap.put(fieldValues.get(0), fieldValues);
	       	}
	       	
	       	Set<String> recordIdSet = objToUpdateMap.keySet();
	       	String queryStr = 'Select ' + String.join(headerFields, ',') + ' from  '+ selectedObject +' Where id IN : recordIdSet ';
	       	
	       	objToUpdateList = Database.query(queryStr);
	       	
	       	Map<String, Schema.DescribeFieldResult> describeFieldResultMap = DataloaderUtil.getDescribeFieldResultMap(globalDescribe.get(selectedObject), headerFields);
	       	
	       	// Looping through the each record to update the field values provided to the existing as part of uploaded file.
	       	for(sObject objToUpdate: objToUpdateList){

	       	 	String recordId = (String)objToUpdate.get('id');
	       	 	
	       	 	// Record id received as part of csv file can be of size 15. so need to try getting value using 15 char id also from the map created above. 
	           	List<String> fieldValues = objToUpdateMap.get(recordId) == null ? (recordId.length() > 15 ? objToUpdateMap.get(recordId.substring(0,15)) : objToUpdateMap.get(recordId)) : objToUpdateMap.get(recordId);
	           	
	           	// Starting the loop from index 1 to eskipe Id at 0 index. 
	           	for(Integer i=1; i < headerFields.size(); i++) {
	           		
	           		// Checking update access on the field for the user. 
        			if( describeFieldResultMap.get(headerFields.get(i)) != null &&  describeFieldResultMap.get(headerFields.get(i)).isUpdateable()
        					&& !String.isBlank(fieldValues.get(i))) // Checking for any null or blank value.
        			{
        				objToUpdate.put(headerFields.get(i), fieldValues.get(i));
        			}
	           	}
	       	}
    	} catch(Exception e) {
    		ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception while parsing uploaded file. Error: '+e.getMessage()));  return null;
    	} 
    	
    	Integer recordSize = objToUpdateList.size();
    	try
    	{
           Database.SaveResult[] results = Database.update(objToUpdateList, false);
           successAndFailureReports = DataloaderUtil.prepareSuccessAndFailureReport(results);
           
        } catch(DMLException er) {
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception while updating the processed file records. Error '+er.getMessage())); return null;
        }
   		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, recordSize+ ' records updated successfully'));
       	return null;
    }
    
    /*
    	Method for performing processing the uploaded file and upserting the records. 
    	Records for which id value is blank is consider for insert and other records are 
    	considered for update. 
    */
    public Pagereference upsertFileRecords() {
    	
    	List<sObject> objToUpdateList = new List<sObject>();
    	List<sObject> objToInsertList = new List<sObject>();
    	try {
	       	nameFile = contentFile.toString();
	       	
	       	List<Id> idList =  new List<Id>();
	       	Map<String,string> map1 = new Map<string,string>();
	       
	       	// parsing the uploaded file.
	       	List<List<String>> parsedFields = DataloaderUtil.parseCSV(nameFile, false);
	       	
	       	system.debug('------------- parsedFields '+ parsedFields);
	        
	        // getting all header columns.
	       	List<String> headerFields = parsedFields.remove(0);
	       	 
	       	// Checking if the uploaded file contains any column or not.  
	       	if(headerFields == null || headerFields.isEmpty()) throw new CustomDataloaderException('Invalid file. File does not contain any columns.');
	       	
	       	Schema.SObjectType objectType = globalDescribe.get(selectedObject);
	       	
	       	Map<String, Schema.DescribeFieldResult> describeFieldResultMap = DataloaderUtil.getDescribeFieldResultMap(objectType, headerFields);
	       	
	       	Map<String, List<String>> objToUpdateMap = new Map<String, List<String>>();
	       	for(List<String> fieldValues : parsedFields) {
	       		
	       		// Adding record Id as key and all other field values as map value.
	       		// Checking if record is blank or not. As record will non bank id will only be updated.
	       		if(!String.isBlank(fieldValues.get(0))) 
	       		{
	       			objToUpdateMap.put(fieldValues.get(0), fieldValues);
        			
	       		} else {
	       			sObject objToInsert = objectType.newSObject();
		        	for(Integer i=1; i< headerFields.size();i++) {
		        		
		        		// Checking create access on the field for the user. 
        				if( describeFieldResultMap.get(headerFields.get(i)) != null &&  describeFieldResultMap.get(headerFields.get(i)).isCreateable()) {
			        		// Checking for any null or blank value and skiping Id field as it is of no use while preparing insert object list.
			        		if(!headerFields.get(i).equalsIgnoreCase('Id') && !String.isBlank(fieldValues.get(i))) objToInsert.put(headerFields.get(i), fieldValues.get(i));
        				}
		        	}
		        	objToInsertList.add(objToInsert);
	       		}
	       	}
	       	
	       	Set<String> recordIdSet = objToUpdateMap.keySet();
	       	String queryStr = 'Select ' + String.join(headerFields, ',') + ' from  '+ selectedObject +' Where id IN : recordIdSet ';
	       	
	       	objToUpdateList = Database.query(queryStr);
	       	
	       	// Looping through the each record to update the field values provided to the existing as part of uploaded file.
	       	for(sObject objToUpdate: objToUpdateList){

	       	 	String recordId = (String)objToUpdate.get('id');
	       	 	
	       	 	// Record id received as part of csv file can be of size 15. so need to try getting value using 15 char id also from the map created above. 
	           	List<String> fieldValues = objToUpdateMap.get(recordId) == null ? (recordId.length() > 15 ? objToUpdateMap.get(recordId.substring(0,15)) : objToUpdateMap.get(recordId)) : objToUpdateMap.get(recordId);
	           	
	           	// Starting the loop from index 1 to eskipe Id at 0 index. 
	           	for(Integer i=1; i < headerFields.size(); i++) {
	           		
	           		// Checking update access on the field for the user. 
        			if( describeFieldResultMap.get(headerFields.get(i)) != null &&  describeFieldResultMap.get(headerFields.get(i)).isUpdateable()
        					&& !String.isBlank(fieldValues.get(i))) // Checking for any null or blank value.
        			{
        				objToUpdate.put(headerFields.get(i), fieldValues.get(i));
        			}
	           	}
	       	}
    	} catch(Exception e) {
    		ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception while parsing uploaded file. Error: '+e.getMessage())); return null;
    	} 
    	
    	Integer recordUpdateSize = objToUpdateList.size();
    	Integer recordInsertSize = objToInsertList.size();
    	try
    	{
    		// Database upsert with false to all partial success.
           Database.UpsertResult[] upsertResults = Database.upsert(objToUpdateList, false);
           successAndFailureReports = DataloaderUtil.prepareSuccessAndFailureReport(upsertResults);
           
           Database.SaveResult[] insertResults = Database.insert(objToInsertList, false);
           successAndFailureReports.addAll(DataloaderUtil.prepareSuccessAndFailureReport(insertResults));
           
        } catch(DMLException er) {
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception while updating the processed file records. Error '+er.getMessage())); return null;
        }
   		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, recordUpdateSize+ ' records update successfully'));
   		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, recordInsertSize+ ' records insert successfully'));
       	return null;
    }
    
    // Custom exception to handle data loader specific exceptions.
    public class CustomDataloaderException extends Exception {}
    
}