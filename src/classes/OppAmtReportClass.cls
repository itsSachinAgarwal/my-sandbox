public class OppAmtReportClass 
{
    public AggregateResult[] queryresult{get;set;}
    public AccountSummary[] AccountSummaries{get;set;}
	public OppAmtReportClass()
    {
       	queryresult= [select accountid, account.name name, sum(amount)  total from opportunity group by accountid,account.name limit 10];
        AccountSummaries = new List<AccountSummary>();
        for (AggregateResult ar : queryresult) {
            AccountSummaries.add(new AccountSummary(ar));
        }
	}   
    
    public class AccountSummary
    {
        public id AccountID{get;set;}
        public double totalAmount{get;set;}
        public String AccountName{get;set;}
        public AccountSummary(AggregateResult ar)
        {
            AccountID=(ID)ar.get('accountid');
            totalAmount=(Decimal)ar.get('total');
            AccountName=(String)ar.get('name');
        }
    }
}