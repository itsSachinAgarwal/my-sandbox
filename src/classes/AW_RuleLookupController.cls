// 
// (c) 2012 Appirio, Inc.
//
//  AW_RuleLookupController                                                    
//criteriaList
// Class for Lookup Rules on Simulation Page
//
// 07/14/2014
public with sharing class AW_RuleLookupController {
	public String objectName{get;set;}
	public String searchText{get;set;}
	private ApexPages.StandardSetController stdController;
	public List<Rule__c> searchedSobject{get{
			if(stdController != null){
				return (List<Rule__c>)stdController.getRecords();
			}
			return new List<Rule__c>();
	}set;}
	
	public AW_RuleLookupController(){
		objectName = ApexPages.currentPage().getParameters().get('objName');
		searchText = ApexPages.currentPage().getParameters().get('sText');
		if(objectName != null){
			loadSearchRecords();
		}
	} 
	
	public void loadSearchRecords(){
		Integer stdPageSize = 20;
		String soqlQuery = 'select id , Name, Status__c  FROM Rule__c WHERE Object__c  = : objectName AND Status__c in (\'Active\' , \'Draft\') ';
		if(searchText != null && searchText.trim() != ''){
			String likeFilter = searchText +'%';
			soqlQuery += ' AND Name like : likeFilter';
		}
		stdController = new ApexPages.StandardSetController(Database.query(soqlQuery));
		AWE_Rule_Lookup_Page_Size__c pageSizeInstance = AWE_Rule_Lookup_Page_Size__c.getInstance('Default');
		
		if(pageSizeInstance != null && pageSizeInstance.page_Size__c != null){
			stdPageSize = Integer.valueOf(pageSizeInstance.page_Size__c);
		}
		stdController.setPageSize(stdPageSize);
	}
	
	public PageReference search(){
		loadSearchRecords();
		return null;
	}
	//Boolean to check if there are more records after the present displaying records
    public Boolean hasNext{
        get
        {
            if(stdController != null){
                return stdController.getHasNext();
            }
            return false;
        }set;}

    //Boolean to check if there are more records before the present displaying records
    public Boolean hasPrevious{
        get
        {
            if(stdController != null){
                return stdController.getHasPrevious();
            }
            return false;
        }set;}

    //Page number of the current displaying records
    public Integer pageNumber{
        get
        {
            if(stdController != null){
                return stdController.getPageNumber();
            }
            return 0;
        }set;}

     //Page number of the Size
    public Integer pageSize{
        get
        {
            if(stdController != null){
                return stdController.getPageSize();
            }
            return 0;
        }set;}

    //Returns the previous page of records
    public void previous(){
        if(stdController != null){
            stdController.previous();
        }
    }

    //Returns the next page of records
    public void next(){
       if(stdController != null){
            stdController.next();
        }
    }

	

}