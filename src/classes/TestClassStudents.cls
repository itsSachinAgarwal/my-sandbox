@istest
public class TestClassStudents
	{
        public Static testmethod void TestfindStudentNames ()
        {
            Class__c singlClass=new Class__c(name='1st');            
            insert singlClass;            
            Student__c singleStu=new Student__c(first_name__c='Rana', last_name__c='Singh', class__c=singlClass.id);
            insert singleStu;
            singleStu=new Student__c(first_name__c='Geet', last_name__c='Singh', class__c=singlClass.id);
            insert singleStu;                        
            list<student__c> studentnames=ClassStudents.findStudentNames(singlClass.id);
            for(student__c std:studentnames)
            {
                System.debug(std.first_name__c);
            }
            
            classstudents obj=new classstudents();
			map<class__c, set<id>> mymaps=new  map<class__c, set<id>>();
			mymaps=obj.getStudentIDs();
			set <class__c> classes=new set <class__c> ();
			classes=mymaps.keyset();
			for(class__C singleclass:classes)
			{
    			system.debug(singleclass.name);
    			system.debug(mymaps.get(singleclass));
			}
            
            DATE a=date.newinstance(2014,11,28);
			DATE b=date.newinstance(2014,11,30);			
			integer count=obj.countworkingdays(a,b);
			system.debug(count);
        }
}