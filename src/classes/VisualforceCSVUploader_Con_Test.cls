@isTest
public class VisualforceCSVUploader_Con_Test{
    
    static testMethod void Test_VisualforceCSVUploader_Con(){
        //create a config first
        CSV_Uploader_Config__c config = new CSV_Uploader_Config__c(Object_API_Name__c='Account',Template_Name__c = 'Account');
        insert config;
        //create field mappings
        CSV_Uploader_Field_Mapping__c mapping = new CSV_Uploader_Field_Mapping__c(CSV_Column_Name__c='Name',Field_API_Name__c='Name');
        mapping.CSV_Uploader_Config__c = config.Id;
        insert mapping;
        mapping = new CSV_Uploader_Field_Mapping__c(CSV_Column_Name__c='Industry',Field_API_Name__c='Industry');
        mapping.CSV_Uploader_Config__c = config.Id;
        insert mapping;
        mapping = new CSV_Uploader_Field_Mapping__c(CSV_Column_Name__c='NumberOfEmployees',Field_API_Name__c='NumberOfEmployees');
        mapping.CSV_Uploader_Config__c = config.Id;
        insert mapping;
        mapping = new CSV_Uploader_Field_Mapping__c(CSV_Column_Name__c='AnnualRevenue',Field_API_Name__c='AnnualRevenue');
        mapping.CSV_Uploader_Config__c = config.Id;
        insert mapping;
        //create a csv file with 2 columns
        blob csvFile = createTestCSV('TestAccount1,TestIndustry,10,100000',4000);
        Test.startTest();
        VisualforceCSVUploader_Con controller = new VisualforceCSVUploader_Con();
        //press the upload button without any file selection
        controller.upload();
        //should throw an message
        System.assertEquals(true, Apexpages.hasMessages(ApexPages.Severity.ERROR));
        //select a file the file doesnt contain the header name
        controller.csvFileName ='Account.csv';
        controller.csvFile =csvFile ;
        controller.upload(); 
        //should throw an error
        System.assertEquals(true, Apexpages.hasMessages(ApexPages.Severity.ERROR));
        //add proper headers
        csvFile = Blob.valueOf('Name,Industry,NumberOfEmployees,AnnualRevenue\r'+csvFile.toString());
        controller.csvFileName ='A1ccount.csv';
        controller.csvFile =csvFile ;
        controller.upload();
         //should throw an error
        System.assertEquals(true, Apexpages.hasMessages(ApexPages.Severity.ERROR));
        //correct the file name and upload
        controller.csvFileName ='Account.csv';
        controller.csvFile =csvFile ;
        controller.upload();
        System.assertEquals(4000,[SELECT Id FROM Account WHERE Name = 'TestAccount1'].size());
        //try with invalid config
        config.Object_API_Name__c='xxxxAccountxxxxx';
        update config;
        controller.csvFileName ='Account.csv';
        controller.csvFile =csvFile ;
        controller.upload();
        Test.stopTest();
    }
    
    
    static testMethod void Test_GenerateCSVUploaderTemplate_Con(){
        //create a config first
        CSV_Uploader_Config__c config = new CSV_Uploader_Config__c(Object_API_Name__c='Account');
        insert config;
        //create field mappings
        CSV_Uploader_Field_Mapping__c mapping = new CSV_Uploader_Field_Mapping__c(CSV_Column_Name__c='Name',Field_API_Name__c='Name');
        mapping.CSV_Uploader_Config__c = config.Id;
        insert mapping;
        mapping = new CSV_Uploader_Field_Mapping__c(CSV_Column_Name__c='Industry',Field_API_Name__c='Industry');
        mapping.CSV_Uploader_Config__c = config.Id;
        insert mapping;
        //start test
        Test.startTest();
        GenerateCSVUploaderTemplate_Con  controller = new GenerateCSVUploaderTemplate_Con (new ApexPages.StandardController(config));
        controller.generateTemplate();
        System.assertNotEquals(null,controller.headers);
        config = [SELECT Name,Id FROM CSV_Uploader_Config__c WHERE Id=:config.Id];
        System.assertEquals('text/plain#'+config.Name+'.csv',controller.contentType);
        config.Template_Name__c = 'Account';
        update config;
        controller.generateTemplate();
        System.assertNotEquals(null,controller.headers);
        System.assertEquals('text/plain#'+config.Template_Name__c +'.csv',controller.contentType);
        Test.stopTest();
    }
    
     static blob createTestCSV(String data,Integer count){
        blob csvFile;
        String csvContent='';
        for(Integer i=1;i<count+1;i++){
            csvContent =csvContent+ (data+'\r');
        }
        
        csvFile = Blob.valueOf(csvContent);
        
        return csvFile;
    }
}