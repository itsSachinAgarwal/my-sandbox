/*
	@author sfdcdev11981
	
	Class for generating Custom Data loader template excel file to be used to upload data using 
	the custom data loader created as part of this app. The generated excel template file only 
	contains those fields that user have access i.e create, update and read access. 
*/
public with sharing class CustomDataloaderTemplateController {
	
	public List<String> selectedObjFieldList {get;set;}
	public String selectedObjectName {get; private set;}
	public String templateType {get; private set;}
	
	public boolean showTemplateSection {get {return String.isBlank(failureMessage) ? true : false; } private set;}
	
	public String failureMessage {get; private set;}
	
	public CustomDataloaderTemplateController() {
		failureMessage = null;
		// getting object type passed as parameter. 
		selectedObjectName = Apexpages.currentpage().getparameters().get('objtype');
		
		// Getting tempalte type passed as parameter. Supported type are insert, update and upsert.
		templateType = Apexpages.currentpage().getparameters().get('temtype');
		
		templateType = String.isBlank(templateType) ? DataloaderUtil.DEFAULT_TEMPLATE_TYPE : templateType;
		
		if(String.isBlank(selectedObjectName)) {
			failureMessage = 'Required object type is missing. Failed to genetate dataloader template.'; return;
		}
		
		try {
			Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
			selectedObjFieldList = DataloaderUtil.getObjectFields(selectedObjectName, globalDescribe, templateType);
		} catch(Exception e) {
			failureMessage = 'Exception while generating data loader template for object: '+ selectedObjectName +' . Error: '+ e.getMessage(); return;
		}
		
		if(selectedObjFieldList == null || selectedObjFieldList.isEmpty()) {
			failureMessage = 'No field available for selected object '+ selectedObjectName +' to gererate dataloader template '; return;
		}
	}

}