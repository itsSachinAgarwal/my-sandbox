/**
* Name:  RoundRobinRefreshMembers
* Purpose: Class which will be used to refresh the members of Round Robin Group                                            
* History                                                           
* 
* VERSION  AUTHOR            				DATE           		Purpose                    
* 1.0 -    Sachin Agarwal ( Appirio JDC)   11/09/2015      INITIAL DEVELOPMENT     
**/
global with sharing class RoundRobinRefreshMembers {
	WebService static String refreshMembers(Id roundRobinGroupId, String queueName){
		//Finding all the queue members which are already associated with the affected
		//Round Robin Groups
		map<id, Queue_Member__c> mapOfAllCurrentQueueMembers = new map<id, Queue_Member__c>();
		map<id, Queue_Member__c> mapOfUserIdAndQueueMember = new map<id, Queue_Member__c>();
		Savepoint sp;
		list<GroupMember> listOfActualQueueMembers = new list<GroupMember>();
		list<Queue_Member__c> listOfQueueMembers = new list<Queue_Member__c>();
		set<Id> setOfActualQueueMembers = new set<Id>();  
		set<Id> setOfQueueMembersToBeDeleted = new set<Id>();
		list<Queue_Member__c> listOfQueueMembersToBeDeleted = new list<Queue_Member__c>();
		set<Id> setOfQueueMembersToBeAdded = new set<Id>();
		if(roundRobinGroupId == null || queueName == null){
			return 'Queue/Round Robin Group id cannot be null';
		}
		try{
			
			mapOfAllCurrentQueueMembers = new map<id, Queue_Member__c>([SELECT id, User__c
																 		FROM Queue_Member__c
																 		WHERE Round_Robin_Group__c = :roundRobinGroupId]);
			for(Queue_Member__c qm : mapOfAllCurrentQueueMembers.Values()){
				mapOfUserIdAndQueueMember.put(qm.User__c, qm);
			}
			listOfActualQueueMembers = [SELECT UserOrGroupId 
								  		FROM GroupMember 
								  		WHERE Group.name = :queueName];
								  
			for(GroupMember gm : listOfActualQueueMembers){
				if(gm.UserOrGroupId != null && String.valueOf(gm.UserOrGroupId).startsWith('005')){
					setOfActualQueueMembers.add(gm.UserOrGroupId);
				}
			}
			System.debug('mapOfUserIdAndQueueMember.keyset() ' + mapOfUserIdAndQueueMember.keyset());	
			for(id singleId : mapOfUserIdAndQueueMember.keyset()){
				if(!setOfActualQueueMembers.contains(singleId)){
					setOfQueueMembersToBeDeleted.add(singleId);	
				}
			}
			for(id singleId : setOfActualQueueMembers){
				if(!mapOfUserIdAndQueueMember.containsKey(singleId)){
					setOfQueueMembersToBeAdded.add(singleId);	
				}
			}
			
			for(Id singleId : setOfQueueMembersToBeDeleted){
				if(mapOfUserIdAndQueueMember.containsKey(singleId)){
					listOfQueueMembersToBeDeleted.add(mapOfUserIdAndQueueMember.get(singleId));	
				}
			}
			sp = Database.setSavepoint();
			delete listOfQueueMembersToBeDeleted;
		}
		catch(Exception ex){
			return 'Error Occurred : ' + ex.getMessage();
		}
		try{
			for(id queueMemberToBeAdded : setOfQueueMembersToBeAdded){
				listOfQueueMembers.add(new Queue_Member__c(Ready_for_Round_Robin_Assignment__c = true, Round_Robin_Group__c = roundRobinGroupId, User__c = queueMemberToBeAdded));
			}
			insert listOfQueueMembers;
			return null;
		}
		catch(Exception e){
			Database.rollback(sp);
			return 'Error Occurred : ' + e.getMessage();
		}
		return null;
	}
}