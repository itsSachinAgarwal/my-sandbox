/**
* Name:  GeneratePDFController
* Purpose: Class acting as a controller for VF page  GeneratePDF                                                    
* History                                                           
* 
* VERSION  AUTHOR            				DATE           		Purpose                    
* 1.0 -    Sachin Agrawal ( Appirio JDC)   14/07/2015      INITIAL DEVELOPMENT     
**/
public with sharing class GeneratePDFController {
	public String bodyHTML {get;set;}
	/**
  	*Purpose - This is the constructor of the class
  	*Parameters - sc
  	*Returns - none
  	**/
	public GeneratePDFController(){
		list<HTMLHolder__c> listOfHolders = new list<HTMLHolder__c>();
		listOfHolders = [SELECT htmlBody__c
						 FROM HTMLHolder__c];
		bodyHTML = '';
		for(HTMLHolder__c holder : listOfHolders){
			bodyHTML += holder.htmlBody__c;
		}
	}
}