public class DeleteStudentsControl 
{
    public list<Student__c> listOfStudents{get;set;}
	public DeleteStudentsControl()
    {
        list<ID> listIDStudents=ApexPages.currentPage().getParameters().get('selectedId').split(',');
        listOfStudents=[select id, First_Name__c, Last_Name__c, Age__c, Class__c from Student__c where id in :listIDStudents];
    }
    public Pagereference DeleteStudents()
    {
    	delete listOfStudents;
        return Cancel();
    }
    public Pagereference Cancel()
    {
    	Schema.DescribeSObjectResult result = Student__c.SObjectType.getDescribe();
    	PageReference pageRef = new PageReference('/' + result.getKeyPrefix());
    	pageRef.setRedirect(true);
    	return pageRef;
    }
    
}