/*******************************************************************
 * Name  : SmartOpportunitySearchExtension
 * Author: Sachin Agarwal
 * Date  : Mar 2, 2016
 *************************************************************************/
public class SmartOpportunitySearchExtension {

	//Search criteria fields
	public String opptyNameToSearch {set;get;}

	public integer searchCount{set; get;}
	public string searchStatus{set; get;}

	public integer limitRecords{set; get;} 

	public boolean isAsc{set; get;}
	public Integer showingFrom{get;set;}
	public Integer showingTo{get;set;}
	public string query;

	public boolean hasNext{get;set;}
	public boolean hasPrevious{get;set;}
	public String requestedPage {get;set;}

	public integer totalResults {set; get;}

	public Integer totalPage {set; get;}

	private static final Integer DEFAULT_RESULTS_PER_PAGE = 20;  
	private static final string SEARCH_TYPE = ' and ';

	public ApexPages.StandardSetController opportunityResults{get;set;}


	//Constructor
	public SmartOpportunitySearchExtension (ApexPages.StandardController controller) {
		resetSearchStatus();
	}


	//Constructor
	public SmartOpportunitySearchExtension (){
		resetSearchStatus();
	}


	//set to default status of page
	public void resetSearchStatus(){
		//Reset opportunity fields

		Opportunities = new List<Opportunity>();
		searchCount = 0;
		searchStatus = '';
		opptyNameToSearch = '';
		isAsc = true;
		hasPrevious = false;
		hasNext = false;
	}


	public List<Opportunity> Opportunities {
		get{
			return Opportunities;
		}set;
	}


	public PageReference cancel(){
		Pagereference pg = null;
		return pg;  
	}


	public String findSearchCondition(String query){
		opptyNameToSearch = opptyNameToSearch.replace('*','');
		if(opptyNameToSearch != null && opptyNameToSearch != ''){
			if(query.toUpperCase().contains('WHERE')){
				query += ' and Name like \'%' + opptyNameToSearch.Trim() + '%\'';
			}
			else{
				query += ' where Name like \'%' + opptyNameToSearch.Trim() +  '%\'';
			}
		}
		return query;
	}  


	public void performSearch() {
		searchOpportunity();
	}


	//method to search opportunity and make list according to pagesize
	private void searchOpportunity(){
		limitRecords = 250;
		query = 'SELECT ID, Name, StageName, CloseDate FROM Opportunity';
		query = findSearchCondition(query);
		query += ' ORDER BY createddate desc';

		try{
			Opportunities = new List<Opportunity>();
			opportunityResults = new ApexPages.StandardSetController(Database.query(query));
			opportunityResults.setPageSize(DEFAULT_RESULTS_PER_PAGE);
			Opportunities = opportunityResults.getRecords();
			searchCount = opportunityResults.getResultSize();
			if (searchCount >= limitRecords) {
				searchStatus = 'Search returned more than ' + limitRecords + ' records. Please refine your search';
			}
		}
		catch(Exception e){
			searchCount = 0;
		}  
		if (searchCount  == 0){
			searchStatus = 'No matching results found.';
		}

		requestedPage = String.valueOf(opportunityResults.getPageNumber());
		showingFrom = 1;

		totalResults = 0;
		for (List<Sobject> recordBatch:Database.query(query)){
			totalResults = totalResults + recordBatch.size();
		}
		totalPage = 0;
		totalPage = totalResults / opportunityResults.getPageSize() ; 
		if (totalPage * opportunityResults.getPageSize() < totalResults){
			totalPage++;
		}

		if(searchCount < opportunityResults.getPageSize()) {
			showingTo = searchCount;
		} else {
			showingTo = opportunityResults.getPageSize();
		}
		if(opportunityResults.getHasNext()) {
			hasNext = true;
		} 
		else {
			hasNext = false;
		}
		hasPrevious = false;
	}

	
	// Method that will take the user to next page
	public PageReference nextPage(){

		if(opportunityResults.getHasNext()) {
			Opportunities = new List<Opportunity>();
			opportunityResults.next();
			Opportunities = opportunityResults.getRecords();
			showingFrom = showingFrom + opportunityResults.getPageSize();
			showingTo =  showingTo + Opportunities.size();
			if(opportunityResults.getHasNext()) {
				hasNext = true;
			} 
			else {
				hasNext = false;
			}
			hasPrevious = true; 
		}
		requestedPage = String.valueOf(opportunityResults.getPageNumber());
		return null;
	}

	
	// Method that will take the user to previous page
	public PageReference previousPage(){
		if(opportunityResults.getHasPrevious()) {
			showingTo =  showingTo - Opportunities.size();
			Opportunities = new List<Opportunity>();
			opportunityResults.previous();
			Opportunities = opportunityResults.getRecords();
			showingFrom = showingFrom - opportunityResults.getPageSize();
			hasNext = true;
			if(opportunityResults.getHasPrevious()) {
				hasPrevious = true;
			} 
			else {
				hasPrevious = false;
			}
		}
		requestedPage = String.valueOf(opportunityResults.getPageNumber());  
		return null;
	}

	
	// Method that will take the user to the requested page
	public PageReference requestedPage(){

		boolean check = pattern.matches('[0-9]+', requestedPage); 
		Integer pageNo = check? Integer.valueOf(requestedPage) : 0;
		if(pageNo == 0 || pageNo > totalPage){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid page number.')); 
			return null;       
		}   
		opportunityResults.setPageNumber(pageNo);
		Opportunities = opportunityResults.getRecords();
		if(opportunityResults.getHasPrevious()) {
			hasPrevious = true;
		} 
		else {
			hasPrevious = false;
		}
		if(opportunityResults.getHasNext()) {
			hasNext = true;
		}
		else {
			hasNext = false;
		}
		showingFrom  = (pageNo - 1) * opportunityResults.getPageSize() + 1;

		showingTo = showingFrom + Opportunities.size() - 1;
		if(showingTo > totalResults) {
			showingTo = totalResults;
		}
		return null;
	}
}