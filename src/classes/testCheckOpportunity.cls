@istest
public class testCheckOpportunity
{
    public static testmethod void testupdateOpportunity()
    {
        Contact singleCt=new Contact(lastname='Gupta',experience__c=10);
        insert singleCt;
        Date dt=Date.newInstance(2014, 12, 23);
        Opportunity opp=new Opportunity(name='haha', closedate=dt, stagename='Prospecting');
        insert opp;
        opp.Bill_to_contact__c=singleCt.id;
        update opp;  
    }
}