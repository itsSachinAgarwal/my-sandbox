public class accPageController {
    public integer total_size=0;
    public integer list_size=10;
    public integer counter=0;
    public integer flag=0;
    public list<account> accList{get; set;}
    public string billingCountry{get;set;}
    public string billingState{get;set;}
    public string billingCity{get;set;}
    
	public accPageController(ApexPages.StandardController controller)
    {
        
    }
    public void getTotalSize()
    {
        if(String.isNotEmpty(billingCountry) && String.isNotEmpty(billingState) && String.isNotEmpty(billingCity))
        {
      		total_size=[select count() from account where billingCountry=:billingCountry and billingState=:billingState and billingCity=:billingCity];    
        }
        else if(String.isNotEmpty(billingCountry) && String.isNotEmpty(billingState) && String.isEmpty(billingCity))
        {
            total_size=[select count() from account where billingCountry=:billingCountry and billingState=:billingState];
        }
        else if(String.isNotEmpty(billingCountry) && String.isEmpty(billingState) && String.isNotEmpty(billingCity))
        {
            total_size=[select count() from account where billingCountry=:billingCountry and billingCity=:billingCity];
        }
        else if(String.isEmpty(billingCountry) && String.isNotEmpty(billingState) && String.isNotEmpty(billingCity))
        {
      		total_size=[select count() from account where billingState=:billingState and billingCity=:billingCity];
        }
        else if(String.isNotEmpty(billingCountry) && String.isEmpty(billingState) && String.isEmpty(billingCity))
        {
      		total_size=[select count() from account where billingCountry=:billingCountry];
        }
        else if(String.isEmpty(billingCountry) && String.isEmpty(billingState) && String.isNotEmpty(billingCity))
        {
      		total_size=[select count() from account where billingCity=:billingCity];
        }
        else if(String.isEmpty(billingCountry) && String.isNotEmpty(billingState) && String.isEmpty(billingCity))
        {
      		total_size=[select count() from account where billingState=:billingState ];
        }
        else
        {
            total_size=0;
        }
    }
    public void search()
    {
        if(flag==0)
        {
            flag=1;
            getTotalSize();
        }
        if(String.isNotEmpty(billingCountry) && String.isNotEmpty(billingState) && String.isNotEmpty(billingCity))
        {
      		accList=[select id, name from account where billingCountry=:billingCountry and billingState=:billingState and billingCity=:billingCity limit :list_size offset :counter];    
        }
        else if(String.isNotEmpty(billingCountry) && String.isNotEmpty(billingState) && String.isEmpty(billingCity))
        {
            accList=[select id, name from account where billingCountry=:billingCountry and billingState=:billingState limit :list_size offset :counter];
        }
        else if(String.isNotEmpty(billingCountry) && String.isEmpty(billingState) && String.isNotEmpty(billingCity))
        {
            accList=[select id, name from account where billingCountry=:billingCountry and billingCity=:billingCity limit :list_size offset :counter];
        }
        else if(String.isEmpty(billingCountry) && String.isNotEmpty(billingState) && String.isNotEmpty(billingCity))
        {
      		accList=[select id, name from account where billingState=:billingState and billingCity=:billingCity limit :list_size offset :counter];
        }
        else if(String.isNotEmpty(billingCountry) && String.isEmpty(billingState) && String.isEmpty(billingCity))
        {
      		accList=[select id, name from account where billingCountry=:billingCountry limit :list_size offset :counter];
        }
        else if(String.isEmpty(billingCountry) && String.isEmpty(billingState) && String.isNotEmpty(billingCity))
        {
      		accList=[select id, name from account where billingCity=:billingCity limit :list_size offset :counter];
        }
        else if(String.isEmpty(billingCountry) && String.isNotEmpty(billingState) && String.isEmpty(billingCity))
        {
      		accList=[select id, name from account where billingState=:billingState limit :list_size offset :counter];
        }
        else
        {
            accList.clear();
        }
        
    }
    public Pagereference Next()
    {
        counter+=list_size;
        search();
        return null;
    }
    public Pagereference Previous()
    {
        counter-=list_size;
        search();
        return null;
    }
    public Pagereference Beginning()
    {
        counter=0;
        search();
        return null;
    }
    public Pagereference End()
    {
        counter = total_size - math.mod(total_size, list_size);
      	search();
        return null;
    }
    public Boolean getPreviousDisabled() 
    { 
        if (counter>0) 
        {
            return false; 
        }
        else
        {
            return true;
        }
    }
 
   public Boolean getNextDisabled() 
   { 
      if (counter + list_size < total_size)
      {
            return false; 
      }
      else
      {
           return true;
      }
   }
}