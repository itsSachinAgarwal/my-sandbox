public class FindRecType 
{
    public list<recordtype> recordtypevalues{get;set;}
    public string recordtypevalue{get;set;}
	public FindRecType(ApexPages.StandardController controller)
    {
        Contact c=[select id, recordtypeid from contact where id=:ApexPages.currentPage().getParameters().get('id')];
        
        recordtypevalues=[select name from recordtype where id=:c.RecordTypeId];
        recordtypeValue='Not associated with any record type';
        
        for(recordtype rt:recordtypevalues)
        {
            recordtypeValue=rt.name;
        }               
    }
}