/**
* Name:  CCMasker
* Purpose: Class for validating and masking Credit Card numbers                                                      
* History                                                           
* 
* VERSION  AUTHOR            				DATE           		Purpose                    
* 1.0 -    Sachin Agrawal ( Appirio JDC)   03/09/2015      INITIAL DEVELOPMENT     
**/
public with sharing class CCMasker {
	//Method to mask credit card number if present in the email body
	public static String maskerMethod(String strSource) {
		//Checking if the input string is null or blank
    	if(strSource != null && String.isNotBlank(strSource)) {
      		String newword = '';
			String oldword = '';
 			map<String, String>  mapOfWords = new map<String, String>();
 			//The credit card numbers are of length 13 to 16.
			Pattern myPattern = Pattern.compile('(?:\\d[ -]*?){13,16}');
        	Matcher myMatcher = myPattern.matcher(strSource);
        	//Searching the input string for all the matching 13-16 digit numbers
        	//and removing any spaces or hyphens
        	while (myMatcher.find())
        	{
        		if(myMatcher.group() != null){
            		oldword = myMatcher.group();
            		newword = myMatcher.group().replaceAll('[ -]*', '');
            		mapOfWords.put(oldword, newword);
        		}
        	}
			System.debug('Mapofwords ' + mapOfWords);
			for(String oneword : mapOfWords.keyset()){
				//Masking all the credit card numbers matching the patterns specified
    			mapOfWords.put(oneword, mapOfWords.get(oneword).replaceall('^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|6(?:011|5[0-9]{2})[0-9]{12}|(?:2131|1800|35\\d{3})\\d{11})$', mask(mapOfWords.get(oneword))));
			}
			System.debug('Mapofwords now' + mapOfWords);
			//Replacing the credit card numbers in the input string with the masked string
			for(String oneword : mapOfWords.keyset()){
    			strSource = strSource.replaceall(oneword, mapOfWords.get(oneword));
			}
			return strSource;
    	} 
    	else {
      		return strSource; // else Return the Original Value
    	}	
  	}
  	//Method to mask credit card number showing only the last two digits
  	public static string mask(String creditCardNumber){
  		if(creditCardNumber != null && creditCardNumber.length() > 12){
	  		String masked = creditCardNumber.subString(0, (creditCardNumber.length() - 2));
	  		masked = masked.replaceall('[0-9]{4}', 'XXXX-');
	  		masked = masked.replaceall('\\d', 'X');
	  		masked += creditCardNumber.subString((creditCardNumber.length() - 2), creditCardNumber.length());
	  		return masked;
  		} 
  		return creditCardNumber;
  	}
}