/*******************************************************************
Name  : SmartOpportunitySearchExtensionTest
Author: Sachin Agarwal
Date  : Mar 2, 2016
Description: To Test SmartOpportunitySearchExtension Class. 
 *************************************************************************/
@isTest
private class SmartOpportunitySearchTest {

	static testMethod void myUnitTest() {
		ApexPages.StandardController sc;
		// Creating test data
		createData();
		SmartOpportunitySearchExtension controller = new SmartOpportunitySearchExtension(sc);
		controller.opptyNameToSearch = 'TestOpptyt0';

		controller.requestedPage = '1';
		controller.performSearch();

		System.assertEquals(55, controller.searchCount);
		System.assertEquals(1, controller.showingFrom);
		System.assertEquals(20, controller.showingTo);
		System.assertEquals(3, controller.totalPage);
		System.assertEquals(true, controller.hasNext);
		System.assertEquals(false, controller.hasPrevious);

		controller.nextPage();
		System.assertEquals(21, controller.showingFrom);
		System.assertEquals(40, controller.showingTo);
		System.assertEquals(3, controller.totalPage);
		System.assertEquals(true, controller.hasNext);
		System.assertEquals(true, controller.hasPrevious);

		controller.previousPage();

		controller.requestedPage = '3';
		controller.requestedPage();
		System.assertEquals(41, controller.showingFrom);
		System.assertEquals(55, controller.showingTo);
		System.assertEquals(3, controller.totalPage);
		System.assertEquals(false, controller.hasNext);
		System.assertEquals(true, controller.hasPrevious);

		controller.cancel();

		controller.requestedPage = '5';
		controller.requestedPage();
	}


	// Method responsible for creating test data
	static void CreateData() {
		Account acc = new Account();
		acc.Name = '00TestAccount0';
		acc.BillingCountry = 'Sweden';
		insert acc;

		List<Opportunity> oppList = new List<Opportunity>();
		Opportunity oppty;
		for(Integer i=0;i<55;i++) {
			oppty = new Opportunity();
			oppty.Name = 'TestOpptyt0' + i;
			oppty.Amount = i*3;
			oppty.AccountId = acc.ID;
			oppty.StageName = 'Won';
			oppty.CloseDate = Date.today().addDays(30);
			oppList.add(oppty);
		}

		insert oppList;
	}
}