/**=====================================================================
 * Appirio, Inc
 * Class Name               : SelectCampaignRTController_Test 
 * Description              : Test class for testing SelectCampaignRTController.cls
 * Created Date             : 06/26/2015
 * Created By               : Sachin Agarwal
 * 
 * Date Modified                Modified By             Description of the update
 * mm/dd/yyy                   FirsName LastName          Reason
 =====================================================================*/
@isTest
public with sharing class SelectCampaignRTController_Test {
    static testMethod void unitTest() {
        list<CampaignRT_MemberRT_Mapping__c> crt = new list<CampaignRT_MemberRT_Mapping__c>();
        crt.add(new CampaignRT_MemberRT_Mapping__c(Name = '1', Campaign_Member_RT__c = 'Application Based Program Member', Campaign_RT__c = 'Application Based Program'));
        crt.add(new CampaignRT_MemberRT_Mapping__c(Name = '2', Campaign_Member_RT__c = 'Mentorship Based Program Member', Campaign_RT__c = 'Mentorship Based Program'));
        insert crt;
        
        SelectCampaignRTController sc = new SelectCampaignRTController();
        PageReference pageRef = Page.SelectCampaignRT;
        Test.setCurrentPage(pageRef);
        
        sc.cmp.RecordTypeId = sc.getRecordTypeId('Campaign', 'Application Based Program');
        PageReference pr = sc.editWithRecordType();
        System.assertEquals(pr.getParameters().get('p8'), sc.getRecordTypeId('CampaignMember', 'Application Based Program Member'));
        pr = sc.cancel();
    }
}