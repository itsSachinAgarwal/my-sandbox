/**
* Name:  CCMasker_Test
* Purpose: Class for testing CCMasker.cls                                                    
* History                                                           
* 
* VERSION  AUTHOR            				DATE           		Purpose                    
* 1.0 -    Sachin Agrawal ( Appirio JDC)   03/09/2015      INITIAL DEVELOPMENT     
**/
@isTest
private class CCMasker_Test {
	//Method responsible for doing the actual testing
    static testMethod void unitTest() {
        String input = 'Valid Visa Credit Card Number : 4012345678912345; Invalid Visa Credit Card Number: 5012345678912345';
        String output = CCMasker.maskerMethod(input);
        System.assertEquals(output, 'Valid Visa Credit Card Number : XXXX-XXXX-XXXX-XX45; Invalid Visa Credit Card Number: 5012345678912345');
    }
}