global class ReceiveEmail implements Messaging.InboundEmailHandler 
{
  global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, 
                                                         Messaging.Inboundenvelope envelope) 
  {
      Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
      //Contact[] lsContact=new Contact[0];
      List <Contact> listContact = new List<Contact> ();
      Messaging.InboundEmail.TextAttachment[] tAttachments = email.textAttachments;
	  Messaging.InboundEmail.BinaryAttachment[] bAttachments = email.BinaryAttachments;
      if(email.textAttachments==null)
          {
              system.debug('text attachments');
          }
      if(email.BinaryAttachments==null)
          {
              system.debug('binary attachments');
          }
      
      if(tAttachments!=null)
      {
          
          	for (Messaging.Inboundemail.TextAttachment tAttachment : tAttachments) 
            {
      			String xml= String.valueOf(tAttachment.body);
      			XmlStreamReader reader = new XmlStreamReader(xml);
      			XMLStreamReaderContacts ct =new XMLStreamReaderContacts();
       			listContact.addAll(ct.parseContacts(reader));
      		}
      }      
      insert listContact;
	  return result;
  }
}