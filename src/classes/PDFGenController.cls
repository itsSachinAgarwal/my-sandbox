public class PDFGenController {
	
    public PDFGenController(ApexPages.StandardController controller)
    {
        
    }
    public pageReference genPDF()
    {
        PageReference pdf = Page.StudentDetailPage;
        Attachment attach=new Attachment();
        pdf.getParameters().put('id',ApexPages.currentPage().getParameters().get('id'));
        
        Blob body;
    	try 
        {

        	// returns the output of the page as a PDF
        	body = pdf.getContent();

    		// need to pass unit test -- current bug    
    	} 
       	catch (VisualforceException e) 
        {
        	body = Blob.valueOf('Some Text');
    	}

    	attach.Body = body;
    	// add the user entered name
    	attach.name='Student Details';
    	attach.IsPrivate = false;
        attach.parentId=ApexPages.currentPage().getParameters().get('id');
   	 	// attach the pdf to the account
   	 	list<attachment> listAttachments=[select id from attachment where parentid=:ApexPages.currentPage().getParameters().get('id')];
        if(listAttachments.size()>0)
        {
        	    delete listAttachments;
        }
    	
    	insert attach;
 return new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));
    }
}