<apex:page controller="MassAddEditDeleteRecordsController" showHeader="true" sidebar="false" title="{!$Label.Mass_Add_Edit_Delete_Label_page_title}">

<apex:sectionHeader title="{!parentRecordName}" subTitle="{!$Label.Mass_Add_Edit_Delete_Label_page_title}" />

<apex:pageMessages id="errmsg" />

 <style type="text/css">
   
  	// Processing popup styles 
	.popupBackground {
        background-color:black;
        opacity: 0.20;
        filter: alpha(opacity = 20);
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        z-index: 998;
        position: absolute;
    }
    .popupPanel {
        border: solid 1px;
        background-color: white;
        left: 50%;
        width: 200px;
        margin-left: -100px;
        top: 50%;
        height: 50px;
        margin-top: -25px;
        z-index: 999;
        position: fixed;
    }
    
    .overlay{
        position:fixed;
        top:0px;
        bottom:0px;
        left:0px;
        right:0px;
        z-index:100;
        opacity: 0.20;
        background-color:lightgray;
    }
    
    .cmpbox{
        position:fixed;
        /*top:-300px;*/
        left:40%;
        background-color:#fff;
        color:#7F7F7F;
        padding:10px;
        border:2px solid #ccc;
        -moz-box-shadow: 0 1px 5px #333;
        -webkit-box-shadow: 0 1px 5px #333;
        z-index:100;
        width:365px;
        top:30%;
        border-radius: 14px 14px 14px 14px;
    }
    
    .closeStyle {
    	cursor: pointer;
   		float: right;
    }
    
    .imgEdit {
    	height: 10px;
    	padding-left: 5px;
    }
 </style>
 
 <!-- Processing status popup -->
	<apex:actionStatus id="processingStatus" stopText="">
	    <apex:facet name="start">
	        <div>
	            <div class="popupBackground" />
	            <div class="popupPanel">
	                <table border="0" width="100%" height="100%">
	                    <tr>
	                        <td align="center" style="font-family: Calibri; font-size: 13px; font-weight: normal;"><b>{!$Label.Mass_Add_Edit_Delete_Label_Processing}</b></td>
	                    </tr>
	                    <tr>
	                        <td align="center"><img src="/img/loading.gif"/></td>
	                    </tr>
	                </table>
	            </div>
	        </div>
	    </apex:facet>
	</apex:actionStatus>
	
<apex:form >
	
	<apex:actionfunction name="jsDeleteRow" action="{!deleteRow}" rerender="errmsg,addEditDeletePB" status="processingStatus" >
		<apex:param name="param1" assignTo="{!selectedRowIndexToDelete}" value="" />
	</apex:actionfunction>
	
	<apex:actionfunction name="jsDeleteSelectedRow" action="{!deleteSelectedRows}" rerender="errmsg,addEditDeletePB" status="processingStatus" />
	
	<!-- Update Section --> 
	<apex:pageBlock id="addEditDeletePB" rendered="{!isEverythingOk}" >
		
	 <apex:pageBlockButtons >
        <apex:commandButton value="{!$Label.Mass_Add_Edit_Delete_Button_Label_Save_All}" action="{!saveAll}" status="processingStatus" rerender="errmsg,addEditDeletePB" />
        <apex:commandButton value="{!$Label.Mass_Add_Edit_Delete_Button_Label_Delete_Selected}" onclick="deleteSelectedRows();return false;" status="processingStatus" rerender="errmsg,addEditDeletePB" />
        <apex:commandButton value="{!$Label.Mass_Add_Edit_Delete_Button_Label_Add_Rows}" action="{!addNewRows}" status="processingStatus" rerender="errmsg,addEditDeletePB" />
        <apex:commandButton value="{!$Label.Mass_Add_Edit_Delete_Button_Label_Cancel}" action="{!cancel}" immediate="true" />
     </apex:pageBlockButtons> 
	    
	  <apex:pageBlockSection columns="1" >
	  	
	  	<div id="existingSelectedMsg" style="font-weight: bold; font-size: 13px;" class="dataCell" ></div>
	    <!-- Page Block Table -->
	    <apex:pageBlockTable value="{!sObjectRowWrapperList}" var="rw" width="100%" >
	    	
	    	<apex:column > 
	   			<a href="" onclick="deleteRow('{!rw.rowIndex}');return false;" >
	      			<apex:image value="/img/func_icons/remove12_on.gif" title="{!$Label.Mass_Add_Edit_Delete_Label_Click_to_delete_the_row}" />
	      		</a>
	    	</apex:column>
	    	
	    	<apex:column >
				<apex:facet name="header">
					<input type="checkbox" id="checkAllRecord" onclick="checkAll(this,'checkBoxId', 'existingSelectedMsg')" />
				</apex:facet>
				<apex:inputCheckbox id="checkBoxId" value="{!rw.isSelected}" onclick="checkItem(this, 'checkAllRecord', 'existingSelectedMsg');" />
			</apex:column>
	    	
	    	<!-- rendering columns as per field set -->
	    	<apex:repeat value="{!fieldSetMemberList}" var="clm" >
	       		<apex:column >
	       			<apex:facet name="header" >
	                    <apex:commandLink value="{!clm.Label}" action="{!showEditFieldPopup}" rerender="errmsg,updateSelectedFieldPopupOP" status="processingStatus">
	                        <apex:param name="editFieldAPIName" value="{!clm.fieldPath}" assignTo="{!selectedFieldForMassUpdate}"/>
	                        <apex:image value="/img/func_icons/util/pencil12.gif" alt="{!$Label.Mass_Add_Edit_Delete_Label_Click_to_mass_edit_selected_column_field}" 
	                        		title="{!$Label.Mass_Add_Edit_Delete_Label_Click_to_mass_edit_selected_column_field}" styleClass="imgEdit" />
	                    </apex:commandLink>
	                </apex:facet>
	       			<apex:outputPanel >
		       			<div class="{!IF(OR(clm.required, clm.dbrequired), 'requiredInput', '')}">
		       			<div class="requiredBlock"></div>
		         		<apex:inputField value="{!rw.record[clm.fieldPath]}"  required="false"
		         			onchange="setRowDirty('{!$Component.savedImg}', '{!$Component.newRowImg}', '{!$Component.rowDirtyHidden}');" />
		         		</div>
	         		</apex:outputPanel>
	       		</apex:column>
	       	</apex:repeat>
	       	
	       	<apex:column > 
	      		<apex:image id="savedImg" value="/img/msg_icons/confirm16.png" rendered="{!AND(rw.rowStatus == 'Saved',NOT(rw.isRowdirty))}" title="{!rw.rowStatus}" />
	      		<apex:image id="newRowImg" value="/img/msg_icons/warning16.png" style="{!IF(rw.isRowdirty, '', 'display:none;')}" title="Unsaved" />
	      		
	      		<apex:inputHidden id="rowDirtyHidden" value="{!rw.isRowdirty}" />
	    	</apex:column>
	       	
	    </apex:pageBlockTable>
	 
	  </apex:pageBlockSection>
	</apex:pageBlock>
	
	<apex:outputPanel id="updateSelectedFieldPopupOP" >
		<apex:outputPanel rendered="{!showEditFieldPopup}" >
			<div id="massUpdateOverlayDiv" >
				<div class="overlay" id="overlay" ></div>
				<div class="cmpbox" id="cmpbox" >
					<apex:pageBlock title="{!$Label.Mass_Add_Edit_Delete_Label_Mass_Update_Field_Value}" id="editFieldPB" >
						<apex:pageBlockSection columns="1" >
							<apex:inputField value="{!childSObjectMockUp[selectedFieldForMassUpdate]}" required="false" />
						</apex:pageBlockSection>
						<apex:pageBlockButtons location="bottom" >
					        <apex:commandButton value="{!$Label.Mass_Add_Edit_Delete_Button_Label_Update}" action="{!updateFieldValueToRows}" status="processingStatus" rerender="errmsg,addEditDeletePB,updateSelectedFieldPopupOP" />
					        <apex:commandButton value="{!$Label.Mass_Add_Edit_Delete_Button_Label_Cancel}" action="{!closeEditFieldPopup}" status="processingStatus" rerender="errmsg,updateSelectedFieldPopupOP" />
					    </apex:pageBlockButtons> 
					</apex:pageBlock>
				</div>
			</div>
		</apex:outputPanel>
	</apex:outputPanel>
	
</apex:form>

<script>
	function deleteRow(selectedRowIndex) {
		if(confirm('{!$Label.Mass_Add_Edit_Delete_Confirmation_Msg_Are_you_sure_you_want_to_delete_this_row}')) {
			jsDeleteRow(selectedRowIndex);
		}
	}
	
	function deleteSelectedRows() {
		if(confirm('{!$Label.Mass_Add_Edit_Delete_Msg_Are_you_sure_you_want_to_delete_the_selected_rows}')) {
			jsDeleteSelectedRow();
		}
	}
	
	function setRowDirty(savedRowImgId, newRowImgId, dirtyCmpId){  
		if(document.getElementById(savedRowImgId) != null){         
	    	document.getElementById(savedRowImgId).style.display = "none";
	    }
	    document.getElementById(newRowImgId).style.display = "block";
	    document.getElementById(dirtyCmpId).value = "true";
	}
	
	function checkAll(comp, matchingStr, selectedInfoMsgDivId)
    {
         var isSelectAll = comp.checked;
         var inputElem = document.getElementsByTagName("input");
         var totalCheckboxCount = 0;
         for ( var i = 0; i < inputElem.length; i++) {
            if (inputElem[i].id.indexOf(matchingStr) != -1) {
                inputElem[i].checked = isSelectAll;
                totalCheckboxCount += 1;
             }
         }
        
        var countMsg = '0 selected';
        if(isSelectAll) { 
            countMsg = totalCheckboxCount +' Selected';
        } 
        document.getElementById(selectedInfoMsgDivId).innerHTML = countMsg;
    }
     
    function checkItem(comp, selectAllCheckboxStr, selectedInfoMsgDivId)
    {
        var allchecked = true;
        var lstindex = comp.id.lastIndexOf(':') + 1;
        var compID = comp.id.substring(lstindex, comp.id.length);
        var inputElem = document.getElementsByTagName("input");
        
        var checkedCount = 0; 
        var totalCheckboxCount = 0; 
        for ( var i = 0; i < inputElem.length; i++) 
        {
            if (inputElem[i].id.indexOf(compID) != -1)
            {   
                totalCheckboxCount += 1;
                if(inputElem[i].checked) {
                    checkedCount += 1;
                } else {
                    allchecked = false;
                }
            }
       } 
       if(allchecked)
         document.getElementById(selectAllCheckboxStr).checked = true; 
       else
         document.getElementById(selectAllCheckboxStr).checked = false;    
        
        var countMsg = checkedCount +' Selected';
        document.getElementById(selectedInfoMsgDivId).innerHTML = countMsg; 
    }

</script>

</apex:page>